package br.com.martinlabs.commons.pagarme;

import br.com.martinlabs.commons.exceptions.RespException;
import me.pagar.SubscriptionStatus;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 * @author ftgibran
 * Params class used for PagarMe POSTBACK
 */
@Ignore
public class PostbackTest {
    //API_KEY
    private final String API_KEY_TEST = "ak_test_ZprIU61dVBii2GC9nqqf0teNO1C1Xv";
    private final String SAMPLE_PAYLOAD_1 = "id=278317&fingerprint=d574b5fe32c90e9b9adb3ccb7e2569c19fa09a22&event=subscription_status_changed&old_status=paid&desired_status=paid&current_status=canceled&object=subscription&subscription%5Bobject%5D=subscription&subscription%5Bplan%5D%5Bobject%5D=plan&subscription%5Bplan%5D%5Bid%5D=242249&subscription%5Bplan%5D%5Bamount%5D=9990&subscription%5Bplan%5D%5Bdays%5D=30&subscription%5Bplan%5D%5Bname%5D=Apptite%20Chef%20Premium%20Test&subscription%5Bplan%5D%5Btrial_days%5D=0&subscription%5Bplan%5D%5Bdate_created%5D=2017-12-13T13%3A59%3A48.155Z&subscription%5Bplan%5D%5Bpayment_methods%5D%5B0%5D=credit_card&subscription%5Bplan%5D%5Bcolor%5D=%23FD9827&subscription%5Bplan%5D%5Bcharges%5D=&subscription%5Bplan%5D%5Binstallments%5D=1&subscription%5Bplan%5D%5Binvoice_reminder%5D=7&subscription%5Bplan%5D%5Bpayment_deadline_charges_interval%5D=1&subscription%5Bid%5D=278317&subscription%5Bcurrent_transaction%5D%5Bobject%5D=transaction&subscription%5Bcurrent_transaction%5D%5Bstatus%5D=paid&subscription%5Bcurrent_transaction%5D%5Brefuse_reason%5D=&subscription%5Bcurrent_transaction%5D%5Bstatus_reason%5D=acquirer&subscription%5Bcurrent_transaction%5D%5Bacquirer_response_code%5D=0000&subscription%5Bcurrent_transaction%5D%5Bacquirer_name%5D=pagarme&subscription%5Bcurrent_transaction%5D%5Bacquirer_id%5D=5a218978aa6baa7b562a6186&subscription%5Bcurrent_transaction%5D%5Bauthorization_code%5D=439931&subscription%5Bcurrent_transaction%5D%5Bsoft_descriptor%5D=&subscription%5Bcurrent_transaction%5D%5Btid%5D=2727283&subscription%5Bcurrent_transaction%5D%5Bnsu%5D=2727283&subscription%5Bcurrent_transaction%5D%5Bdate_created%5D=2018-01-09T10%3A44%3A58.967Z&subscription%5Bcurrent_transaction%5D%5Bdate_updated%5D=2018-01-09T10%3A44%3A59.650Z&subscription%5Bcurrent_transaction%5D%5Bamount%5D=9990&subscription%5Bcurrent_transaction%5D%5Bauthorized_amount%5D=9990&subscription%5Bcurrent_transaction%5D%5Bpaid_amount%5D=9990&subscription%5Bcurrent_transaction%5D%5Brefunded_amount%5D=0&subscription%5Bcurrent_transaction%5D%5Binstallments%5D=1&subscription%5Bcurrent_transaction%5D%5Bid%5D=2727283&subscription%5Bcurrent_transaction%5D%5Bcost%5D=20&subscription%5Bcurrent_transaction%5D%5Bcard_holder_name%5D=HELLO%20WORLD&subscription%5Bcurrent_transaction%5D%5Bcard_last_digits%5D=6550&subscription%5Bcurrent_transaction%5D%5Bcard_first_digits%5D=491603&subscription%5Bcurrent_transaction%5D%5Bcard_brand%5D=visa&subscription%5Bcurrent_transaction%5D%5Bcard_pin_mode%5D=&subscription%5Bcurrent_transaction%5D%5Bpostback_url%5D=&subscription%5Bcurrent_transaction%5D%5Bpayment_method%5D=credit_card&subscription%5Bcurrent_transaction%5D%5Bcapture_method%5D=ecommerce&subscription%5Bcurrent_transaction%5D%5Bantifraud_score%5D=&subscription%5Bcurrent_transaction%5D%5Bboleto_url%5D=&subscription%5Bcurrent_transaction%5D%5Bboleto_barcode%5D=&subscription%5Bcurrent_transaction%5D%5Bboleto_expiration_date%5D=&subscription%5Bcurrent_transaction%5D%5Breferer%5D=api_key&subscription%5Bcurrent_transaction%5D%5Bip%5D=54.94.137.80&subscription%5Bcurrent_transaction%5D%5Bsubscription_id%5D=278317&subscription%5Bcurrent_transaction%5D%5Bsplit_rules%5D=&subscription%5Bcurrent_transaction%5D%5Breference_key%5D=&subscription%5Bcurrent_transaction%5D%5Bdevice%5D=&subscription%5Bcurrent_transaction%5D%5Blocal_transaction_id%5D=&subscription%5Bcurrent_transaction%5D%5Blocal_time%5D=&subscription%5Bcurrent_transaction%5D%5Bfraud_covered%5D=false&subscription%5Bpostback_url%5D=http%3A%2F%2Fbeta.martinlabs.com.br%3A8080%2FApptiteChefPainel%2Fapi%2FPostback&subscription%5Bpayment_method%5D=credit_card&subscription%5Bcard_brand%5D=mastercard&subscription%5Bcard_last_digits%5D=8151&subscription%5Bcurrent_period_start%5D=2018-01-09T10%3A44%3A58.804Z&subscription%5Bcurrent_period_end%5D=2018-02-08T10%3A44%3A58.804Z&subscription%5Bcharges%5D=0&subscription%5Bstatus%5D=canceled&subscription%5Bdate_created%5D=2018-01-09T10%3A44%3A59.623Z&subscription%5Bdate_updated%5D=2018-01-09T10%3A50%3A19.684Z&subscription%5Bphone%5D%5Bobject%5D=phone&subscription%5Bphone%5D%5Bddi%5D=55&subscription%5Bphone%5D%5Bddd%5D=11&subscription%5Bphone%5D%5Bnumber%5D=111111111&subscription%5Bphone%5D%5Bid%5D=248864&subscription%5Baddress%5D%5Bobject%5D=address&subscription%5Baddress%5D%5Bstreet%5D=Rua%20Barra%20Funda&subscription%5Baddress%5D%5Bcomplementary%5D=200&subscription%5Baddress%5D%5Bstreet_number%5D=100&subscription%5Baddress%5D%5Bneighborhood%5D=Barra%20Funda&subscription%5Baddress%5D%5Bcity%5D=S%C3%A3o%20Paulo&subscription%5Baddress%5D%5Bstate%5D=SP&subscription%5Baddress%5D%5Bzipcode%5D=01152000&subscription%5Baddress%5D%5Bcountry%5D=Brasil&subscription%5Baddress%5D%5Bid%5D=259325&subscription%5Bcustomer%5D%5Bobject%5D=customer&subscription%5Bcustomer%5D%5Bid%5D=442824&subscription%5Bcustomer%5D%5Bexternal_id%5D=&subscription%5Bcustomer%5D%5Btype%5D=&subscription%5Bcustomer%5D%5Bcountry%5D=&subscription%5Bcustomer%5D%5Bdocument_number%5D=49562951200&subscription%5Bcustomer%5D%5Bdocument_type%5D=cpf&subscription%5Bcustomer%5D%5Bname%5D=Gil%20KKKK&subscription%5Bcustomer%5D%5Bemail%5D=melanke.hc%40gmail.com&subscription%5Bcustomer%5D%5Bphone_numbers%5D=&subscription%5Bcustomer%5D%5Bborn_at%5D=&subscription%5Bcustomer%5D%5Bbirthday%5D=&subscription%5Bcustomer%5D%5Bgender%5D=Masculino&subscription%5Bcustomer%5D%5Bdate_created%5D=2018-01-09T10%3A44%3A58.723Z&subscription%5Bcard%5D%5Bobject%5D=card&subscription%5Bcard%5D%5Bid%5D=card_cjc7ik921030n0s6d5o8820do&subscription%5Bcard%5D%5Bdate_created%5D=2018-01-09T10%3A49%3A07.034Z&subscription%5Bcard%5D%5Bdate_updated%5D=2018-01-09T10%3A49%3A07.387Z&subscription%5Bcard%5D%5Bbrand%5D=mastercard&subscription%5Bcard%5D%5Bholder_name%5D=NEW%20CARD&subscription%5Bcard%5D%5Bfirst_digits%5D=556557&subscription%5Bcard%5D%5Blast_digits%5D=8151&subscription%5Bcard%5D%5Bcountry%5D=INDIA&subscription%5Bcard%5D%5Bfingerprint%5D=cjc7ik9140oir0h53jbtysdiq&subscription%5Bcard%5D%5Bvalid%5D=true&subscription%5Bcard%5D%5Bexpiration_date%5D=0523&subscription%5Bmetadata%5D=&subscription%5Bsettled_charges%5D=";
    private final String SAMPLE_SIGNATURE_1 = "sha1=41b9272f7c886d04ae4e1321892aa7c0ecc28d06";

    private final String SAMPLE_INVALID_SIGNATURE = "sha1=7c4a8d09ca3762af61e59520943dc26494f8941b";

    @Test
    public void allowPostbackWithParams() throws Exception {
        Postback params = new Postback();
        params.allowPostbackWithParams(SAMPLE_PAYLOAD_1, SAMPLE_SIGNATURE_1, API_KEY_TEST);

        Postback.Event event = params.getEvent("event");
        String subscriptionId = params.getString("subscription[id]");
        Date dateCreated = params.getDate("subscription[plan][date_created]");
        Double paidAmountInCents = params.getDouble("subscription[current_transaction][paid_amount]");
        SubscriptionStatus currentStatus = params.getSubscriptionStatus("current_status");

        assertEquals(event, Postback.Event.SUBSCRIPTION_STATUS_CHANGED);
        assertEquals(subscriptionId, "278317");
        assertEquals(dateCreated.getTime(), 1513180788155L);
        assertEquals(paidAmountInCents, new Double(9990));
        assertEquals(currentStatus, SubscriptionStatus.CANCELED);
    }

    @Test(expected = RespException.class)
    public void denyPostbackWithParams() throws Exception {
        Postback params = new Postback();

        params.allowPostbackWithParams(SAMPLE_PAYLOAD_1, SAMPLE_INVALID_SIGNATURE, API_KEY_TEST);
    }
}