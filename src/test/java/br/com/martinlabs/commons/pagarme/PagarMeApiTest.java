package br.com.martinlabs.commons.pagarme;

import br.com.martinlabs.commons.CreditCardNumberGenerator;
import br.com.martinlabs.commons.exceptions.RespException;
import br.com.martinlabs.commons.pagarme.model.*;
import me.pagar.model.*;
import org.junit.Test;

import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;

/**
 * @author ftgibran
 */
@Ignore
public class PagarMeApiTest {
    //API_KEY
    private final String API_KEY_TEST = "[INSERT_API_KEY_HERE]";

    //Invalid ID
    private final String INVALID_ID = "000000";
    private final String INVALID_CARD_NUMBER = "0000000000000000";

    //Account 1
    private final String PLAN_ID_TEST_1_ID = "242249";
    private final String SUBSCRIPTION_ID_TEST_1_ID = "270299";
    private final String CARD_ID_TEST_1_ID = "card_cjb54znyp01ps7f6dwjexhgrg";

    //Account 2
    private final String PLAN_ID_TEST_2_ID = "252363";
    private final String SUBSCRIPTION_ID_TEST_2_ID = "278867";
    private final String CARD_ID_TEST_2_ID = "card_cjc97acg402qm786enfzdnpkk";

    //Samples
    private final String SAMPLE_STREET = "Street UNIT TEST";
    private final String SAMPLE_STREET_NUMBER = "555";
    private final String SAMPLE_NEIGHBORHOOD = "Neighborhood UNIT TEST";
    private final String SAMPLE_ZIPCODE = "30120-907";

    private final String SAMPLE_NAME = "Unit Test";
    private final String SAMPLE_EMAIL = "unit@test555.co";

    private final String SAMPLE_DDD = "55";
    private final String SAMPLE_PHONE_NUMBER = "555555555";

    private final String SAMPLE_CARD_NUMBER = "4111111111111111";
    private final String SAMPLE_CARD_HOLDER_NAME = "UNIT TEST";
    private final Integer SAMPLE_CARD_CVV = 123;
    private final String SAMPLE_CARD_EXPIRES_AT = "1230";

    //References
    private IPlan iPlan = new IPlan() {
        String pagarmePlanId;
        PlanModel plan;

        @Override
        public String getPagarmePlanId() {
            return pagarmePlanId;
        }

        @Override
        public void setPagarmePlanId(String pagarmePlanId) {
            this.pagarmePlanId = pagarmePlanId;
        }

        @Override
        public PlanModel getPlan() {
            return plan;
        }

        @Override
        public void setPlan(PlanModel plan) {
            this.plan = plan;
        }
    };
    private ISubscription iSubscription = new ISubscription() {
        String pagarmeSubscriptionId;
        String pagarmeCardId;

        SubscriptionModel subscription;
        CardModel card;

        @Override
        public String getPagarmeSubscriptionId() {
            return pagarmeSubscriptionId;
        }

        @Override
        public void setPagarmeSubscriptionId(String pagarmeSubscriptionId) {
            this.pagarmeSubscriptionId = pagarmeSubscriptionId;
        }

        @Override
        public String getPagarmeCardId() {
            return pagarmeCardId;
        }

        @Override
        public void setPagarmeCardId(String pagarmeCardId) {
            this.pagarmeCardId = pagarmeCardId;
        }

        @Override
        public SubscriptionModel getSubscription() {
            return subscription;
        }

        @Override
        public void setSubscription(SubscriptionModel subscription) {
            this.subscription = subscription;
        }

        @Override
        public CardModel getCard() {
            return card;
        }

        @Override
        public void setCard(CardModel card) {
            this.card = card;
        }
    };
    private ITransaction iTransaction = new ITransaction() {
        String pagarmeTransactionId;
        TransactionModel transaction;

        @Override
        public String getPagarmeTransactionId() {
            return pagarmeTransactionId;
        }

        @Override
        public void setPagarmeTransactionId(String pagarmeTransactionId) {
            this.pagarmeTransactionId = pagarmeTransactionId;
        }

        @Override
        public TransactionModel getTransaction() {
            return transaction;
        }

        @Override
        public void setTransaction(TransactionModel transaction) {
            this.transaction = transaction;
        }
    };

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void signedInPlan() throws RespException {
        iPlan.setPagarmePlanId(PLAN_ID_TEST_1_ID);
        iSubscription.setPagarmeSubscriptionId(SUBSCRIPTION_ID_TEST_1_ID);

        boolean result = PagarMeApi.signedInPlan(iPlan, iSubscription, API_KEY_TEST);
        assertTrue(result);
    }

    @Test
    public void notSignedInPlan() throws RespException {
        iPlan.setPagarmePlanId(PLAN_ID_TEST_2_ID);
        iSubscription.setPagarmeSubscriptionId(SUBSCRIPTION_ID_TEST_1_ID);

        boolean result = PagarMeApi.signedInPlan(iPlan, iSubscription, API_KEY_TEST);
        assertFalse(result);
    }

    @Test
    public void getPlan() throws RespException {
        iPlan.setPagarmePlanId(PLAN_ID_TEST_1_ID);

        PagarMeApi.getPlan(iPlan, API_KEY_TEST);

        assertNotNull(iPlan.getPlan());
    }

    @Test(expected = RespException.class)
    public void notFoundPlan() throws RespException {
        iPlan.setPagarmePlanId(INVALID_ID);

        PagarMeApi.getPlan(iPlan, API_KEY_TEST);
    }

    @Test
    public void getSubscriptionAndCard() throws RespException {
        iSubscription.setPagarmeSubscriptionId(SUBSCRIPTION_ID_TEST_1_ID);
        iSubscription.setPagarmeCardId(CARD_ID_TEST_1_ID);

        PagarMeApi.getSubscription(iSubscription, API_KEY_TEST);

        assertNotNull(iSubscription.getSubscription());
        assertNotNull(iSubscription.getCard());
    }

    @Test
    public void getOnlySubscription() throws RespException {
        iSubscription.setPagarmeSubscriptionId(SUBSCRIPTION_ID_TEST_1_ID);

        PagarMeApi.getSubscription(iSubscription, API_KEY_TEST);

        assertNotNull(iSubscription.getSubscription());
        assertNull(iSubscription.getCard());
    }

    @Test(expected = RespException.class)
    public void notFoundSubscription() throws RespException {
        iSubscription.setPagarmeSubscriptionId(INVALID_ID);

        PagarMeApi.getSubscription(iSubscription, API_KEY_TEST);
    }

    @Test
    public void createSubscription() throws RespException {
        SubscriptionModel subscription = new SubscriptionModel();
        CardModel card = new CardModel();
        CustomerModel customer = new CustomerModel(SAMPLE_NAME, SAMPLE_EMAIL);
        AddressModel address = new AddressModel(SAMPLE_STREET, SAMPLE_STREET_NUMBER, SAMPLE_NEIGHBORHOOD, SAMPLE_ZIPCODE);
        PhoneModel phone = new PhoneModel(SAMPLE_DDD, SAMPLE_PHONE_NUMBER);
        PlanModel plan = new PlanModel();

        card.setCardNumber(SAMPLE_CARD_NUMBER);
        card.setCardHolderName(SAMPLE_CARD_HOLDER_NAME);
        card.setCvv(SAMPLE_CARD_CVV);
        card.setCardExpirationDate(SAMPLE_CARD_EXPIRES_AT);

        customer.setAddress(address);
        customer.setPhone(phone);

        plan.setPlanId(PLAN_ID_TEST_1_ID);

        iSubscription.setSubscription(subscription);
        iSubscription.setCard(card);
        iSubscription.getSubscription().setCustomer(customer);
        iSubscription.getSubscription().setPlan(plan);

        PagarMeApi.createSubscription(iSubscription, "unit.test", API_KEY_TEST);

        assertNotNull(iSubscription.getPagarmeSubscriptionId());
        assertNotNull(iSubscription.getSubscription());
    }

    @Test
    public void createSubscriptionWithTransaction() throws RespException {
        SubscriptionModel subscription = new SubscriptionModel();
        CardModel card = new CardModel();
        CustomerModel customer = new CustomerModel(SAMPLE_NAME, SAMPLE_EMAIL);
        AddressModel address = new AddressModel(SAMPLE_STREET, SAMPLE_STREET_NUMBER, SAMPLE_NEIGHBORHOOD, SAMPLE_ZIPCODE);
        PhoneModel phone = new PhoneModel(SAMPLE_DDD, SAMPLE_PHONE_NUMBER);
        PlanModel plan = new PlanModel();

        card.setCardNumber(SAMPLE_CARD_NUMBER);
        card.setCardHolderName(SAMPLE_CARD_HOLDER_NAME);
        card.setCvv(SAMPLE_CARD_CVV);
        card.setCardExpirationDate(SAMPLE_CARD_EXPIRES_AT);

        customer.setAddress(address);
        customer.setPhone(phone);

        plan.setPlanId(PLAN_ID_TEST_1_ID);

        iSubscription.setSubscription(subscription);
        iSubscription.setCard(card);
        iSubscription.getSubscription().setCustomer(customer);
        iSubscription.getSubscription().setPlan(plan);

        PagarMeApi.createSubscription(iSubscription, iTransaction, "unit.test", API_KEY_TEST);

        assertNotNull(iSubscription.getPagarmeSubscriptionId());
        assertNotNull(iSubscription.getSubscription());

        assertNotNull(iSubscription.getPagarmeCardId());
        assertNotNull(iSubscription.getCard());

        assertNotNull(iTransaction.getPagarmeTransactionId());
        assertNotNull(iTransaction.getTransaction());
    }

    @Test
    public void updateCard() throws RespException {
        CardModel card = new CardModel();

        String uuid = UUID.randomUUID().toString();
        CreditCardNumberGenerator number = new CreditCardNumberGenerator();

        card.setCardNumber(number.generate("536382", 16));
        card.setCardHolderName(uuid);
        card.setCvv(SAMPLE_CARD_CVV);
        card.setCardExpirationDate(SAMPLE_CARD_EXPIRES_AT);

        iSubscription.setCard(card);
        iSubscription.setPagarmeSubscriptionId(SUBSCRIPTION_ID_TEST_1_ID);

        PagarMeApi.updateCard(iSubscription, API_KEY_TEST);

        assertNotNull(iSubscription.getCard());
        assertNotNull(iSubscription.getPagarmeCardId());
        assertNotEquals(iSubscription.getCard().getCardNumber(), SAMPLE_CARD_NUMBER);
    }

    @Test
    public void cancelSubscription() throws RespException {
        iSubscription.setPagarmeSubscriptionId(SUBSCRIPTION_ID_TEST_2_ID);

        try {
            PagarMeApi.cancelSubscription(iSubscription, API_KEY_TEST);
        } catch (RespException e) {
            if (!e.getMessage().equalsIgnoreCase("Assinatura já cancelada.")) {
                throw e;
            }
        }

    }

    /**
     * Test of signedInPlan method, of class PagarMeApi.
     */
    @Test
    public void testSignedInPlan() {
        System.out.println("signedInPlan");
        IPlan i1 = null;
        ISubscription i2 = null;
        String API_KEY = "";
        boolean expResult = false;
        boolean result = PagarMeApi.signedInPlan(i1, i2, API_KEY);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPlan method, of class PagarMeApi.
     */
    @Test
    public void testGetPlan() {
        System.out.println("getPlan");
        IPlan i = null;
        String API_KEY = "";
        PagarMeApi.getPlan(i, API_KEY);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSubscription method, of class PagarMeApi.
     */
    @Test
    public void testGetSubscription() {
        System.out.println("getSubscription");
        ISubscription i = null;
        String API_KEY = "";
        PagarMeApi.getSubscription(i, API_KEY);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of createTransaction method, of class PagarMeApi.
     */
    @Test
    public void testCreateTransaction() throws PagarMeException {
        System.out.println("createTransaction");
        ITransaction i = null;
        String API_KEY = "";
        PagarMeApi.createTransaction(i,"" ,API_KEY,null);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of createSubscription method, of class PagarMeApi.
     */
    @Test
    public void testCreateSubscription_3args() {
        System.out.println("createSubscription");
        ISubscription i = null;
        String postbackUrl = "";
        String API_KEY = "";
        PagarMeApi.createSubscription(i, postbackUrl, API_KEY);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of createSubscription method, of class PagarMeApi.
     */
    @Test
    public void testCreateSubscription_4args() {
        System.out.println("createSubscription");
        ISubscription i1 = null;
        ITransaction i2 = null;
        String postbackUrl = "";
        String API_KEY = "";
        PagarMeApi.createSubscription(i1, i2, postbackUrl, API_KEY);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of updateCard method, of class PagarMeApi.
     */
    @Test
    public void testUpdateCard() {
        System.out.println("updateCard");
        ISubscription i = null;
        String API_KEY = "";
        PagarMeApi.updateCard(i, API_KEY);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of cancelSubscription method, of class PagarMeApi.
     */
    @Test
    public void testCancelSubscription() {
        System.out.println("cancelSubscription");
        ISubscription i = null;
        String API_KEY = "";
        PagarMeApi.cancelSubscription(i, API_KEY);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}