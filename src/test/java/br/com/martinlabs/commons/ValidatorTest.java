/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.martinlabs.commons;

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ricardoprado
 */
public class ValidatorTest {
    
    public ValidatorTest() {
    }

    @Test
    public void testFormatCelPhone() {
        String cel = "11971020249";
        String saida = Validator.formatCelPhone(cel);
        Assert.assertEquals("(11)97102-0249", saida);
    }

}
