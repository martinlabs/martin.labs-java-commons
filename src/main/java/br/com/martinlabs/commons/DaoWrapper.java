package br.com.martinlabs.commons;

import br.com.martinlabs.commons.exceptions.RespException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Uma solucao para encapsular o dao, deixando-o mais simples e legivel
 *
 * @author gil
 */
public class DaoWrapper {

    protected DataSource ds;
    protected Connection con;
    private boolean transactional = false;
    private Level logLevel = Level.ALL;
    protected LanguageHolder lang;

    //unique resources for the whole application
    private static Context ctx;
    private static Context envContext;

    @Deprecated
    public DaoWrapper(String dataSourceName) {
        try {

            if (ctx == null) {
                ctx = new InitialContext();
            }

            if (envContext == null) {
                envContext = (Context) ctx.lookup("java:/comp/env");
            }

            if (ds == null) {
                ds = (DataSource) envContext.lookup(dataSourceName);
            }

        } catch (NamingException ex) {
            Logger.getLogger(DaoWrapper.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Deprecated
    public DaoWrapper(String dataSourceName, Level logLevel) {
        this(dataSourceName);
        this.logLevel = logLevel;
    }

    public DaoWrapper(Connection con, LanguageHolder lang) {
        this.con = con;
        this.transactional = true;
        this.lang = lang;
    }
    
    @Deprecated
    public DaoWrapper(Connection con) {
        this(con, LanguageHolder.instance);
    }

    public DaoWrapper(Connection con, Level logLevel) {
        this(con);
        this.logLevel = logLevel;
    }

    /**
     * to insert or make an update
     *
     * @param strStatement your query with ? for the variables
     * @param objStatement variables that will replace the ?
     * @return database response
     */
    protected GenericResult update(String strStatement, Object... objStatement) {

        Connection con = null;
        PreparedStatement statem = null;
        GenericResult genResult = new GenericResult();
        ResultSetWrapper keys = null;
        try {
            con = getConnection();
            statem = prepareUpdate(con, strStatement, objStatement);
            Logger.getLogger(DaoWrapper.class.getName()).log(logLevel, statem.toString());
            genResult.affectedRows = statem.executeUpdate();

            keys = new ResultSetWrapper(statem.getGeneratedKeys());

            if (keys.next()) {
                genResult.key = keys.getLong(1);
            }
            finishOperation(statem, keys, con);
        } catch (Exception ex) {
            finishOperation(statem, keys, con);
            RespException re = new RespException(lang.unexpectedError());
            re.initCause(ex);
            throw re;
        }

        return genResult;

    }

    public static GenericResult updateForTest(Connection con, String strStatement, Object... objStatement) {
        return new DaoWrapper(con).update(strStatement, objStatement);
    }

    /**
     * to make a select
     *
     * @param <T> object type to be constructed by the select
     * @param strStatement your query with ? for the variables
     * @param callback a callback to construct the resulting object with the
     * ResultSet
     * @param objStatement variables that will replace the ?
     * @return constructed object
     */
    protected <T> T select(String strStatement, ResultCallback<T> callback, Object... objStatement) {

        Connection con = null;
        PreparedStatement statem = null;
        ResultSetWrapper rs = null;
        T result = null;

        try {
            con = getConnection();
            statem = prepareSelect(con, strStatement, objStatement);
            Logger.getLogger(DaoWrapper.class.getName()).log(logLevel, statem.toString());
            rs = new ResultSetWrapper(statem.executeQuery());

            result = callback.process(rs);
            finishOperation(statem, rs, con);
        } catch (Exception ex) {
            finishOperation(statem, rs, con);
            RespException re = new RespException(lang.unexpectedError());
            re.initCause(ex);
            throw re;
        }

        return result;

    }

    /**
     * to make a select with an object list
     *
     * @param <T> type of list that will be constructed by the select
     * @param strStatement your query with ? for the variables
     * @param callback a callback to construct each object of the list with the
     * ResultSet
     * @param objStatement variables that will replace the ?
     * @return constructed list
     */
    protected <T> List<T> selectList(String strStatement, final ResultCallback<T> callback, Object... objStatement) {

        return select(strStatement, new ResultCallback<List<T>>() {

            @Override
            public List<T> process(ResultSetWrapper rs) throws SQLException {
                List<T> result = new LinkedList<T>();

                while (rs.next()) {
                    result.add(callback.process(rs));
                }

                return result;

            }

        }, objStatement);

    }

    /**
     * to make a select with a single object
     *
     * @param <T> type of object that will be constructed by the select
     * @param strStatement your query with ? for the variables
     * @param callback a callback to construct each object of the list with the
     * ResultSet
     * @param objStatement variables that will replace the ?
     * @return constructed object
     */
    protected <T> T selectOne(String strStatement, final ResultCallback<T> callback, Object... objStatement) {

        return select(strStatement, new ResultCallback<T>() {

            @Override
            public T process(ResultSetWrapper rs) throws SQLException {

                if (rs.next()) {
                    return callback.process(rs);
                }

                return null;

            }

        }, objStatement);

    }

    /**
     * checks if the query returns any data
     *
     * @param strStatement your query with ? for the variables
     * @param objStatement variables that will replace the ?
     * @return true if there is data in the query
     */
    protected boolean exist(String strStatement, Object... objStatement) {

        return select(strStatement, new ResultCallback<Boolean>() {

            @Override
            public Boolean process(ResultSetWrapper rs) throws SQLException {

                return rs.next();

            }

        }, objStatement);

    }

    public static boolean existForTest(Connection con, String strStatement, Object... objStatement) {
        return new DaoWrapper(con).exist(strStatement, objStatement);
    }

    /**
     * para obter o campo de nome "id"
     *
     * @param strStatement your query with ? for the variables
     * @param objStatement variables that will replace the ?
     * @return long do id selecionado
     */
    @Deprecated
    protected Long selectId(String strStatement, Object... objStatement) {

        return selectOne(strStatement, new ResultCallback<Long>() {

            @Override
            public Long process(ResultSetWrapper rs) throws SQLException {

                return rs.getLong("id");

            }

        }, objStatement);

    }

    protected Integer selectFirstInt(String strStatement, Object... objStatement) {

        return selectOne(strStatement, new ResultCallback<Integer>() {

            @Override
            public Integer process(ResultSetWrapper rs) throws SQLException {
                return rs.getIntOrNull(1);
            }

        }, objStatement);

    }

    protected Long selectFirstLong(String strStatement, Object... objStatement) {

        return selectOne(strStatement, new ResultCallback<Long>() {

            @Override
            public Long process(ResultSetWrapper rs) throws SQLException {
                return rs.getLongOrNull(1);
            }

        }, objStatement);

    }

    protected Double selectFirstDouble(String strStatement, Object... objStatement) {

        return selectOne(strStatement, new ResultCallback<Double>() {

            @Override
            public Double process(ResultSetWrapper rs) throws SQLException {
                return rs.getDoubleOrNull(1);
            }

        }, objStatement);

    }

    protected String selectFirstString(String strStatement, Object... objStatement) {

        return selectOne(strStatement, new ResultCallback<String>() {

            @Override
            public String process(ResultSetWrapper rs) throws SQLException {
                return rs.getString(1);
            }

        }, objStatement);

    }

    protected Date selectFirstDate(String strStatement, Object... objStatement) {

        return selectOne(strStatement, new ResultCallback<Date>() {

            @Override
            public Date process(ResultSetWrapper rs) throws SQLException {
                return rs.getTimestamp(1);
            }

        }, objStatement);

    }

    protected Boolean selectFirstBoolean(String strStatement, Object... objStatement) {

        return selectOne(strStatement, new ResultCallback<Boolean>() {

            @Override
            public Boolean process(ResultSetWrapper rs) throws SQLException {
                return rs.getBooleanOrNull(1);
            }

        }, objStatement);

    }
    
    protected GenericResult insertWithMap(String tablename, Map<String, Object> columnNamesAndValues) {
        String[] columnNames = columnNamesAndValues.keySet().toArray(new String[columnNamesAndValues.size()]);
        
        return update("INSERT INTO " 
                + tablename
                + " ( "
                + stringifyInsertColumns(columnNames)
                + ") VALUES "
                + generateInsertCommas(1, columnNamesAndValues.size()),
                columnNamesAndValues.values().toArray());
    }
    
    protected GenericResult updateWithMap(String tablename, Map<String, Object> columnNamesAndValues, String clause, Object...params) {
        String[] columnNames = columnNamesAndValues.keySet().toArray(new String[columnNamesAndValues.size()]);
        ArrayList values = new ArrayList(columnNamesAndValues.values());
        values.addAll(Arrays.asList(params));
        
        return update("UPDATE " 
                + tablename 
                + " SET "
                + stringifyUpdateColumns(columnNames)
                + clause,
                values.toArray());
    }

    /**
     * gets a connection
     *
     * @return a conexão
     * @throws SQLException
     */
    protected Connection getConnection() throws SQLException {
        if (transactional) {
            return con;
        } else {
            return ds.getConnection();
        }
    }

    /**
     * finish the operation closing the statement, resultset and connection if
     * it is not transactional
     *
     * @param pstmt
     * @param rs
     * @param con
     */
    protected void finishOperation(PreparedStatement pstmt, ResultSetWrapper rs, Connection con) {
        if (transactional) {
            closeStatementAndResult(pstmt, rs);
        } else {
            closeAll(pstmt, rs, con);
        }
    }

    /**
     * prepare the statement
     *
     * @param con connection to be used
     * @param strStatement your query with ? for the variables
     * @param objStatement variables that will replace the ?
     * @return the prepared statement
     * @throws SQLException if (1) couldn't get the connection; (2) coudn't
     * prepare the statement; (3) coudn't understand the object
     */
    protected PreparedStatement prepareUpdate(Connection con, String strStatement, Object... objStatement) throws SQLException {
        PreparedStatement statem = con.prepareStatement(strStatement, Statement.RETURN_GENERATED_KEYS);

        int i = 1;
        for (Object o : objStatement) {

            if (o != null && o.getClass() == java.util.Date.class) {
                o = new java.sql.Timestamp(((java.util.Date) o).getTime());
            }

            statem.setObject(i, o);
            i++;
        }

        return statem;
    }

    /**
     * prepara prepare the statement to select
     *
     * @param con connection to be used
     * @param strStatement your query with ? for the variables
     * @param objStatement variables that will replace the ?
     * @return the prepared statement
     * @throws SQLException if (1) couldn't get the connection; (2) coudn't
     * prepare the statement; (3) coudn't understand the object
     *
     */
    protected PreparedStatement prepareSelect(Connection con, String strStatement, Object... objStatement) throws SQLException {
        PreparedStatement statem = con.prepareStatement(strStatement);

        int i = 1;
        for (Object o : objStatement) {

            if (o != null && o.getClass() == java.util.Date.class) {
                o = new java.sql.Timestamp(((java.util.Date) o).getTime());
            }

            statem.setObject(i, o);
            i++;
        }

        return statem;
    }

    /**
     * closes the statement, resultset and connection
     *
     * @param pstmt
     * @param rs
     * @param con
     */
    protected void closeAll(PreparedStatement pstmt, ResultSetWrapper rs, Connection con) {

        if (rs != null) {
            try {
                rs.close();
            } catch (Exception ex) {
                try {
                    Logger.getLogger(DaoWrapper.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                } catch (Exception ex2) {
                }
            }
        }

        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (Exception ex) {
                try {
                    Logger.getLogger(DaoWrapper.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                } catch (Exception ex2) {
                }
            }
        }

        if (con != null) {
            try {
                if (!con.isClosed()) {
                    con.close();
                }
            } catch (Exception ex) {
                try {
                    Logger.getLogger(DaoWrapper.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                } catch (Exception ex2) {
                }
            }
        }
    }

    /**
     * closes the statement and resultset
     *
     * @param pstmt
     * @param rs
     */
    protected void closeStatementAndResult(PreparedStatement pstmt, ResultSetWrapper rs) {

        if (rs != null) {
            try {
                rs.close();
            } catch (Exception ex) {
                try {
                    Logger.getLogger(DaoWrapper.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                } catch (Exception ex2) {
                }
            }
        }

        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (Exception ex) {
                try {
                    Logger.getLogger(DaoWrapper.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                } catch (Exception ex2) {
                }
            }
        }
    }

    protected boolean nullToFalse(Boolean obj) {
        return obj != null ? obj : false;
    }

    protected int nullToZero(Integer obj) {
        return obj != null ? obj : 0;
    }

    protected long nullToZero(Long obj) {
        return obj != null ? obj : 0;
    }

    protected double nullToZero(Double obj) {
        return obj != null ? obj : 0;
    }

    protected float nullToZero(Float obj) {
        return obj != null ? obj : 0;
    }

    protected String generateInsertCommas(int lines, int columns) {
        StringBuilder sb = new StringBuilder();

        sb.append("(");

        for (int j = 0; j < columns; j++) {
            if (j > 0) {
                sb.append(", ");
            }

            sb.append("?");
        }

        sb.append(")");

        String l = sb.toString();
        sb.setLength(0);

        for (int i = 0; i < lines; i++) {
            if (i > 0) {
                sb.append(", ");
            }

            sb.append(l);
        }

        return sb.toString();
    }

    protected String stringifyUpdateColumns(String[] columns) {
        StringBuilder sb = new StringBuilder();

        for (int j = 0; j < columns.length; j++) {
            if (j > 0) {
                sb.append(", ");
            }

            sb.append(columns[j]);
            sb.append(" = ?");
        }
        
        sb.append(" ");

        return sb.toString();
    }
    
    protected String stringifyInsertColumns(String[] columns) {
        return String.join(",", columns) + " ";
    }

    /**
     * used to build the object from a ResultSet
     *
     * @param <T> type of the object
     */
    public interface ResultCallback<T> {

        public T process(ResultSetWrapper rs) throws SQLException;

    }

    /**
     * result of the update. affectedRows is the amount of affected rows; key id
     * the id of the insert;
     */
    public class GenericResult {

        public int affectedRows;
        public long key;
    }

}
