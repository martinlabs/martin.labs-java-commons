package br.com.martinlabs.commons;

import br.com.martinlabs.commons.exceptions.RespException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A solution to encapsulate the servlet, using a ServletContent and
 * putting the result in an OpResp
 * @author gil
 */
public abstract class OpServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        
        ServletContent content = createServletContent();
        
        content.context = getServletContext();
        
        content.request = req;
        content.request.setCharacterEncoding(content.charset);
        
        content.response = res;

        content.response.setContentType(content.contentType);
        PrintWriter out = content.response.getWriter();

        Object result = null;
        try {
            //Tentar processar
            result = process(content);
            
            if (!(result instanceof OpResp)) {
                //se não é OpResp vamos coloca-lo dentro de um
                result = new OpResp(result);
            }

        } catch (RespException re) {
            Logger.getLogger(ServletWrapper.class.getName()).log(Level.INFO, re.getMessage(), re);
            
            result = new OpResp (false, re.getMessage());

        } catch (Throwable e) {
            Logger.getLogger(ServletWrapper.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            
            result = new OpResp(false, LanguageHolder.instance.unexpectedError());
            
        } finally {
            out.print(content.gson.toJson(result));
            out.close();
        }
    }
    
    protected abstract Object process(ServletContent content);

    protected ServletContent createServletContent() {
        return new ServletContent();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
