package br.com.martinlabs.commons;

import com.goebl.david.Webb;
import com.google.gson.Gson;

/**
 *
 * @author ricardoprado
 */
public class SlackUtils {
    
    
    public void publishInChannel(String message, String url){
        Webb webb = Webb.create();
        Gson gson = new Gson();
        Payload payload = new Payload(message);
        webb.post(url).param("payload", gson.toJson(payload)).ensureSuccess().asVoid();
    }
    
    @Deprecated
    public void publicarEmCanal(String message, String url){
        publishInChannel(message, url);
    }

    private class Payload{

        private String text;

        public Payload(String text) {
            this.text = text;
        }

    }
}
