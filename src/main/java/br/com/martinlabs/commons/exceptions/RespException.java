package br.com.martinlabs.commons.exceptions;

import br.com.martinlabs.commons.OpResponse;

/**
 *
 * @author gil
 */
public class RespException extends RuntimeException {
    private final OpResponse operationResponse;

    public RespException(String text) {
        super(text);
        operationResponse = new OpResponse(false, text);
    }
    
    public RespException(Integer code, String text) {
        super(text);
        operationResponse = new OpResponse(false, text);
        operationResponse.setCode(code);
    }

    @Deprecated
    public RespException(String text, Object details) {
        super(text);
        operationResponse = new OpResponse(false, text, details);
    }
    
    @Deprecated
    public OpResponse getOperationResponse() {
        return operationResponse;
    }

    @Override
    public String getMessage() {
        return operationResponse.getMessage(); 
    }

    @Override
    public String getLocalizedMessage() {
        return operationResponse.getMessage(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Integer getCode() {
        return operationResponse.getCode(); 
    }
    
    
    
}
