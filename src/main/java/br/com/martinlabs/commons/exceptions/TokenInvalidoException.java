/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.martinlabs.commons.exceptions;

/**
 *
 * @author developer-a
 */
@Deprecated
public class TokenInvalidoException extends RespException {

        public TokenInvalidoException() {
            super("Sessão expirada");
            getOperationResponse().setCode(33);
        }

    }
