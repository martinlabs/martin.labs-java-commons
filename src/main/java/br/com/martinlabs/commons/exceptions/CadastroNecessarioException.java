package br.com.martinlabs.commons.exceptions;

/**
 *
 * @author Koba
 */
@Deprecated
public class CadastroNecessarioException extends RespException {

    public CadastroNecessarioException() {
        super("É necessário criar uma conta.");
        getOperationResponse().setCode(77);
    }
}
