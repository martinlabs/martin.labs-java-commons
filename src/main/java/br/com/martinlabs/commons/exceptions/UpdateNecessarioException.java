/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.martinlabs.commons.exceptions;

/**
 *
 * @author Koba
 */
@Deprecated
public class UpdateNecessarioException extends RespException {

    public UpdateNecessarioException() {
        super("É necessário atualizar o aplicativo.");
        getOperationResponse().setCode(165);
    }
}
