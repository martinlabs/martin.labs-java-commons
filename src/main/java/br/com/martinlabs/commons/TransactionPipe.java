package br.com.martinlabs.commons;

import br.com.martinlabs.commons.exceptions.RespException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author gil
 */
public class TransactionPipe {
    
    private DataSource ds;
    
    private static Context ctx;
    private static Context envContext;
    
    public TransactionPipe(String dsName) {
    
        try {
            if (ctx == null) {
                ctx = new InitialContext();
            }

            if (envContext == null) {
                envContext = (Context) ctx.lookup("java:/comp/env");
            }

            ds = (DataSource) envContext.lookup(dsName);
        
        } catch (NamingException ex) {
            Logger.getLogger(TransactionPipe.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public <T> T handle(Trans<T> c){
        return handle(c, true);
    }

    public <T> T handle(Trans<T> c, boolean useTransaction){
        T result;
        Connection con;
            
        try {
            con = ds.getConnection();
            con.setAutoCommit(!useTransaction);
        } catch (SQLException ex) {
            Logger.getLogger(TransactionPipe.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            throw new RespException(new EnglishLanguage().unexpectedError());
        }
        
        try {
            result = c.action(con);
            commitAndFinish(con, useTransaction);
        } catch (Throwable e) {
            rollbackAndFinish(con, useTransaction);
            throw e;
        }
        
        return result;
    }
    
    public static void commit(Connection con) {
        if (con != null) {
            try {
                con.commit();
            } catch (SQLException ex) {
                Logger.getLogger(TransactionPipe.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }
    
    public static void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                Logger.getLogger(TransactionPipe.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }
    
    public static void finish(Connection con) {
        if (con != null) {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(TransactionPipe.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }
    
    public static void commitAndFinish(Connection con, boolean useTransaction) {
        if (useTransaction)
            commit(con);

        finish(con);
    }
    
    public static void rollbackAndFinish(Connection con, boolean useTransaction) {
        if (useTransaction)
            rollback(con);

        finish(con);
    }
    
    public interface Trans<T> {
        public T action(Connection con);
    }

}
