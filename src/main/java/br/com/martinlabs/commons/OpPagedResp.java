package br.com.martinlabs.commons;

/**
 *
 * @author gil
 */
public class OpPagedResp<T> extends OpResp<T> {
    private int recordsTotal;
    private int recordsFiltered;

    public OpPagedResp() {
    }

    public OpPagedResp(T Data) {
        super(Data);
    }

    public OpPagedResp(boolean success, String message) {
        super(success, message);
    }

    public OpPagedResp(boolean success, String message, T details) {
        super(success, message, details);
    }

    public int getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(int RecordsTotal) {
        this.recordsTotal = RecordsTotal;
    }

    public int getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(int RecordsFiltered) {
        this.recordsFiltered = RecordsFiltered;
    }
}
