package br.com.martinlabs.commons.iugu;

import java.util.List;

/**
 *
 * @author Kobayashi
 */
public class IuguClientAccount {
    private String idClient;
    private List<IuguPayment> payments;

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public List<IuguPayment> getPayments() {
        return payments;
    }

    public void setPayments(List<IuguPayment> payments) {
        this.payments = payments;
    }
}
