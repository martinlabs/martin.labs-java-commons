package br.com.martinlabs.commons.iugu;

/**
 *
 * @author Kobayashi
 */
public class IuguPedidoSaqueRequest extends IuguRequest {
    private double amount;

    public IuguPedidoSaqueRequest(double amount, String api_token) {
        super(api_token);
        this.amount = amount;
    }
    
    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
