package br.com.martinlabs.commons.iugu;

/**
 *
 * @author Kobayashi
 */
public class IuguBankVerificationRequest extends IuguRequest {
    private String agency;
    private String account;
    private String account_type;
    private String bank;
    private String automatic_validation;
    
    public IuguBankVerificationRequest(String agency, String account, String account_type, String bank, String api_token) {
        super(api_token);
        this.agency = agency;
        this.account = account;
        this.account_type = account_type;
        this.bank = bank;
        this.automatic_validation = "false";
    }
    
    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String isAutomatic_validation() {
        return automatic_validation;
    }

    public void setAutomatic_validation(String automatic_validation) {
        this.automatic_validation = automatic_validation;
    }
}
