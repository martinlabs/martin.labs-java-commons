package br.com.martinlabs.commons.iugu;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Kobayashi
 */
public class IuguRequest {
    private String api_token;
    private String method;
    private String token = null;
    private List<IuguItem> items;
    private String email;
    private IuguPayer payer;
    private int bank_slip_extra_days;

    public IuguRequest(String api_token) {
        this.api_token = api_token;
    }

    public IuguRequest(String api_token, String email) {
        this.api_token = api_token;
        this.email = email;
    }
    
    /**
     * para boleto
     * @param api_token
     * @param items 
     */
    public IuguRequest(String api_token, IuguPayer payer, List<IuguItem> items) {
        this(api_token, payer.email);
        this.payer = payer;
        this.method = "bank_slip";
        this.items = items;
    }

    public int getBank_slip_extra_days() {
        return bank_slip_extra_days;
    }

    public void setBank_slip_extra_days(int bank_slip_extra_days) {
        this.bank_slip_extra_days = bank_slip_extra_days;
    }


    /**
     * boleto
     * @param api_token
     * @param item 
     */
    public IuguRequest(String api_token, IuguPayer payer, IuguItem item) {
        this(api_token, payer, Arrays.asList(item));
    }
    
    /**
     * cartao de credito
     * @param api_token
     * @param email do cliente
     * @param token do cliente
     * @param items 
     */
    public IuguRequest(String api_token, String email, String token, List<IuguItem> items) {
        this(api_token, email);
        this.token = token;
        this.items = items;
    }
    
    /**
     * cartão de credito
     * @param api_token
     * @param email do cliente
     * @param token do cliente
     * @param item 
     */
    public IuguRequest(String api_token, String email, String token, IuguItem item) {
        this(api_token, email, token, Arrays.asList(item));
    }

    public static class IuguItem {

        String description;
        String quantity;
        String price_cents;

        public IuguItem(String description, String quantity, String price_cents) {
            this.description = description;
            this.quantity = quantity;
            this.price_cents = price_cents;
        }

        public IuguItem(String description, int quantity, int price_cents) {
            this(description, quantity + "", price_cents + "");
        }
        
        
    }

    public static class IuguPayer {

        String email;
        String cpf_cnpj;
        String name;
        String phone_prefix;
        String phone;
        IuguAddress address;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCpf_cnpj() {
            return cpf_cnpj;
        }

        public void setCpf_cnpj(String cpf_cnpj) {
            this.cpf_cnpj = cpf_cnpj;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone_prefix() {
            return phone_prefix;
        }

        public void setPhone_prefix(String phone_prefix) {
            this.phone_prefix = phone_prefix;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public IuguAddress getAddress() {
            return address;
        }

        public void setAddress(IuguAddress address) {
            this.address = address;
        }
        
        
        
    }

    public static class IuguAddress {

        String street;//	Rua
        String number;//	Número
        String city;//          Cidade
        String district;//      Bairro
        String state;//         Estado (Ex: SP)
        String country;//	País
        String zip_code;//	CEP
        String complement;//	Complemento

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getZip_code() {
            return zip_code;
        }

        public void setZip_code(String zip_code) {
            this.zip_code = zip_code;
        }

        public String getComplement() {
            return complement;
        }

        public void setComplement(String complement) {
            this.complement = complement;
        }
        
        
    }
}
