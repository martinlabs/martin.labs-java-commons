package br.com.martinlabs.commons.iugu;

import br.com.martinlabs.commons.exceptions.RespException;
import com.goebl.david.Webb;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kobayashi
 */
public abstract class IuguProcess {
    private static String IUGU_TOKEN;
    private static boolean IS_PRODUCTION;
    private static String IUGU_MASTER_ID;
    private static String NAME_IN_CARD;
    
    public static void initialize(String tokenProduction, String tokenTest, String iuguMasterID, String nameInCard, Boolean isProduction) {
        IUGU_MASTER_ID = iuguMasterID;
        NAME_IN_CARD = nameInCard;
        IS_PRODUCTION = isProduction;
        if (IS_PRODUCTION)
            IUGU_TOKEN = tokenProduction;
        else
            IUGU_TOKEN = tokenTest;
    }
    
    public static IuguClientAccount createAccountVerification(String email, String name) throws RespException {
        IuguClientAccount account = new IuguClientAccount();
        String idCliente = verifyAccount(email);
        if (idCliente == null) {
            idCliente = createAccount(email, name);
        }
        if (idCliente == null)
            throw new RespException("Erro ao tentar realizar o login, tente novamente mais tarde");
        
        List<IuguPayment> lstPayments = listPayments(idCliente);
        account.setIdClient(idCliente);
        account.setPayments(lstPayments);
        
        return account;
    } 
    
    public static IuguClientAccount createFullAccountVerification(String email, String name,String doc,String zipcode, 
            String number,String street,String city,String state, String district) throws RespException {
        IuguClientAccount account = new IuguClientAccount();
        
        
          String idCliente = verifyAccountV2(email);
        
        if (idCliente == null) {
            idCliente = createFullAccount(email, name, doc, zipcode, number, street, city, state, district);
        }
        if (idCliente == null)
            throw new RespException("Erro ao tentar realizar o login, tente novamente mais tarde");
        
        List<IuguPayment> lstPayments = listPayments(idCliente);
        account.setIdClient(idCliente);
        account.setPayments(lstPayments);
        
        return account;
    }   
    
    public static IuguPayment createNewPayment(String idCliente, IuguNewPayment payment, boolean isFirstCard) throws RespException {
        if (payment == null)
            throw new RespException("Dados inválidos");
        
        payment.setNumber(payment.getNumber().replace(" ", ""));
        if (payment.getNumber() == null || payment.getNumber().trim().length() < 13)
            throw new RespException("Número do cartão inválido");
        if (payment.getVerification_value() == null || payment.getVerification_value().trim().length() < 3)
            throw new RespException("CVV inválido");
        if (payment.getMonth() == null || payment.getMonth().trim().length() != 2)
            throw new RespException("Mês do cartão inválido");
        if (payment.getMonth() == null || payment.getMonth().trim().length() != 2)
            throw new RespException("Por favor, preencha o mês do vencimento do cartão com 2 digitos");
        if (payment.getYear() == null || payment.getYear().trim().length() != 4)
            throw new RespException("Por favor, preencha o ano do vencimento do cartão com 4 digitos");
        if (payment.getFirst_name() == null || payment.getFirst_name().trim().length() < 1) 
            throw new RespException("Por favor, informe o primeiro nome descrito no cartão");
        if (payment.getLast_name() == null || payment.getLast_name().trim().length() < 1) 
            throw new RespException("Por favor, informe o último nome descrito no cartão");
        
        String tokenPayment = createPaymentToken(payment);
        if (tokenPayment == null) 
            throw new RespException("Erro ao criar token para o cartão, tente novamente mais tarde");  
            
        IuguPayment iuguNewPayment = createPayment(idCliente, tokenPayment, isFirstCard);
        if (iuguNewPayment == null) 
            throw new RespException("Erro ao cadastrar o cartão, tente novamente mais tarde");  
            
        return iuguNewPayment;
    }
    
    public static IuguPayment alterPayment(String idCliente, String idPayment,
            IuguNewPayment payment, String itemType, boolean defaultCard)
    {
        if (payment == null)
            throw new RespException("Dados inválidos");
        
        payment.setNumber(payment.getNumber().replace(" ", ""));
        if (payment.getNumber() == null || payment.getNumber().trim().length() < 13)
            throw new RespException("Número do cartão inválido");
        if (payment.getVerification_value() == null || payment.getVerification_value().trim().length() < 3)
            throw new RespException("CVV inválido");
        if (payment.getMonth() == null || payment.getMonth().trim().length() != 2)
            throw new RespException("Mês do cartão inválido");
        if (payment.getMonth() == null || payment.getMonth().trim().length() != 2)
            throw new RespException("Por favor, preencha o mês do vencimento do cartão com 2 digitos");
        if (payment.getYear() == null || payment.getYear().trim().length() != 4)
            throw new RespException("Por favor, preencha o ano do vencimento do cartão com 4 digitos");
        if (payment.getFirst_name() == null || payment.getFirst_name().trim().length() < 1) 
            throw new RespException("Por favor, informe o primeiro nome descrito no cartão");
        if (payment.getLast_name() == null || payment.getLast_name().trim().length() < 1) 
            throw new RespException("Por favor, informe o último nome descrito no cartão");
        
        return AlterPayment(idCliente,idPayment, payment,itemType ,defaultCard);
    }
    
    public static IuguPayment createNewPaymentV2(String idCliente, 
            IuguNewPayment payment, String itemType, boolean defaultCard) throws RespException {
        if (payment == null)
            throw new RespException("Dados inválidos");
        
        payment.setNumber(payment.getNumber().replace(" ", ""));
        if (payment.getNumber() == null || payment.getNumber().trim().length() < 13)
            throw new RespException("Número do cartão inválido");
        if (payment.getVerification_value() == null || payment.getVerification_value().trim().length() < 3)
            throw new RespException("CVV inválido");
        if (payment.getMonth() == null || payment.getMonth().trim().length() != 2)
            throw new RespException("Mês do cartão inválido");
        if (payment.getMonth() == null || payment.getMonth().trim().length() != 2)
            throw new RespException("Por favor, preencha o mês do vencimento do cartão com 2 digitos");
        if (payment.getYear() == null || payment.getYear().trim().length() != 4)
            throw new RespException("Por favor, preencha o ano do vencimento do cartão com 4 digitos");
        if (payment.getFirst_name() == null || payment.getFirst_name().trim().length() < 1) 
            throw new RespException("Por favor, informe o primeiro nome descrito no cartão");
        if (payment.getLast_name() == null || payment.getLast_name().trim().length() < 1) 
            throw new RespException("Por favor, informe o último nome descrito no cartão");
        
        return createPayment(idCliente, payment,itemType ,defaultCard);
    }
    
    public static IuguSubconta createSubAccount(String name, Double comission) throws RespException {
        if (name == null)
            throw new RespException("Dados inválidos");
        
        IuguSubcontaRequest request = new IuguSubcontaRequest(IUGU_TOKEN, comission);
        request.setName(name);
        Webb requestMaker = Webb.create();
        Gson gson = new Gson();
        IuguSubconta response = null;
        String json = gson.toJson(request);
        
        String resposta = requestMaker.post("https://api.iugu.com/v1/marketplace/create_account")
                .body(json)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        IuguSubconta iuguSubconta = gson.fromJson(resposta, IuguSubconta.class);
        
        if (iuguSubconta == null) 
            throw new RespException("Erro ao cadastrar o subconta do IUGU, tente novamente mais tarde");  
            
        return iuguSubconta;
    }

    public static JsonObject verifySubAccountStatus(IuguSubconta subaccount) throws RespException {
        Webb requestMaker = Webb.create();
        JsonObject response = null;
        String resposta = requestMaker.get("https://api.iugu.com/v1/accounts/" + subaccount.getAccount_id() + 
                "?api_token=" + subaccount.getUser_token())
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();

        response = new JsonParser().parse(resposta).getAsJsonObject();

        return response;
    }
    
    public static JsonObject updateBankData(IuguBankVerificationRequest request) throws RespException {
        if (request == null) {
            throw new RespException("Dados inválidos");
        }
        return changeBankInformations(request);
    }
            
    public static List<IuguBankVerificationStatus> listBankVerifications(String liveToken) throws RespException {
        Webb requestMaker = Webb.create();
        List<IuguBankVerificationStatus> response = null;
        String resposta = requestMaker.get("https://api.iugu.com/v1/bank_verification?api_token=" + liveToken)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        
        response = new Gson().fromJson(resposta, new TypeToken<ArrayList<IuguBankVerificationStatus>>(){}.getType());
        return response;
    }
    
    public static IuguTransferResponse transferToSubAccount(String account, double value) throws RespException{
        if (account == null || account.trim().length() == 0)
            throw new RespException("Conta destino inválido");
        if (value <= 0)
            throw new RespException("Valor para transferência inválida");
        
        long precoTratado = 0;
        precoTratado = Math.round(value * 100);
        IuguTransferRequest request = new IuguTransferRequest(IUGU_TOKEN, account, precoTratado);
        Webb requestMaker = Webb.create();
        Gson gson = new Gson();
        IuguTransferResponse response = null;
        String json = gson.toJson(request);
        String resposta = requestMaker.post("https://api.iugu.com/v1/transfers")
                .body(json)
                .header("content-type", "application/json")
                .ensureSuccess().asString().getBody();
        response = gson.fromJson(resposta, IuguTransferResponse.class);
        return response;
    }
    
    public static void removePayment(String clientId, String idPayment) throws RespException {
        if (clientId == null || idPayment == null)
            throw new RespException("Dados inválidos");
        
        String id = removerPayment(clientId, idPayment);
        if (id == null)
            throw new RespException("Erro ao tentar remover esta forma de pagamento, tente novamente mais tarde");
    }
    
    public static void updateDefaultPayment(String clientId, String idPayment) throws RespException {
        if (clientId == null || idPayment == null)
            throw new RespException("Dados inválidos");
        
        String id;
        
        Webb requestMaker = Webb.create();
        IuguChangeClientRequest request = new IuguChangeClientRequest(IUGU_TOKEN, clientId, idPayment);
        JsonObject response = null;
        Gson gson = new Gson();
        String json = gson.toJson(request);
        String resposta = requestMaker.put("https://api.iugu.com/v1/customers/" + clientId)
                .body(json)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        
        response = new JsonParser().parse(resposta).getAsJsonObject();
        
        if (response != null && response.get("id") != null && 
                response.get("id").getAsString() != null && response.get("id").getAsString().trim().length() > 0) {
            id = response.get("id").getAsString();
        }
        else {
            if (response != null && response.get("errors") != null && 
                    response.get("errors").getAsString() != null && response.get("errors").getAsString().trim().length() > 0) {
                throw new RespException(response.get("errors").getAsString());
            }
            else {
                id = null;
            }
        } 
        
        if (id == null)
            throw new RespException("Erro ao tentar selecionar esta forma de pagamento como principal, tente novamente mais tarde");        
    }
    
    public static String createSubscription(String clientId, String planIdentifier) throws RespException {
        if (clientId == null || planIdentifier == null)
            throw new RespException("Dados inválidos");
             
        String id;
        
        Webb requestMaker = Webb.create();
        IuguSubscriptionRequest request = new IuguSubscriptionRequest(IUGU_TOKEN, planIdentifier, clientId);
        JsonObject response = null;
        Gson gson = new Gson();
        String json = gson.toJson(request);
        String resposta = requestMaker.post("https://api.iugu.com/v1/subscriptions")
                .body(json)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        
        response = new JsonParser().parse(resposta).getAsJsonObject();
        
        if (response != null && response.get("id") != null && 
                response.get("id").getAsString() != null && response.get("id").getAsString().trim().length() > 0) {
            id = response.get("id").getAsString();
        }
        else {
            if (response != null && response.get("errors") != null && 
                    response.get("errors").getAsString() != null && response.get("errors").getAsString().trim().length() > 0) {
                throw new RespException(response.get("errors").getAsString());
            }
            else {
                id = null;
            }
        } 
        
        if (id == null)
            throw new RespException("Erro ao tentar assinar o plano, tente novamente mais tarde");  
        
        return id;
    }
    
    public static IuguAssinatura createSubscriptionV2(String clientId, String planIdentifier) throws RespException 
    {
        if (clientId == null || planIdentifier == null)
            throw new RespException("Dados inválidos");
             
        Webb requestMaker = Webb.create();
        IuguSubscriptionRequest request = new IuguSubscriptionRequest(IUGU_TOKEN, planIdentifier, clientId);
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        String json = gson.toJson(request);
        String resposta = requestMaker.post("https://api.iugu.com/v1/subscriptions")
                .body(json)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        
         IuguAssinatura iuguAssinatura = gson.fromJson(resposta, IuguAssinatura.class);
        
        if (iuguAssinatura == null) 
            throw new RespException("Erro ao cadastrar o subconta do IUGU, tente novamente mais tarde"); 
        
        return iuguAssinatura;
    }
    
    public static IuguAssinatura activateSubscriptionV2(String subscribeId) throws RespException {
        if (subscribeId == null)
            throw new RespException("Dados inválidos");
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Webb requestMaker = Webb.create();
        IuguRequest request = new IuguRequest(IUGU_TOKEN);
        JsonObject response = null;
        String json = gson.toJson(request);
        String url = "https://api.iugu.com/v1/subscriptions/" + subscribeId +"/activate";
        String resposta = requestMaker.post(url)
                .body(json)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        
        IuguAssinatura iuguAssinatura = gson.fromJson(resposta, IuguAssinatura.class);
        
        if (iuguAssinatura == null) 
            throw new RespException("Erro ao ativar Assinatura, tente novamente mais tarde"); 
        
        return  iuguAssinatura;
    }
    
    public static IuguAssinatura getSubscription(String subscriptionId) throws RespException 
    {
        if (subscriptionId == null)
            throw new RespException("Dados inválidos");
             
        Webb requestMaker = Webb.create();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        
        String url = "https://api.iugu.com/v1/subscriptions/" + subscriptionId + "?api_token=" + IUGU_TOKEN;
        String resposta = requestMaker.get(url)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        
         IuguAssinatura iuguAssinatura = gson.fromJson(resposta, IuguAssinatura.class);
        
        if (iuguAssinatura == null) 
            throw new RespException("Erro ao obter Assinatura, tente mais tarde"); 
        
        return iuguAssinatura;
    }
    
    public static String activateSubscription(String subscribeId) throws RespException {
        if (subscribeId == null)
            throw new RespException("Dados inválidos");
             
        String id;
        
        Webb requestMaker = Webb.create();
        IuguRequest request = new IuguRequest(IUGU_TOKEN);
        JsonObject response = null;
        Gson gson = new Gson();
        String json = gson.toJson(request);
        String resposta = requestMaker.post("https://api.iugu.com/v1/subscriptions/" + subscribeId +"/activate")
                .body(json)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        
        response = new JsonParser().parse(resposta).getAsJsonObject();
        
        if (response != null && response.get("id") != null && 
                response.get("id").getAsString() != null && response.get("id").getAsString().trim().length() > 0) {
            id = response.get("id").getAsString();
        }
        else {
            if (response != null && response.get("errors") != null && 
                    response.get("errors").getAsString() != null && response.get("errors").getAsString().trim().length() > 0) {
                throw new RespException(response.get("errors").getAsString());
            }
            else {
                id = null;
            }
        } 
        
        if (id == null)
            throw new RespException("Erro ao tentar assinar o plano, tente novamente mais tarde");  
        
        return id;
    }
    
    public static void cancelSubscription(String idSubscription) throws RespException {
        if (idSubscription == null)
            throw new RespException("Dados inválidos");
             
        String id;
        
        Webb requestMaker = Webb.create();
        JsonObject response = null;
        String resposta = requestMaker.delete("https://api.iugu.com/v1/subscriptions/" + idSubscription + "?api_token=" + IUGU_TOKEN)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        
        response = new JsonParser().parse(resposta).getAsJsonObject();
        
        if (response != null && response.get("id") != null && 
                response.get("id").getAsString() != null && response.get("id").getAsString().trim().length() > 0) {
            id = response.get("id").getAsString();
        }
        else {
            if (response != null && response.get("errors") != null && 
                    response.get("errors").getAsString() != null && response.get("errors").getAsString().trim().length() > 0) {
                throw new RespException(response.get("errors").getAsString());
            }
            else {
                id = null;
            }
        }
        
        
        if (id == null)
            throw new RespException("Erro ao tentar cancelar o plano, tente novamente mais tarde");  
    }
    
    public static void suspendSubscription(String idSubscription) throws RespException {
        if (idSubscription == null)
            throw new RespException("Dados inválidos");
             
        String id;
        
        Webb requestMaker = Webb.create();
        JsonObject response = null;
        String url = "https://api.iugu.com/v1/subscriptions/" + idSubscription + "/suspend" 
                + "?api_token=" + IUGU_TOKEN;
        String resposta = requestMaker.post(url)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        
        response = new JsonParser().parse(resposta).getAsJsonObject();
        
        if (response != null && response.get("id") != null && 
                response.get("id").getAsString() != null && response.get("id").getAsString().trim().length() > 0) {
            id = response.get("id").getAsString();
        }
        else {
            if (response != null && response.get("errors") != null && 
                    response.get("errors").getAsString() != null && response.get("errors").getAsString().trim().length() > 0) {
                throw new RespException(response.get("errors").getAsString());
            }
            else {
                id = null;
            }
        }
        
        
        if (id == null)
            throw new RespException("Erro ao tentar cancelar o plano, tente novamente mais tarde");  
    }
    
    public static void updateSubscriptionPlan(String signatureId, String planIdentifier) throws RespException {
        if (signatureId == null || planIdentifier == null)
            throw new RespException("Dados inválidos");
             
        String id;
        
        Webb requestMaker = Webb.create();
        IuguChangePlanRequest request = new IuguChangePlanRequest(IUGU_TOKEN, signatureId, planIdentifier);
        JsonObject response = null;
        Gson gson = new Gson();
        String json = gson.toJson(request);
        String resposta = requestMaker.post("https://api.iugu.com/v1/subscriptions/" + signatureId + "/change_plan/" + planIdentifier)
                .body(json)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        
        response = new JsonParser().parse(resposta).getAsJsonObject();
        
        if (response != null && response.get("id") != null && 
                response.get("id").getAsString() != null && response.get("id").getAsString().trim().length() > 0) {
            id = response.get("id").getAsString();
        }
        else {
            if (response != null && response.get("errors") != null && 
                    response.get("errors").getAsString() != null && response.get("errors").getAsString().trim().length() > 0) {
                throw new RespException(response.get("errors").getAsString());
            }
            else {
                id = null;
            }
        }  
        
        if (id == null)
            throw new RespException("Erro ao tentar cancelar o plano, tente novamente mais tarde");  
    }
    
    //verifica se existe a conta, se sim, retornar o id
    private static String verifyAccount(String email) throws RespException {
        Webb requestMaker = Webb.create();
        JsonObject response = null;
        
        String resposta = requestMaker.get("https://api.iugu.com/v1/customers" +
                "?api_token=" + IUGU_TOKEN + 
                "&query=\"" + email + "\"")
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();

        response = new JsonParser().parse(resposta).getAsJsonObject();
        if (response != null && response.get("id") != null && 
                response.get("id").getAsString() != null && response.get("id").getAsString().trim().length() > 0) {
            return response.get("id").getAsString();
        }
        else {
            if (response != null && response.get("errors") != null && 
                    response.get("errors").getAsString() != null && response.get("errors").getAsString().trim().length() > 0) {
                throw new RespException(response.get("errors").getAsString());
            }
            else {
                return null;
            }
        }   
    }
    
    private static String verifyAccountV2(String email) throws RespException {
        Webb requestMaker = Webb.create();
        JsonObject response = null;
        
         String url = "https://api.iugu.com/v1/customers" +
                "?api_token=" + IUGU_TOKEN + 
                "&query=" + email;
        
        String resposta = requestMaker.get(url)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();

        response = new JsonParser().parse(resposta).getAsJsonObject();
        
        if(response != null)
        {
          int total = response.get("totalItems").getAsInt();
            JsonArray  array = response.getAsJsonArray("items");
            
            if(array != null && total > 0)
            {
               return array.get(0).getAsJsonObject().get("id").getAsString();
                
            }
            
            else 
            {
                if (response != null && response.get("errors") != null && 
                        response.get("errors").getAsString() != null && response.get("errors").getAsString().trim().length() > 0) {
                    throw new RespException(response.get("errors").getAsString());
                }
                else {
                    return null;
                }
            }
        }
        
        return null;
    }

    private static String createAccount(String email, String name) throws RespException {
        IuguEmailRequest request = new IuguEmailRequest(IUGU_TOKEN, email, name);
        Webb requestMaker = Webb.create();
        Gson gson = new Gson();
        JsonObject response = null;
        String json = gson.toJson(request);
        String resposta = requestMaker.post("https://api.iugu.com/v1/customers")
                .body(json)
                .header("content-type", "application/json")
                .ensureSuccess().asString().getBody();
        response = new JsonParser().parse(resposta).getAsJsonObject();

        if (response != null && response.get("id") != null && 
                response.get("id").getAsString() != null && response.get("id").getAsString().trim().length() > 0) {
            return response.get("id").getAsString();
        }
        else {
            if (response != null && response.get("errors") != null && 
                    response.get("errors").getAsString() != null && response.get("errors").getAsString().trim().length() > 0) {
                throw new RespException(response.get("errors").getAsString());
            }
            else {
                return null;
            }
        }   
    }
    
    private static String createFullAccount(String email, String name,String doc,String zipcode, 
            String number,String street,String city,String state, String district) throws RespException {
         
        IuguFullAccountRequest request = 
                new IuguFullAccountRequest(IUGU_TOKEN, email, name, doc, zipcode, number, street, city, state, district);
        Webb requestMaker = Webb.create();
        Gson gson = new Gson();
        JsonObject response = null;
        String json = gson.toJson(request);
        String resposta = requestMaker.post("https://api.iugu.com/v1/customers")
                .body(json)
                .header("content-type", "application/json")
                .ensureSuccess().asString().getBody();
        response = new JsonParser().parse(resposta).getAsJsonObject();

        if (response != null && response.get("id") != null && 
                response.get("id").getAsString() != null && response.get("id").getAsString().trim().length() > 0) {
            return response.get("id").getAsString();
        }
        else {
            if (response != null && response.get("errors") != null && 
                    response.get("errors").getAsString() != null && response.get("errors").getAsString().trim().length() > 0) {
                throw new RespException(response.get("errors").getAsString());
            }
            else {
                return null;
            }
        }   
    }
            
    private static List<IuguPayment> listPayments(String clientId) throws RespException {
        Webb requestMaker = Webb.create();
        List<IuguPayment> response = null;
        String url = "https://api.iugu.com/v1/customers/" + clientId + "/payment_methods?api_token=" + IUGU_TOKEN;
        String resposta = requestMaker.get(url)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        
        response = new Gson().fromJson(resposta, new TypeToken<ArrayList<IuguPayment>>(){}.getType());
        return response;
    }
    
    private static String createPaymentToken(IuguNewPayment payment) throws RespException {
        Webb requestMaker = Webb.create();
        IuguPaymentTokenRequest request = new IuguPaymentTokenRequest(IUGU_MASTER_ID, !IS_PRODUCTION, payment);
        JsonObject response = null;
        Gson gson = new Gson();
        String json = gson.toJson(request);
        String resposta = requestMaker.post("https://api.iugu.com/v1/payment_token")
                .body(json)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        
        response = new JsonParser().parse(resposta).getAsJsonObject();
        
        if (response != null && response.get("id") != null && 
                response.get("id").getAsString() != null && response.get("id").getAsString().trim().length() > 0) {
            return response.get("id").getAsString();
        }
        else {
            if (response != null && response.get("errors") != null && 
                    response.get("errors").getAsString() != null && response.get("errors").getAsString().trim().length() > 0) {
                throw new RespException(response.get("errors").getAsString());
            }
            else {
                return null;
            }
        }   
    }
    
    private static IuguPayment createPayment(String clientId, String tokenPayment, boolean isFirstCard) throws RespException {
        Webb requestMaker = Webb.create();
        IuguNewPaymentRequest request = new IuguNewPaymentRequest(IUGU_TOKEN, clientId, NAME_IN_CARD, tokenPayment, isFirstCard);
        IuguPayment response = null;
        Gson gson = new Gson();
        String json = gson.toJson(request);
        String resposta = requestMaker.post("https://api.iugu.com/v1/customers/" + clientId + "/payment_methods")
                .body(json)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        
        response = gson.fromJson(resposta, IuguPayment.class);
        
        return response;
    }
    
    private static IuguPayment createPayment(String clientId, IuguNewPayment payment, String itemType, boolean cardDefault) throws RespException {
        Webb requestMaker = Webb.create();
        IuguNewPaymentRequestV2 request = 
                new IuguNewPaymentRequestV2(IUGU_TOKEN, payment, itemType,cardDefault ,NAME_IN_CARD);
        IuguPayment response = null;
        Gson gson = new Gson();
        String json = gson.toJson(request);
        String url = "https://api.iugu.com/v1/customers/" + clientId + "/payment_methods";
        String resposta = requestMaker.post(url)
                .body(json)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        
        response = gson.fromJson(resposta, IuguPayment.class);
        
        return response;
    }
    
    private static String removerPayment(String clientId, String idPayment) throws RespException {
        Webb requestMaker = Webb.create();
        JsonObject response = null;
        String url = "https://api.iugu.com/v1/customers/" + clientId + "/payment_methods/" 
                + idPayment + "?api_token=" + IUGU_TOKEN;
        String resposta = requestMaker.delete(url)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        
        response = new JsonParser().parse(resposta).getAsJsonObject();
        
        if (response != null && response.get("id") != null && 
                response.get("id").getAsString() != null && response.get("id").getAsString().trim().length() > 0) {
            return response.get("id").getAsString();
        }
        else {
            if (response != null && response.get("errors") != null && 
                    response.get("errors").getAsString() != null && response.get("errors").getAsString().trim().length() > 0) {
                throw new RespException(response.get("errors").getAsString());
            }
            else {
                return null;
            }
        }   
    }
    
    private static JsonObject changeBankInformations(IuguBankVerificationRequest request) throws RespException{
        Webb requestMaker = Webb.create();
        Gson gson = new Gson();
        JsonObject response = null;
        String json = gson.toJson(request);
        String resposta = requestMaker.post("https://api.iugu.com/v1/bank_verification")
                .body(json)
                .header("content-type", "application/json")
                .ensureSuccess().asString().getBody();
        
        response = new JsonParser().parse(resposta).getAsJsonObject();
        return response;
    }
    
    public static IuguPedidoSaqueResponse withdraw(String subaccount, double amount, String apiToken) throws RespException{
        IuguPedidoSaqueRequest request = new IuguPedidoSaqueRequest(amount, apiToken);
        Webb requestMaker = Webb.create();
        Gson gson = new Gson();
        IuguPedidoSaqueResponse response = null;
        String json = gson.toJson(request);
        String resposta = requestMaker.post("https://api.iugu.com/v1/accounts/" + subaccount + "/request_withdraw")
                .body(json)
                .header("content-type", "application/json")
                .ensureSuccess().asString().getBody();
        response = gson.fromJson(resposta, IuguPedidoSaqueResponse.class);
        return response;
    }
    
    @Deprecated
    public static IuguClientAccount criarVerificarConta(String email, String name) throws RespException {
        return createAccountVerification(email, name);
    }   
    
    @Deprecated
    public static IuguPayment criarNovoPagamento(String idCliente, IuguNewPayment payment, boolean ehPrimeiroCartao) throws RespException {
        return createNewPayment(idCliente, payment, ehPrimeiroCartao);
    }
    
    @Deprecated
    public static IuguSubconta cadastrarSubconta(String nome, Double comissao) throws RespException {
        return createSubAccount(nome, comissao);
    }

    @Deprecated
    public static JsonObject verificarStatusSubconta(IuguSubconta subconta) throws RespException {
        return verifySubAccountStatus(subconta);
    }
    
    @Deprecated
    public static JsonObject alterarDadosBancarios(IuguBankVerificationRequest request) throws RespException {
        return updateBankData(request);
    }
           
    @Deprecated
    public static List<IuguBankVerificationStatus> listarBankVerifications(String liveToken) throws RespException {
        return listBankVerifications(liveToken);
    }
    
    @Deprecated
    public static IuguTransferResponse realizarTransferenciaSubconta(String account, double valor) throws RespException{
        return transferToSubAccount(account, valor);
    }
    
    @Deprecated
    public static void removerPagamento(String idCliente, String idPayment) throws RespException {
        removePayment(idCliente, idPayment);
    }
    
    @Deprecated
    public static void alterarPagamentoDefault(String idCliente, String idPayment) throws RespException {
        updateDefaultPayment(idCliente, idPayment);
    }
    
    @Deprecated
    public static String criarAssinatura(String idCliente, String planIdentifier) throws RespException {
        return createSubscription(idCliente, planIdentifier);
    }
    
    @Deprecated
    public static void cancelarAssinatura(String idAssinatura) throws RespException {
        cancelSubscription(idAssinatura);
    }
    
    @Deprecated
    public static void alterarPlanoAssinatura(String idAssinatura, String planIdentifier) throws RespException {
        updateSubscriptionPlan(idAssinatura, planIdentifier);
    }
    
    @Deprecated
    public static IuguPedidoSaqueResponse realizarSaque(String subconta, double amount, String apiToken) throws RespException{
        return withdraw(subconta, amount, apiToken);
    }

    private static IuguPayment AlterPayment(String idCliente, String idPayment, IuguNewPayment payment, String itemType, boolean defaultCard) {
        Webb requestMaker = Webb.create();
        IuguNewPaymentRequestV2 request = 
                new IuguNewPaymentRequestV2(IUGU_TOKEN, payment, itemType,defaultCard ,NAME_IN_CARD);
        IuguPayment response = null;
        Gson gson = new Gson();
        String json = gson.toJson(request);
        String resposta = requestMaker.post("https://api.iugu.com/v1/customers/" + idCliente + "/payment_methods/" + idPayment)
                .body(json)
                .header("content-type", "application/json")
                .ensureSuccess()
                .asString().getBody();
        
        response = gson.fromJson(resposta, IuguPayment.class);
        
        return response;
    }

}
