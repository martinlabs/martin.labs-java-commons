/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.martinlabs.commons.iugu;

/**
 *
 * @author developer
 */
public class IuguNewPaymentRequestV2 extends IuguRequest {
    
   private IuguNewPayment data;
   private String item_type;
   private boolean set_as_default;
   private String description;

    public IuguNewPaymentRequestV2(String api_token, IuguNewPayment iuguNewPayment, 
            String item_type, boolean set_as_default, String description) {
        super(api_token);
        this.data = iuguNewPayment;
        this.item_type = item_type;
        this.set_as_default = set_as_default;
        this.description = description;
    }

    
    
}
