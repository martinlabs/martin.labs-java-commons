package br.com.martinlabs.commons.iugu;

/**
 *
 * @author Kobayashi
 */
public class IuguSubscriptionRequest extends IuguRequest {
    private String plan_identifier;
    private String customer_id;
    private boolean only_on_charge_success;

    public IuguSubscriptionRequest(String api_token, String plan_identifier, String customer_id) {
        super(api_token);
        this.plan_identifier = plan_identifier;
        this.customer_id = customer_id;
        this.only_on_charge_success = true;
    }

    public String getPlan_identifier() {
        return plan_identifier;
    }

    public void setPlan_identifier(String plan_identifier) {
        this.plan_identifier = plan_identifier;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public boolean isOnly_on_charge_success() {
        return only_on_charge_success;
    }

    public void setOnly_on_charge_success(boolean only_on_charge_success) {
        this.only_on_charge_success = only_on_charge_success;
    }
}
