package br.com.martinlabs.commons.iugu;

/**
 *
 * @author Kobayashi
 */
public class IuguNewPaymentRequest extends IuguRequest {
    private String customer_id;
    private String description;
    private String token;
    private boolean set_as_default;

    public IuguNewPaymentRequest(String api_token, String customer_id, String description, String token, boolean isdefault) {
        super(api_token);
        this.customer_id = customer_id;
        this.description = description;
        this.token = token;
        this.set_as_default = isdefault;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isSet_as_default() {
        return set_as_default;
    }

    public void setSet_as_default(boolean set_as_default) {
        this.set_as_default = set_as_default;
    }
}
