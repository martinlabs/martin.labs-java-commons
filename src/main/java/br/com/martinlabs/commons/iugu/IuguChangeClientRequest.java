package br.com.martinlabs.commons.iugu;

/**
 *
 * @author Kobayashi
 */
public class IuguChangeClientRequest extends IuguRequest{
    private String id;
    private String default_payment_method_id;

    public IuguChangeClientRequest(String api_token, String id, String default_payment_method_id) {
        super(api_token);
        this.id = id;
        this.default_payment_method_id = default_payment_method_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDefault_payment_method_id() {
        return default_payment_method_id;
    }

    public void setDefault_payment_method_id(String default_payment_method_id) {
        this.default_payment_method_id = default_payment_method_id;
    }
}
