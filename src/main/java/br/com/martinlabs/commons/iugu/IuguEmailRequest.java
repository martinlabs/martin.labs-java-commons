package br.com.martinlabs.commons.iugu;

/**
 *
 * @author Kobayashi
 */
public class IuguEmailRequest extends IuguRequest{
    private String email;
    private String name;

    public IuguEmailRequest(String api_token, String email, String name) {
        super(api_token);
        this.email = email;
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
