package br.com.martinlabs.commons.iugu;

/**
 *
 * @author Kobayashi
 */
public class IuguChangePlanRequest extends IuguRequest {
    private String ID;
    private String plan_identifier;

    public IuguChangePlanRequest(String api_token, String ID, String plan_identifier) {
        super(api_token);
        this.ID = ID;
        this.plan_identifier = plan_identifier;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getPlan_identifier() {
        return plan_identifier;
    }

    public void setPlan_identifier(String plan_identifier) {
        this.plan_identifier = plan_identifier;
    }
}
