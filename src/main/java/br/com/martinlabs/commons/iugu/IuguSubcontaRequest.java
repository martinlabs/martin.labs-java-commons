package br.com.martinlabs.commons.iugu;

/**
 *
 * @author Kobayashi
 */
    
public class IuguSubcontaRequest extends IuguRequest {
    private String name;
    private Double commission_percent;
    
    public IuguSubcontaRequest(String api_token, Double comissao){
        super(api_token);
        this.commission_percent = comissao;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}