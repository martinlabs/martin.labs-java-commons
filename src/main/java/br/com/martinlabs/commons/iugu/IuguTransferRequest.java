package br.com.martinlabs.commons.iugu;

/**
 *
 * @author Kobayashi
 */
public class IuguTransferRequest extends IuguRequest {
    private String receiver_id;
    private long amount_cents;
    
    public IuguTransferRequest(String api_token, String receiver_id, long amount_cents) {
        super(api_token);
        this.receiver_id = receiver_id;
        this.amount_cents = amount_cents;
    }

    public String getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(String receiver_id) {
        this.receiver_id = receiver_id;
    }

    public long getAmount_cents() {
        return amount_cents;
    }

    public void setAmount_cents(long amount_cents) {
        this.amount_cents = amount_cents;
    }
}
