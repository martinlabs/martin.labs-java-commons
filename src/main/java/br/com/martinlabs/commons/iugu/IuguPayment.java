package br.com.martinlabs.commons.iugu;

/**
 *
 * @author Kobayashi
 */
public class IuguPayment {
    private String id;
    private String description;
    private String item_type;
    private IuguCard data;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getItem_type() {
        return item_type;
    }

    public void setItem_type(String item_type) {
        this.item_type = item_type;
    }

    public IuguCard getData() {
        return data;
    }

    public void setData(IuguCard data) {
        this.data = data;
    }
}
