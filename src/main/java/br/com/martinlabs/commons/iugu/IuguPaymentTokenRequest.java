package br.com.martinlabs.commons.iugu;

/**
 *
 * @author Kobayashi
 */
public class IuguPaymentTokenRequest {
    private String account_id;
    private String method;
    private boolean test;
    private IuguNewPayment data;

    public IuguPaymentTokenRequest(String account_id, boolean test, IuguNewPayment data) {
        this.account_id = account_id;
        this.method = "credit_card";
        this.test = test;
        this.data = data;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public boolean getTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }

    public IuguNewPayment getData() {
        return data;
    }

    public void setData(IuguNewPayment data) {
        this.data = data;
    }
    
}
