package br.com.martinlabs.commons.iugu;

/**
 *
 * @author Kobayashi
 */
public class IuguSubconta {
    private String account_id;
    private String name;
    private String live_api_token;
    private String test_api_token;
    private String user_token;

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLive_api_token() {
        return live_api_token;
    }

    public void setLive_api_token(String live_api_token) {
        this.live_api_token = live_api_token;
    }

    public String getTest_api_token() {
        return test_api_token;
    }

    public void setTest_api_token(String test_api_token) {
        this.test_api_token = test_api_token;
    }

    public String getUser_token() {
        return user_token;
    }

    public void setUser_token(String user_token) {
        this.user_token = user_token;
    }
}