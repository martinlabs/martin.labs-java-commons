package br.com.martinlabs.commons.iugu;

/**
 *
 * @author Kobayashi
 */
public class IuguTransferResponse {
    private String id;
    private String created_at;
    private String amount_cents;
    private String amount_localized;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getAmount_cents() {
        return amount_cents;
    }

    public void setAmount_cents(String amount_cents) {
        this.amount_cents = amount_cents;
    }

    public String getAmount_localized() {
        return amount_localized;
    }

    public void setAmount_localized(String amount_localized) {
        this.amount_localized = amount_localized;
    }
}
