/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.martinlabs.commons.iugu;

/**
 *
 * @author developer
 */
public class IuguFullAccountRequest extends IuguRequest{
    
    
    private String name;
    private String cpf_cnpj;
    private String zip_code;
    private String number;
    private String street;
    private String state;
    private String city;
    private String district;
    
    public IuguFullAccountRequest(String api_token,String email, String name, 
            String cpf_cnpj, String zip_code, String number, String street, String city,
            String state , String district) {
        super(api_token,email);
        this.name = name;
        this.cpf_cnpj = cpf_cnpj;
        this.zip_code = zip_code;
        this.number = number;
        this.street = street;
        this.city = city;
        this.state = state;
        this.district = district;
    }
    
   
    
}
