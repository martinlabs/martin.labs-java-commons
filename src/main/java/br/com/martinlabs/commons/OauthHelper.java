package br.com.martinlabs.commons;

import javax.servlet.http.HttpServletRequest;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.ParameterStyle;
import org.apache.oltu.oauth2.rs.request.OAuthAccessResourceRequest;

/**
 *
 * @author gil
 */
public class OauthHelper {

    public static String getToken(HttpServletRequest req) {
        String token = null;

        try {
            OAuthAccessResourceRequest oauthRequest = new OAuthAccessResourceRequest(req, ParameterStyle.HEADER);
            token = oauthRequest.getAccessToken();
        } catch (OAuthSystemException ex) {
        } catch (OAuthProblemException ex) {
        }
        
        return token;
    }

}
