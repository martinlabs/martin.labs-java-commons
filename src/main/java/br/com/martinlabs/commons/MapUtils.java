package br.com.martinlabs.commons;

/**
 *
 * @author gil
 */
public abstract class MapUtils {

    public static double calcDistance(double latitudeA, double longitudeA, double latitudeB, double longitudeB) {
        int radiusEarthInKm = 6371;
        double dLat = degreeToRadius(latitudeB - latitudeA);
        double dLon = degreeToRadius(longitudeB - longitudeA);
        double angle = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(degreeToRadius(latitudeA)) * Math.cos(degreeToRadius(latitudeB)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(angle), Math.sqrt(1 - angle));
        double distancia = radiusEarthInKm * c;
        return distancia;
    }

    public static double degreeToRadius(double degree) {
        return degree * (Math.PI / 180);
    }

    public static double milesToLatitudeDegrees(double miles) {
        double earthRadius = 3960.0; // in miles
        double radiansToDegrees = 180.0/Math.PI;
        return (miles/earthRadius) * radiansToDegrees;
    }

    public static double milesToLongitudeDegrees(double miles, double atLatitude) {
        double earthRadius = 3960.0; // in miles
        double degreesToRadians = Math.PI/180.0;
        double radiansToDegrees = 180.0/Math.PI;
        // derive the earth's radius at that point in latitude
        double radiusAtLatitude = earthRadius * Math.cos(atLatitude * degreesToRadians);
        return (miles / radiusAtLatitude) * radiansToDegrees;
    }
    
}
