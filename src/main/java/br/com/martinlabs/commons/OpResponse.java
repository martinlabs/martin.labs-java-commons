package br.com.martinlabs.commons;

/**
 *
 * @author gil
 */
@Deprecated
public class OpResponse<T> {
    private boolean Success; //if got success in operation
    private String Message; //used to transmit error message
    private T Data; //result data
    private Integer Code; //error code

    public OpResponse() {
        this(true, null);
    }

    public T getData() {
        return Data;
    }

    public void setData(T Data) {
        this.Data = Data;
    }

    public OpResponse(T Data) {
        this.Success = true;
        this.Data = Data;
    }

    public OpResponse(boolean success, String message) {
        this.Success = success;
        this.Message = message;
    }

    public OpResponse(boolean success, String message, T details) {
        this(success, message);
        this.Data = details;
    }

    public boolean isSuccess() {
        return Success;
    }

    public String getMessage() {
        return Message;
    }

    public Integer getCode() {
        return Code;
    }

    public void setCode(Integer Code) {
        this.Code = Code;
    }
}
