package br.com.martinlabs.commons;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gil
 */
public class PropertyHelper {
    private Properties p;

    public PropertyHelper(String file) {
        InputStream is = this.getClass().getResourceAsStream(file);
        p = new Properties();
        
        try {
            p.load(is);   
        } catch (IOException ex) {
            Logger.getLogger(AwsFileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String get(String key) {
        return p.getProperty(key);
    }
}
