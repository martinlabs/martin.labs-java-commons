package br.com.martinlabs.commons;

/**
 *
 * @author gil
 */
public class OpResp<T> {
    private boolean success; //if got success in operation
    private String message; //used to transmit error message
    private T data; //result data
    private Integer code; //error code

    public OpResp() {
        this(true, null);
    }

    public OpResp(T Data) {
        this.success = true;
        this.data = Data;
    }

    public OpResp(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public OpResp(boolean success, String message, T details) {
        this(success, message);
        this.data = details;
    }

    public T getData() {
        return data;
    }

    public void setData(T Data) {
        this.data = Data;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer Code) {
        this.code = Code;
    }
}
