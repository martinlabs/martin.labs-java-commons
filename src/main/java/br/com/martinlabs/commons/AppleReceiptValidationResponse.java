package br.com.martinlabs.commons;

/**
 *
 * @author ricardoprado
 */
public class AppleReceiptValidationResponse {

    //Status    
    //21000
    //The App Store could not read the JSON object you provided.
    //21002
    //The data in the receipt-data property was malformed or missing.
    //21003
    //The receipt could not be authenticated.
    //21004
    //The shared secret you provided does not match the shared secret on file for your account.
    //Only returned for iOS 6 style transaction receipts for auto-renewable subscriptions.
    //21005
    //The receipt server is not currently available.
    //21006
    //This receipt is valid but the subscription has expired. When this status code is returned to your server, the receipt data is also decoded and returned as part of the response.
    //Only returned for iOS 6 style transaction receipts for auto-renewable subscriptions.
    //21007
    //This receipt is from the test environment, but it was sent to the production environment for verification. Send it to the test environment instead.
    //21008
    //This receipt is from the production environment, but it was sent to the test environment for verification. Send it to the production environment instead.
    private long status;
    private ApplePurchaseReceipt receipt;
    private String latest_receipt;
    private ApplePurchaseReceipt latest_receipt_info;

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public String getLatest_receipt() {
        return latest_receipt;
    }

    public void setLatest_receipt(String latest_receipt) {
        this.latest_receipt = latest_receipt;
    }

    public ApplePurchaseReceipt getReceipt() {
        return receipt;
    }

    public void setReceipt(ApplePurchaseReceipt receipt) {
        this.receipt = receipt;
    }

    public ApplePurchaseReceipt getLatest_receipt_info() {
        return latest_receipt_info;
    }

    public void setLatest_receipt_info(ApplePurchaseReceipt latest_receipt_info) {
        this.latest_receipt_info = latest_receipt_info;
    }

   
    
    
    
}
