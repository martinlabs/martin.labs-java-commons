package br.com.martinlabs.commons;

/**
 *
 * @author gil
 */
@Deprecated
public class OpResponsePaginado<T> extends OpResponse<T> {
    private int QuantidadeTotal;
    private int QuantidadeFiltrada;

    public OpResponsePaginado() {
    }

    public OpResponsePaginado(T Data) {
        super(Data);
    }

    public OpResponsePaginado(boolean success, String message) {
        super(success, message);
    }

    public OpResponsePaginado(boolean success, String message, T details) {
        super(success, message, details);
    }

    public int getQuantidadeTotal() {
        return QuantidadeTotal;
    }

    public void setQuantidadeTotal(int QuantidadeTotal) {
        this.QuantidadeTotal = QuantidadeTotal;
    }

    public int getQuantidadeFiltrada() {
        return QuantidadeFiltrada;
    }

    public void setQuantidadeFiltrada(int QuantidadeFiltrada) {
        this.QuantidadeFiltrada = QuantidadeFiltrada;
    }
}
