package br.com.martinlabs.commons;

import java.util.HashMap;

/**
 *
 * @author gil
 */
public class NamedWrapper<T> extends HashMap<String, T>{

    public NamedWrapper() {
    }

    public NamedWrapper(String name, T object) {
        put(name, object);
    }

}
