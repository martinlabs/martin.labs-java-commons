package br.com.martinlabs.commons;

import br.com.martinlabs.commons.exceptions.RespException;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletContext;
import org.apache.log4j.BasicConfigurator;

/**
 *
 * @author ricardoprado
 */
public abstract class JavaMailWrapper {

    protected String from = "";
    protected String name = from;
    protected String to = "";
    protected String bcc = "";
    protected List<String> ListBcc;
    protected String subject = "";
    protected String host = "";
    protected String body;
    protected String attachment = "";
    protected String nameAttachment = "";
    protected String protocolo = "";
    protected boolean useLocaweb = false;
    protected String userSmtp = "";
    protected String passwordStmp = "";

    private static Configuration templateConfig;

    public void send() {
        send("587", false);
    }
    
    public void send(String smtpPort, boolean useSocketFactoryConfig) {
        if (useLocaweb) {
            sendWithLocaweb(smtpPort, useSocketFactoryConfig);
        }
        else {
            sendWithoutLocaweb(smtpPort, useSocketFactoryConfig);
        }
    }
    
    public void sendWithLocaweb(String smtpPort, boolean useSocketFactoryConfig) {
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", host);
        properties.put("mail.smtp.starttls.enable", "true");
        if (protocolo.length() > 0) {
            properties.put("mail.transport.protocol", protocolo);
        }
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", smtpPort);
        properties.put("mail.smtp.socketFactory.fallback", "false");
        if (useSocketFactoryConfig) {
            properties.put("mail.smtp.socketFactory.port", smtpPort);
            properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        }

        // Get the default Session object.
        Session session = Session.getDefaultInstance(properties, null);

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from, name));
            if (to.length() > 0) {
                String[] emails = to.split(";");
                if (emails.length > 0) {
                    for (String email : emails) {
                        message.addRecipient(Message.RecipientType.TO,
                                new InternetAddress(email));
                    }
                } else {
                    message.addRecipient(Message.RecipientType.TO,
                            new InternetAddress(to));
                }

            }
            if (bcc.length() > 0) {
                String[] emails = bcc.split(";");
                if (emails.length > 0) {
                    for (String email : emails) {
                        message.addRecipient(Message.RecipientType.BCC,
                                new InternetAddress(email));
                    }
                } else {
                    message.addRecipient(Message.RecipientType.BCC,
                            new InternetAddress(bcc));
                }
            }
            if (ListBcc != null && !ListBcc.isEmpty()) {
                for (String email : ListBcc) {
                    message.addRecipient(Message.RecipientType.BCC,
                            new InternetAddress(email));
                }
            }
            message.setSubject(subject);
            if(attachment != null && attachment.length() > 0)
            {
                Multipart multipart = new MimeMultipart();
                BodyPart messageBodyPart = new MimeBodyPart();
                messageBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(this.body, "text/html")));
                
                BodyPart attachFilePart = new MimeBodyPart();
                attachFilePart.setDataHandler(new DataHandler(
                        new ByteArrayDataSource(attachment.getBytes("UTF-8"),"application/octet-stream")));
                attachFilePart.setFileName(this.nameAttachment);
                multipart.addBodyPart(messageBodyPart);
                multipart.addBodyPart(attachFilePart);
                message.setContent(multipart);
            }
            message.setContent(body, "text/html; charset=utf-8");
            Transport tr = session.getTransport(protocolo);
            tr.connect(host, userSmtp, passwordStmp);

            message.saveChanges();

            tr.sendMessage(message, message.getAllRecipients());
            tr.close();
        } catch (Exception mex) {
            Logger.getAnonymousLogger().severe("Erro ao enviar o e-mail. " + mex.getLocalizedMessage());
            mex.printStackTrace();
            RespException e = new RespException(LanguageHolder.instance.errorSendingTheEmail());
            e.initCause(mex);
            throw e;
        }
    }
    
    public void sendWithoutLocaweb(String smtpPort, boolean useSocketFactoryConfig) {
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", host);
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", smtpPort);
        properties.put("mail.smtp.socketFactory.fallback", "false");
        if (useSocketFactoryConfig) {
            properties.put("mail.smtp.socketFactory.port", smtpPort);
            properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        }

        // Get the default Session object.
        Session session = Session.getDefaultInstance(properties, ObterAutenticador());

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from, name));
            if (to.length() > 0) {
                String[] emails = to.split(";");
                if (emails.length > 0) {
                    for (String email : emails) {
                        message.addRecipient(Message.RecipientType.TO,
                                new InternetAddress(email));
                    }
                } else {
                    message.addRecipient(Message.RecipientType.TO,
                            new InternetAddress(to));
                }

            }
            if (bcc.length() > 0) {
                String[] emails = bcc.split(";");
                if (emails.length > 0) {
                    for (String email : emails) {
                        message.addRecipient(Message.RecipientType.BCC,
                                new InternetAddress(email));
                    }
                } else {
                    message.addRecipient(Message.RecipientType.BCC,
                            new InternetAddress(bcc));
                }
            }
            if (ListBcc != null && !ListBcc.isEmpty()) {
                for (String email : ListBcc) {
                    message.addRecipient(Message.RecipientType.BCC,
                            new InternetAddress(email));
                }
            }
            message.setSubject(subject);
            if(attachment != null && attachment.length() > 0)
            {
                Multipart multipart = new MimeMultipart();
                BodyPart messageBodyPart = new MimeBodyPart();
                messageBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(this.body, "text/html")));
                
                BodyPart attachFilePart = new MimeBodyPart();
                attachFilePart.setDataHandler(new DataHandler(
                        new ByteArrayDataSource(attachment.getBytes("UTF-8"),"application/octet-stream")));
                attachFilePart.setFileName(this.nameAttachment);
                multipart.addBodyPart(messageBodyPart);
                multipart.addBodyPart(attachFilePart);
                message.setContent(multipart);
            }
            else
            {
            
            message.setDataHandler(new DataHandler(new ByteArrayDataSource(this.body, "text/html")));
//            message.setContent(this.body, "text/html; charset=utf-8");
            }
            
            Transport.send(message);
        } catch (Exception mex) {
            Logger.getAnonymousLogger().severe("Erro ao enviar o e-mail. " + mex.getLocalizedMessage());
            mex.printStackTrace();
            RespException e = new RespException(LanguageHolder.instance.errorSendingTheEmail());
            e.initCause(mex);
            throw e;
        }
    }

    /**
     *
     * @param forClassLoader this.getClass()
     * @return
     */
    public static Configuration getTemplateConfigInstance(Class forClassLoader) {
        if (templateConfig == null) {
            BasicConfigurator.configure();
            templateConfig = new Configuration(Configuration.VERSION_2_3_22);
            templateConfig.setClassForTemplateLoading(forClassLoader, "/mail-templates");
            templateConfig.setDefaultEncoding("UTF-8");
            templateConfig.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            templateConfig.setLocalizedLookup(false);
        }

        return templateConfig;
    }

    /**
     *
     * @param forClassLoader this.getClass()
     * @param model
     * @param templateFilename
     */
    public final void setBodyFromTemplate(Class forClassLoader, Map<String, Object> model, String templateFilename) {
        try {
            Template temp = getTemplateConfigInstance(forClassLoader).getTemplate(templateFilename);
            Writer out = new StringWriter();
            temp.process(model, out);
            body = out.toString();
        } catch (Exception ex) {
            Logger.getLogger(JavaMailWrapper.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract Authenticator ObterAutenticador();

    @Deprecated
    public static Configuration getTemplateConfigInstance(ServletContext ctx) {
        if (templateConfig == null) {
            templateConfig = new Configuration(Configuration.VERSION_2_3_22);
            templateConfig.setServletContextForTemplateLoading(ctx, "mail-templates");
            templateConfig.setDefaultEncoding("UTF-8");
            templateConfig.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        }

        return templateConfig;
    }

    @Deprecated
    public void setBodyFromTemplate(ServletContext ctx, Map<String, Object> model, String templateFilename) {
        try {
            Template temp = getTemplateConfigInstance(ctx).getTemplate(templateFilename);
            Writer out = new StringWriter();
            temp.process(model, out);
            body = out.toString();
        } catch (Exception ex) {
            Logger.getLogger(JavaMailWrapper.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Deprecated
    public void enviarEmail() {
        send();
    }

    @Deprecated
    public void enviarEmail(String body) {
        this.body = body;
        enviarEmail();
    }
}
