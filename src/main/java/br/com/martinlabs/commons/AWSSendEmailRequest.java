/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.martinlabs.commons;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.model.*;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.apache.http.HttpHeaders.FROM;
import org.apache.log4j.BasicConfigurator;

/**
 *
 * @author ricardoprado
 */
public class AWSSendEmailRequest  {

    protected String from = "";
    protected String name = from;
    protected String to = "";
    protected String bcc = "";
    protected List<String> ListBcc;
    protected String subject = "";
    protected String host = "";
    protected String body;
    protected String attachment = "";
    protected String nameAttachment = "";
    private Regions region;
    private static Configuration templateConfig;

    public AWSSendEmailRequest(Regions region) {
        this.region = region;
    }

    public AWSSendEmailRequest(){
        this.region = Regions.US_WEST_2;
    }

    public void send() {
        // Construct an object to contain the recipient address.
        Destination destination = new Destination().withToAddresses(new String[]{to});

        // Create the subject and body of the message.
        Content subjectParam = new Content().withData(subject);
        Content textBody = new Content().withData(body);
        Body bodyParam = new Body().withHtml(textBody);

        // Create a message with the specified subject and body.
        Message message = new Message().withSubject(subjectParam).withBody(bodyParam);

        // Assemble the email.
        SendEmailRequest request = new SendEmailRequest().withSource(from)
                .withDestination(destination).withMessage(message);
        AwsSES ses = new AwsSES();
        ses.setRegion(Region.getRegion(region));
        ses.sendEmail(request);
    }

    /**
     *
     * @param forClassLoader this.getClass()
     * @return
     */
    public static Configuration getTemplateConfigInstance(Class forClassLoader) {
        if (templateConfig == null) {
            BasicConfigurator.configure();
            templateConfig = new Configuration(Configuration.VERSION_2_3_22);
            templateConfig.setClassForTemplateLoading(forClassLoader, "/mail-templates");
            templateConfig.setDefaultEncoding("UTF-8");
            templateConfig.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            templateConfig.setLocalizedLookup(false);
        }

        return templateConfig;
    }

    /**
     *
     * @param forClassLoader this.getClass()
     * @param model
     * @param templateFilename
     */
    public final void setBodyFromTemplate(Class forClassLoader, Map<String, Object> model, String templateFilename) {
        try {
            Template temp = getTemplateConfigInstance(forClassLoader).getTemplate(templateFilename);
            Writer out = new StringWriter();
            temp.process(model, out);
            body = out.toString();
        } catch (Exception ex) {
            Logger.getLogger(JavaMailWrapper.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

}
