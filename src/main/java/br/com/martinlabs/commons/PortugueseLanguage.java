package br.com.martinlabs.commons;

/**
 *
 * @author gil
 */
public class PortugueseLanguage extends LanguageHolder {

    @Override
    public String language() {
        return "pt-br";
    }
    
    @Override
    public String cannotBeNull(String propertyName) {
        return propertyName + " não pode ser vazio";
    }

    @Override
    public String cannotBeNegative(String propertyName) {
        return propertyName + " não pode ser um número negativo";
    }

    @Override
    public String lengthCannotBeMoreThan(String propertyName, int size) {
        return propertyName + " possui um limite de "+size + " caracteres";
    }

    @Override
    public String unexpectedError() {
        return "Erro inesperado, tente novamente mais tarde";
    }

    @Override
    public String invalidLogin() {
        return "Login inválido";
    }

    @Override
    public String pleaseLogin() {
        return "Por favor, faça login";
    }

    @Override
    public String errorSendingTheEmail() {
        return "Erro ao enviar o e-mail";
    }

    @Override
    public String theImageIsTooBig() {
        return "A imagem é muito grande";
    }

    @Override
    public String errorExecutingTheFunctionInThreadPool() {
        return "Erro executando a função na Thread Pool: ";
    }

    @Override
    public String contactEmail() {
        return "E-mail de contato";
    }

    @Override
    public String invalidEntry() {
        return "Entrada inválida";
    }

    @Override
    public String isNotAValidEmail(String propertyName) {
        return propertyName + " não é um e-mail válido";
    }

    @Override
    public String isNotAValidCPF(String propertyName) {
        return propertyName + " não é um CPF válido";
    }

    @Override
    public String isNotAValidCNPJ(String propertyName) {
        return propertyName + " não é um CNPJ válido";
    }

    @Override
    public String alreadyExist(String propertyName) {
        return "Já existe uma entrada com " + propertyName + " igual";
    }
}
