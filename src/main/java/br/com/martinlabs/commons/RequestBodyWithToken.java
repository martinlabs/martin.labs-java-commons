package br.com.martinlabs.commons;

/**
 *
 * @author gil
 */
public class RequestBodyWithToken<T> {
    
    private T content;
    private String token;

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
