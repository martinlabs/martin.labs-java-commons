package br.com.martinlabs.commons;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.androidpublisher.AndroidPublisher;
import com.google.api.services.androidpublisher.model.ProductPurchase;
import com.google.api.services.androidpublisher.model.SubscriptionPurchase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author ricardoprado
 */
public class PurchasedTransactionValidator {

    private static final String appleUrlSandbox = "https://sandbox.itunes.apple.com/verifyReceipt";
    private static final String appleUrlProduction = "https://buy.itunes.apple.com/verifyReceipt";
    private String appleUrlToUse;
    private final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss 'Etc/GMT'").create();

    public PurchasedTransactionValidator() {
        appleUrlToUse = appleUrlProduction;
    }

    public PurchasedTransactionValidator(Boolean isProductionApple) {
        if (isProductionApple) {
            appleUrlToUse = appleUrlProduction;
        } else {
            appleUrlToUse = appleUrlSandbox;
        }
    }

    public boolean isGooglePurchaseValid(String packageName, String productId, String receipt, String p12Path, String serviceAccountId) {
        boolean saida = false;
        try {
            final HttpTransport TRANSPORT = new NetHttpTransport();
            final JacksonFactory JSON_FACTORY = new JacksonFactory();
            final List<String> SCOPE = Arrays.asList("https://www.googleapis.com/auth/androidpublisher");

            GoogleCredential credential = new GoogleCredential.Builder()
                    .setTransport(TRANSPORT)
                    .setJsonFactory(JSON_FACTORY)
                    .setServiceAccountId(serviceAccountId)
                    .setServiceAccountScopes(SCOPE)
                    .setServiceAccountPrivateKeyFromP12File(new File(p12Path))
                    .build();

            AndroidPublisher publisher = new AndroidPublisher(TRANSPORT, JSON_FACTORY, credential);
            AndroidPublisher.Purchases.Products.Get get = publisher.purchases().products().get(packageName, productId, receipt);
            ProductPurchase purchase = get.execute();

            if (purchase != null && purchase.getPurchaseTimeMillis() > 0) {
                saida = true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return saida;
    }

    public boolean isGoogleSubscriptionValid(String packageName, String subscriptionId, String token, String p12Path, String serviceAccountId) {
        boolean saida = false;
        try {
            final HttpTransport TRANSPORT = new NetHttpTransport();
            final JacksonFactory JSON_FACTORY = new JacksonFactory();
            final List<String> SCOPE = Arrays.asList("https://www.googleapis.com/auth/androidpublisher");

            GoogleCredential credential = new GoogleCredential.Builder()
                    .setTransport(TRANSPORT)
                    .setJsonFactory(JSON_FACTORY)
                    .setServiceAccountId(serviceAccountId)
                    .setServiceAccountScopes(SCOPE)
                    .setServiceAccountPrivateKeyFromP12File(new File(p12Path))
                    .build();

            AndroidPublisher publisher = new AndroidPublisher(TRANSPORT, JSON_FACTORY, credential);
            AndroidPublisher.Purchases.Subscriptions.Get get = publisher.purchases().subscriptions().get(packageName, subscriptionId, token);
            SubscriptionPurchase purchase = get.execute();

            if (purchase != null && purchase.getExpiryTimeMillis() > new Date().getTime()) {
                saida = true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return saida;
    }

    public boolean isApplePurchaseValid(String data, String idProduto) {
        boolean saida = false;
        AppleReceiptValidationResponse resp = validateApplePurchase(data);

        if (resp.getStatus() < 21000) {
            saida = true;
            if (idProduto != null) {
                saida = false;
                List<ApplePurchasedObject> produtosComprados = resp.getReceipt().getIn_app();
                for (ApplePurchasedObject produtoComprado : produtosComprados) {
                    if(produtoComprado.getProduct_id().equals(idProduto)){
                        saida = true;
                    }
                }
            }
        }
        return saida;
    }

    public boolean isApplePurchaseValid(String data) {
        return PurchasedTransactionValidator.this.isApplePurchaseValid(data, null);
    }

    public AppleReceiptValidationResponse validateApplePurchase(String receiptData) {
        ApplePurchaseValidationRequest request = new ApplePurchaseValidationRequest();
        request.setReceipt_data(receiptData);
        AppleReceiptValidationResponse response = sendAppleRequest(request);
        return response;
    }

    private AppleReceiptValidationResponse sendAppleRequest(ApplePurchaseValidationRequest reciboApple) {
        AppleReceiptValidationResponse saida = null;
        try {
            HttpClient httpclient;
            HttpPost httppost = new HttpPost(appleUrlToUse);
            httpclient = new DefaultHttpClient();
            StringEntity str = new StringEntity(gson.toJson(reciboApple));
            httppost.setEntity(str);
            httppost.addHeader("Content-Type", "application/json");
            httppost.addHeader("Accept", "application/json");
            HttpResponse response = httpclient.execute(httppost);
            String json = EntityUtils.toString(response.getEntity());
            saida = gson.fromJson(json, AppleReceiptValidationResponse.class);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return saida;
    }
    
    @Deprecated
    public boolean ehCompraGoogleValido(String packageName, String productId, String receipt, String p12Path, String serviceAccountId) {
        return isGooglePurchaseValid(packageName, productId, receipt, p12Path, serviceAccountId);
    }
    
    @Deprecated
    public boolean ehAssinaturaGoogleValido(String packageName, String subscriptionId, String token, String p12Path, String serviceAccountId) {
        return isGoogleSubscriptionValid(packageName, subscriptionId, token, p12Path, serviceAccountId);
    }
    
    @Deprecated
    public boolean ehComprovanteAppleValido(String data, String idProduto) {
        return PurchasedTransactionValidator.this.isApplePurchaseValid(data, idProduto);
    }
    
    @Deprecated
    public boolean ehComprovanteAppleValido(String data) {
        return isApplePurchaseValid(data);
    }
}
