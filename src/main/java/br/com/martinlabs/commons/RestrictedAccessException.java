package br.com.martinlabs.commons;

import br.com.martinlabs.commons.exceptions.RespException;

/**
 *
 * @author gil
 */
@Deprecated
public class RestrictedAccessException extends RespException {

    public RestrictedAccessException() {
        super("Realize Login");
    }

}
