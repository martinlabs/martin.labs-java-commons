package br.com.martinlabs.commons;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.ec2.util.S3UploadPolicy;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadResult;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gil
 */
public class AwsFileManager {

    private String bucketName;
    private String endpoint;
    private InputStream properties;
    private PropertiesCredentials credentials;
    private AmazonS3 s3Client;

    public AwsFileManager(String bucketName) {
        this(bucketName, "https://s3-sa-east-1.amazonaws.com/");
    }

    public AwsFileManager(String bucketName, String endpoint) {
        try {
            this.bucketName = bucketName;
            this.endpoint = endpoint;
            properties = AwsFileManager.class.getResourceAsStream("/AwsCredentials.properties");
            credentials = new PropertiesCredentials(properties);
            s3Client = new AmazonS3Client(credentials);
        } catch (IOException ex) {
            Logger.getLogger(AwsFileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String initiateMultipartUpload(String folder, String filename){
        String path = folder + "/" + filename;
        InitiateMultipartUploadRequest multipartRequest = new InitiateMultipartUploadRequest(bucketName, path);
        InitiateMultipartUploadResult multipartResult = s3Client.initiateMultipartUpload(multipartRequest);
        return multipartResult.getUploadId();
    }
    
    public String getPresignedUrlForFile(String filename, String contentType, HttpMethod httpMethod) {
        String path = filename;
        java.util.Date expiration = new java.util.Date();
        long msec = expiration.getTime();
        msec += 24000 * 60 * 60;
        expiration.setTime(msec);
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, path);
        generatePresignedUrlRequest.setMethod(httpMethod);
        if(contentType != null){
            generatePresignedUrlRequest.setContentType(contentType);
        }
        generatePresignedUrlRequest.setExpiration(expiration);
        URL s = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
        return s.toExternalForm();
    }
    
    public String getPresignedUrl(String folder, String filename, String contentType, HttpMethod httpMethod) {
        String path = folder + "/" + filename;
        java.util.Date expiration = new java.util.Date();
        long msec = expiration.getTime();
        msec += 24000 * 60 * 60;
        expiration.setTime(msec);
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, path);
        generatePresignedUrlRequest.setMethod(httpMethod);
        if(contentType != null){
            generatePresignedUrlRequest.setContentType(contentType);
        }
        generatePresignedUrlRequest.setExpiration(expiration);
        URL s = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
        return s.toExternalForm();
    }
    
    public String getPresignedUrl(String folder, String filename, String contentType) {
        return getPresignedUrl(folder, filename, contentType, HttpMethod.PUT);
    }

    public String getPresignedUrl(String folder, String filename) {
        return this.getPresignedUrl(folder, filename, null);
    }

    public String[] getUploadCredentials(String folder, String filename) {
        String path = folder + "/" + filename;
        S3UploadPolicy policy = new S3UploadPolicy(credentials.getAWSAccessKeyId(), credentials.getAWSSecretKey(), bucketName, path, 10);
        return new String[]{policy.getPolicyString(), policy.getPolicySignature()};
    }

    public String upload(String folder, String filename, InputStream input) {
        String path = folder + "/" + filename;
        ObjectMetadata objectMetadata = new ObjectMetadata();
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, path, input, objectMetadata);
        s3Client.putObject(putObjectRequest);
        return endpoint + bucketName + "/" + path;
    }

    public String upload(String folder, String filename, byte[] file) {
        InputStream stream = new ByteArrayInputStream(file);
        return upload(folder, filename, stream);
    }

    public List<String> listFiles(String folder) {
        List<String> saida = new ArrayList<String>();

        ObjectListing objetosNaPasta = s3Client.listObjects(bucketName, folder);
        List<S3ObjectSummary> objectSummaries = objetosNaPasta.getObjectSummaries();
        for (S3ObjectSummary obj : objectSummaries) {
            if (obj.getSize() > 0) {
                saida.add(obj.getKey());
            }
        }

        return saida;
    }

}
