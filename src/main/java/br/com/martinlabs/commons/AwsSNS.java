/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.martinlabs.commons;

import br.com.martinlabs.commons.exceptions.RespException;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ricardoprado
 */
public class AwsSNS {

    private PropertiesCredentials credentials;
    private AmazonSNSClient snsClient;

    public AwsSNS() {
        try {
            InputStream properties = AwsFileManager.class.getResourceAsStream("/AwsCredentials.properties");
            credentials = new PropertiesCredentials(properties);
            snsClient = new AmazonSNSClient(credentials);
            snsClient.setRegion(Region.getRegion(Regions.US_EAST_1));
        } catch (IOException ex) {
            Logger.getLogger(AwsSNS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public AwsSNS(String credentialsFileName) {
        try {
            InputStream properties = AwsFileManager.class.getResourceAsStream(credentialsFileName);
            credentials = new PropertiesCredentials(properties);
            snsClient = new AmazonSNSClient(credentials);
            snsClient.setRegion(Region.getRegion(Regions.US_EAST_1));
        } catch (IOException ex) {
            Logger.getLogger(AwsSNS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void enviarSMS(String destinatario, String mensagem) {
        if(!Validator.isValidCelPhoneNumber(destinatario)){
            return;
        }
        
        PublishRequest publishRequest = new PublishRequest()
                .withMessage(mensagem)
                .withPhoneNumber(destinatario);
        snsClient.publish(publishRequest);
    }
    
}
