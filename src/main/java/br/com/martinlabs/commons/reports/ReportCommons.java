/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.martinlabs.commons.reports;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author ricardoprado
 */
public class ReportCommons {

    private static final Logger LOG = Logger.getLogger(ReportCommons.class.getName());

    public synchronized static byte[] csvToXLS(String csv, String nomePlanilha) {
        XSSFWorkbook workBook = new XSSFWorkbook();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {

            XSSFSheet sheet = workBook.createSheet(nomePlanilha);
            String currentLine = null;
            int RowNum = 0;
            BufferedReader br = new BufferedReader(new StringReader(csv));
            while ((currentLine = br.readLine()) != null) {
                String str[] = currentLine.split(";");
                RowNum++;
                XSSFRow currentRow = sheet.createRow(RowNum);
                for (int i = 0; i < str.length; i++) {
                    currentRow.createCell(i).setCellValue(str[i]);
                }
            }
            bos = new ByteArrayOutputStream();
            workBook.write(bos);
            workBook.close();

            byte[] saida = bos.toByteArray();
            return saida;

        } catch (Exception ex) {
            LOG.severe(ex.getMessage());
        } finally {
            try {
                workBook.close();
            } catch (IOException ex) {
                Logger.getLogger(ReportCommons.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException ex) {
                    Logger.getLogger(ReportCommons.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return null;
    }

    public synchronized static void csvToXLS(String csv, String nomePlanilha, OutputStream output) {
        XSSFWorkbook workBook = new XSSFWorkbook();
        try {
            XSSFSheet sheet = workBook.createSheet(nomePlanilha);
            String currentLine = null;
            int RowNum = 0;
            BufferedReader br = new BufferedReader(new StringReader(csv));
            while ((currentLine = br.readLine()) != null) {
                String str[] = currentLine.split(";");
                RowNum++;
                XSSFRow currentRow = sheet.createRow(RowNum);
                for (int i = 0; i < str.length; i++) {
                    currentRow.createCell(i).setCellValue(str[i]);
                }
            }
            workBook.write(output);
            output.flush();
            output.close();
            
        } catch (Exception ex) {
            LOG.severe(ex.getMessage());
        } finally {
            try {
                workBook.close();
            } catch (IOException ex) {
                Logger.getLogger(ReportCommons.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
