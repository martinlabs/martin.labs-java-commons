package br.com.martinlabs.commons.pagarme;

import br.com.martinlabs.commons.exceptions.RespException;
import br.com.martinlabs.commons.pagarme.model.*;
import com.google.common.base.Strings;
import me.pagar.SubscriptionStatus;
import me.pagar.model.*;
import me.pagar.model.Card;
import me.pagar.model.Customer;
import me.pagar.model.Plan;
import me.pagar.model.Subscription;
import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author ftgibran
 */
public class PagarMeApi {

    /**
     * Verify if selected subscription belongs to selected plan and its payments
     * is ok
     *
     * @param i1 IPlan object. It must have pagarmePlanId
     * @param i2 ISubscription object. It must have pagarmeSubscriptionId
     * @return true if it is signed
     */
    public static boolean signedInPlan(IPlan i1, ISubscription i2, final String API_KEY) {
        PagarMe.init(API_KEY);

        String planId = i1.getPagarmePlanId();
        String subscriptionId = i2.getPagarmeSubscriptionId();

        if (subscriptionId == null || planId == null) {
            return false;
        }

        try {
            Subscription subscription = new Subscription().find(subscriptionId);
            Plan plan = subscription.getPlan();

            if (plan.getId().equals(planId)) {
                SubscriptionStatus status = subscription.getStatus();

                return !(status == SubscriptionStatus.UNPAID || status == SubscriptionStatus.ENDED);
            }

            return false;
        } catch (PagarMeException ignored) {
            return false;
        }
    }

    /**
     * Populates plan data from Pagarme into PlanoChef object Note: Requires
     * pagarmePlanId
     *
     * @param i object with pagarmePlanId
     */
    public static void getPlan(IPlan i, final String API_KEY) {
        PagarMe.init(API_KEY);

        String planId = i.getPagarmePlanId();

        try {
            Plan plan = new Plan().find(planId);

            PlanModel planModel = new PlanModel();
            planModel.setName(plan.getName());
            planModel.setAmountInCents(plan.getAmount());
            planModel.setDays(plan.getDays());
            planModel.setTrialDays(plan.getTrialDays());
            planModel.setCharges(plan.getCharges());
            planModel.setInstallments(plan.getInstallments());

            i.setPlan(planModel);
        } catch (PagarMeException e) {
            responseException(e);
        }
    }

    /**
     * Populates subscription data from Pagarme into an ISubscription object
     * Note1: Requires pagarmeSubscriptionId Note2: if pagarmeCardId is set then
     * gets the card data too
     *
     * @param i object with pagarmeSubscriptionId and pagarmeCardId
     * @param API_KEY Token auth
     */
    public static void getSubscription(ISubscription i, final String API_KEY) {
        PagarMe.init(API_KEY);

        String subscriptionId = i.getPagarmeSubscriptionId();
        String cardId = i.getPagarmeCardId();

        try {
            Subscription subscription = new Subscription().find(subscriptionId);
            Plan plan = subscription.getPlan();

            i.setSubscription(new SubscriptionModel(subscription));
            i.getSubscription().setPlan(new PlanModel(plan));

            if (!Strings.isNullOrEmpty(cardId)) {
                Card card = new Card().find(cardId);

                i.setCard(new CardModel(card));
            }
        } catch (PagarMeException e) {
            responseException(e);
        }
    }

    /**
     * Submits a new subscription to PagarMe from an ISubscription object
     *
     * @param i object data
     * @param postbackUrl url to allow PagarMe sends an event request from
     * current subscription
     * @param API_KEY Token auth
     */
    public static void createSubscription(ISubscription i, String postbackUrl, final String API_KEY) {
        PagarMe.init(API_KEY);

        String planId = i.getSubscription().getPlan().getPlanId();
        SubscriptionModel subscriptionModel = i.getSubscription();
        CardModel cardModel = i.getCard();

        //
        CustomerModel customerModel = subscriptionModel.getCustomer();
        PhoneModel phoneModel = customerModel.getPhone();
        AddressModel addressModel = customerModel.getAddress();

        Phone phone = new Phone();
        phone.setDdd(phoneModel.getDdd());
        phone.setNumber(phoneModel.getNumber());

        String street = addressModel.getStreet();
        String streetNumber = addressModel.getStreetNumber();
        String complementary = addressModel.getComplementary();
        String neighborhood = addressModel.getNeighborhood();
        String zipcode = addressModel.getZipcode();
        Address address = new Address(street, streetNumber, neighborhood, zipcode);
        address.setComplementary(complementary);

        String name = customerModel.getName();
        String email = customerModel.getEmail();
        String documentNumber = customerModel.getDocumentNumber();
        String sex = customerModel.getSex();
        Date bornAt = customerModel.getBornAt();
        Customer customer = new Customer(name, email);
        customer.setAddress(address);
        customer.setPhone(phone);
        customer.setDocumentNumber(documentNumber);
        customer.setGender(sex);
        customer.setBornAt(new LocalDate(bornAt));

        Card card = new Card();
        card.setNumber(cardModel.getCardNumber());
        card.setHolderName(cardModel.getCardHolderName());
        card.setExpiresAt(cardModel.getCardExpirationDate());
        card.setCvv(cardModel.getCvv());

        try {
            Card response = card.save();

            i.setCard(new CardModel(response));
            i.setPagarmeCardId(response.getId());
        } catch (PagarMeException e) {
            responseException(e);
        }

        try {
            Subscription subscription = new Subscription();
            subscription.setCreditCardSubscriptionWithCardId(planId, card.getId(), customer);

            if (!Strings.isNullOrEmpty(postbackUrl)) {
                subscription.setPostbackUrl(postbackUrl);
            }

            Subscription response = subscription.save();

            i.setSubscription(new SubscriptionModel(response));
            i.setPagarmeSubscriptionId(response.getId());
        } catch (PagarMeException e) {
            responseException(e);
        }
    }

    /**
     * Submits a new subscription to PagarMe from an ISubscription object Note:
     * Also it stores the first transaction in ITransaction object
     *
     * @param i1 object data
     * @param i2 object data
     * @param postbackUrl url to allow PagarMe sends an event request from
     * current subscription
     * @param API_KEY Token auth
     */
    public static void createSubscription(ISubscription i1, ITransaction i2, String postbackUrl, final String API_KEY) {
        createSubscription(i1, postbackUrl, API_KEY);
        TransactionModel transaction = i1.getSubscription().getLastTransaction();
        i2.setPagarmeTransactionId(transaction.getTransactionId());
        i2.setTransaction(transaction);
    }

    /**
     * Updates a selected subscription to PagarMe from an ISubscription object
     * Note: it is only possible updates the card of subscription
     *
     * @param i object data
     * @param API_KEY Token auth
     */
    public static void updateCard(ISubscription i, final String API_KEY) {
        PagarMe.init(API_KEY);

        CardModel cardModel = i.getCard();
        String subscriptionId = i.getPagarmeSubscriptionId();

        //
        Card card = new Card();
        card.setNumber(cardModel.getCardNumber());
        card.setHolderName(cardModel.getCardHolderName());
        card.setExpiresAt(cardModel.getCardExpirationDate());
        card.setCvv(cardModel.getCvv());

        try {
            Card response = card.save();

            Subscription subscription = new Subscription().find(subscriptionId);
            subscription.setCardId(response.getId());
            subscription.save();

            i.setCard(new CardModel(response));
            i.setPagarmeCardId(card.getId());
        } catch (PagarMeException e) {
            responseException(e);
        }
    }

    /**
     * Cancels a selected subscription Note: this operation is irreversible
     *
     * @param i object data
     * @param API_KEY Token auth
     */
    public static void cancelSubscription(ISubscription i, final String API_KEY) {
        PagarMe.init(API_KEY);

        String subscriptionId = i.getPagarmeSubscriptionId();

        try {
            Subscription subscription = new Subscription().find(subscriptionId);
            subscription.cancel();
        } catch (PagarMeException e) {
            responseException(e);
        }
    }

    /**
     *
     * @param c
     * @param API_KEY
     */
    public static void createCustomer(CustomerModel c, final String API_KEY) {
        PagarMe.init(API_KEY);

        try {

            String street = c.getAddress().getStreet();
            String streetNumber = c.getAddress().getStreetNumber();
            String complementary = c.getAddress().getComplementary();
            String neighborhood = c.getAddress().getNeighborhood();
            String zipcode = c.getAddress().getZipcode();
            Address address = new Address(street, streetNumber, neighborhood, zipcode);
            address.setComplementary(complementary);

            ArrayList<Document> docs = new ArrayList<>();
            Document document = new Document(c.getDocType(), c.getDocumentNumber());
            docs.add(document);

            Customer customer = new Customer();
            customer.setName(c.getName());
            customer.setEmail(c.getEmail());
            customer.setExternalId(c.getExternalId());
            customer.setDocumentType(c.getDocumentType());
            customer.setType(c.getType());
            customer.setCountry(c.getCountry());
            customer.setPhoneNumbers(c.getPhoneNumbers());
            customer.setDocuments(docs);
            //customer.setAddress(address);
            customer = customer.save();
            c.setCustomerId(Integer.toString(customer.getId()));
        } catch (PagarMeException e) {
            responseException(e);
        }
    }

    public static void createCard(CardModel c, final String API_KEY) {
        PagarMe.init(API_KEY);
        try {

            Card card = new Card();
            card.setCustomerId(Integer.parseInt(c.getCustomerId()));
            card.setNumber(c.getCardNumber());
            card.setCvv(c.getCvv());
            card.setExpiresAt(c.getCardExpirationDate());
            card.setHolderName(c.getCardHolderName());

            card.save();
            
            c.setValid(card.getValid());
            c.setCardId(card.getId());

        } catch (PagarMeException e) {
            responseException(e);
        }
    }

    public static void getCard(CardModel c, final String API_KEY) {
        PagarMe.init(API_KEY);
        try {

            Card card = new Card();
            card.find(c.getCardId());
            c.setLastDigits(card.getLastDigits());
            if (card.getBrand() != null) {
                c.setBrand(card.getBrand().toString());
            }

        } catch (PagarMeException e) {
            responseException(e);
        }
    }

    /**
     *
     * @param i
     * @param postBack
     * @param API_KEY
     * @param billing
     */
    public static void createTransaction(ITransaction i, String postBack, final String API_KEY, Billing billing) throws PagarMeException {
        PagarMe.init(API_KEY);

        TransactionModel transactionModel = i.getTransaction();

        Transaction transaction = new Transaction();
        transaction.setAsync(false);
        transaction.setAmount(transactionModel.getPaidAmount());
        transaction.setCardId(transactionModel.getCardId());
        transaction.setPaymentMethod(Transaction.PaymentMethod.CREDIT_CARD);

        ArrayList<Item> itens = new ArrayList<>();

        i.getTransaction().getItens().stream().forEach((item) -> {
            itens.add(
                    new Item(
                            item.getId(),
                            item.getTitle(),
                            item.getUnitPrice(),
                            item.getQuantity(),
                            true)
            );
        });

        if (!itens.isEmpty()) {
            transaction.setItems(itens);
        }

        Card card = new Card();
        card = card.find(transactionModel.getCardId());
        transaction.setCustomer(card.getCustomer());
        transaction.setPostbackUrl(postBack);
        transaction.setBilling(billing);

        Transaction t = transaction.save();
        i.setTransaction(new TransactionModel(t));
        if (t.getId() != null) {
            i.setPagarmeTransactionId(t.getId().toString());
        }
    }

    public static void cancelTransaction(ITransaction i, final String API_KEY) {
        PagarMe.init(API_KEY);

        try {
            int id = Integer.parseInt(i.getPagarmeTransactionId());
            Transaction transaction = new Transaction();

            transaction.setId(Integer.parseInt(i.getPagarmeTransactionId()));
            transaction = transaction.find(id);

            if (transaction != null && transaction.getStatus() != Transaction.Status.PROCESSING) {
                transaction.refund(transaction.getAmount());
            }

        } catch (PagarMeException e) {
            responseException(e);
        }
    }

    /**
     * General PagarMeException response
     *
     * @param e PagarMe Exception
     */
    private static void responseException(PagarMeException e) {
        if (e == null || e.getErrors() == null) {
            throw new RespException(e.getLocalizedMessage());
        }
        else {
            List<PagarMeError> errors = new ArrayList<>(e.getErrors());
            if (errors.size() > 0) {
                if (Strings.isNullOrEmpty(errors.get(0).getMessage())) {
                    throw new RespException(e.getReturnCode(), e.getLocalizedMessage());
                }
                throw new RespException(e.getReturnCode(), errors.get(0).getMessage());
            } else {
                throw new RespException(e.getReturnCode(), e.getLocalizedMessage());
            }
        }
    }

}
