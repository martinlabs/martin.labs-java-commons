package br.com.martinlabs.commons.pagarme.model;

import me.pagar.model.Transaction;

import java.util.Date;
import java.util.List;

/**
 * @author ftgibran
 */
public class TransactionModel {
    
    private String transactionId;
    private String cardId;
    private String cardHolderName;
    private String cardExpirationDate;
    private String cardNumber;
    private String cardCvv;
    private String paymentMethod;
    private String postbackUrl;
    private String localTime;
    private String refuseReason;
    private List<ItemModel> itens;
    
    private Integer paidAmount;
    private Transaction.Status status;
    private Transaction.Event event;
    private Transaction.Status oldStatus;
    private Transaction.Status currentStatus;
    private Transaction.Status desiredStatus;
    private CustomerModel customer;
    private Date createdAt;

    public TransactionModel() {
    }

    public TransactionModel(Transaction transaction) {
        if (transaction != null) {
            try
            {
                if (transaction.getId() != null) {
                    setTransactionId(transaction.getId().toString());
                }
                if (transaction.getCreatedAt() != null) {
                    setCreatedAt(transaction.getCreatedAt().toDate());
                }
            }
            catch(Exception e) {
            }
            setPaidAmount(transaction.getPaidAmount());
            setStatus(transaction.getStatus());
            setRefuseReason(transaction.getRefuseReason());
        }
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(Integer paidAmount) {
        this.paidAmount = paidAmount;
    }

    public Transaction.Status getStatus() {
        return status;
    }

    public void setStatus(Transaction.Status status) {
        this.status = status;
    }

    public Transaction.Event getEvent() {
        return event;
    }

    public void setEvent(Transaction.Event event) {
        this.event = event;
    }

    public Transaction.Status getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(Transaction.Status oldStatus) {
        this.oldStatus = oldStatus;
    }

    public Transaction.Status getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(Transaction.Status currentStatus) {
        this.currentStatus = currentStatus;
    }

    public Transaction.Status getDesiredStatus() {
        return desiredStatus;
    }

    public void setDesiredStatus(Transaction.Status desiredStatus) {
        this.desiredStatus = desiredStatus;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public CustomerModel getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerModel customer) {
        this.customer = customer;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getCardExpirationDate() {
        return cardExpirationDate;
    }

    public void setCardExpirationDate(String cardExpirationDate) {
        this.cardExpirationDate = cardExpirationDate;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardCvv() {
        return cardCvv;
    }

    public void setCardCvv(String cardCvv) {
        this.cardCvv = cardCvv;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPostbackUrl() {
        return postbackUrl;
    }

    public void setPostbackUrl(String postbackUrl) {
        this.postbackUrl = postbackUrl;
    }

    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    public List<ItemModel> getItens() {
        return itens;
    }

    public void setItens(List<ItemModel> itens) {
        this.itens = itens;
    }

    public String getRefuseReason() {
        return refuseReason;
    }

    public void setRefuseReason(String refuseReason) {
        this.refuseReason = refuseReason;
    }
}
