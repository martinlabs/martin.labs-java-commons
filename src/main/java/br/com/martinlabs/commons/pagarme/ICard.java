/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.martinlabs.commons.pagarme;

import br.com.martinlabs.commons.pagarme.model.CardModel;
import br.com.martinlabs.commons.pagarme.model.CustomerModel;

/**
 *
 * @author developer
 */
public interface ICard {
    
    // pagarmeCardId
    String getPagarmeCardId();
    void setPagarmeCardId(String cardId);
    
    // pagarmeCustomerId
    String getPagarmeCustomerId();
    void setPagarmeCustomerId(String customerId);

    /*** Request ***/
    CardModel getCard();

    /*** Response ***/
    void setCard(CardModel card);
}
