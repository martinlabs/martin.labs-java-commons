package br.com.martinlabs.commons.pagarme.model;

import me.pagar.model.Card;

/**
 * @author ftgibran
 */
public class CardModel {
    private String customerId;
    private String cardId;
    private String lastDigits; //4 digits
    private String cardNumber; //test use: 4556366941062122
    private String brand;
    private Integer cvv; //test use: 122
    private String cardHolderName;
    private String cardExpirationDate; //format: "mmyy"
    private Boolean valid;

    public CardModel() {
    }

    public CardModel(Card card) {
        if (card != null) {
            setCardId(card.getId());
            setLastDigits(card.getLastDigits());
            if (card.getBrand() != null) {
                setBrand(card.getBrand().name());
            }
            setValid(card.getValid());
        }
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getLastDigits() {
        return lastDigits;
    }

    public void setLastDigits(String lastDigits) {
        this.lastDigits = lastDigits;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Integer getCvv() {
        return cvv;
    }

    public void setCvv(Integer cvv) {
        this.cvv = cvv;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getCardExpirationDate() {
        return cardExpirationDate;
    }

    public void setCardExpirationDate(String cardExpirationDate) {
        this.cardExpirationDate = cardExpirationDate;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
    
    
}
