package br.com.martinlabs.commons.pagarme.model;

import java.util.Date;
import java.util.List;
import me.pagar.model.Customer.Type;
import me.pagar.model.Document;

/**
 * @author ftgibran
 */
public class CustomerModel {
    private Type type;
    private Document.Type docType;
    private String customerId;
    private String name;
    private String email;
    private String documentNumber;
    private String documentType;
    private String sex;
    private Date bornAt;
    private String bornAtFormat; //dd-mm-yyyy
    private String externalId;
    private String country;
    private List<String> phoneNumbers;
    private List<Document> documents;
    
    private AddressModel address;
    private PhoneModel phone;

    public CustomerModel() {
    }

    public CustomerModel(String name, String email) {
        this.name = name;
        this.email = email;
    }
    
    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBornAt() {
        return bornAt;
    }

    public void setBornAt(Date bornAt) {
        this.bornAt = bornAt;
    }

    public String getBornAtFormat() {
        return bornAtFormat;
    }

    public void setBornAtFormat(String bornAtFormat) {
        this.bornAtFormat = bornAtFormat;
    }

    public AddressModel getAddress() {
        return address;
    }

    public void setAddress(AddressModel address) {
        this.address = address;
    }

    public PhoneModel getPhone() {
        return phone;
    }

    public void setPhone(PhoneModel phone) {
        this.phone = phone;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Document.Type getDocType() {
        return docType;
    }

    public void setDocType(Document.Type docType) {
        this.docType = docType;
    }
    
    
    
    
}