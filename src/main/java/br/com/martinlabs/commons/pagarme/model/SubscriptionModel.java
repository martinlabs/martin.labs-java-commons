package br.com.martinlabs.commons.pagarme.model;

import me.pagar.SubscriptionStatus;
import me.pagar.model.Subscription;
import me.pagar.model.Transaction;

import java.util.Date;

/**
 * @author ftgibran
 */
public class SubscriptionModel {
    private String subscriptionId;
    private SubscriptionStatus status;
    private Integer charges;
    private Date currentPeriodStart;
    private Date currentPeriodEnd;
    private Date createdAt;
    private Transaction.PaymentMethod paymentMethod;

    private PlanModel plan;
    private CustomerModel customer;
    private TransactionModel lastTransaction;

    public SubscriptionModel() {
    }

    public SubscriptionModel(Subscription subscription) {
        if (subscription != null) {
            Transaction transaction = subscription.getCurrentTransaction();

            setSubscriptionId(subscription.getId());
            setStatus(subscription.getStatus());
            setCreatedAt(subscription.getCreatedAt().toDate());
            setCharges(subscription.getCharges());
            setCurrentPeriodStart(subscription.getCurrentPeriodStart().toDate());
            setCurrentPeriodEnd(subscription.getCurrentPeriodEnd().toDate());
            setPaymentMethod(subscription.getPaymentMethod());
            setLastTransaction(new TransactionModel(transaction));
        }
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public SubscriptionStatus getStatus() {
        return status;
    }

    public void setStatus(SubscriptionStatus status) {
        this.status = status;
    }

    public Integer getCharges() {
        return charges;
    }

    public void setCharges(Integer charges) {
        this.charges = charges;
    }

    public Date getCurrentPeriodStart() {
        return currentPeriodStart;
    }

    public void setCurrentPeriodStart(Date currentPeriodStart) {
        this.currentPeriodStart = currentPeriodStart;
    }

    public Date getCurrentPeriodEnd() {
        return currentPeriodEnd;
    }

    public void setCurrentPeriodEnd(Date currentPeriodEnd) {
        this.currentPeriodEnd = currentPeriodEnd;
    }

    public Transaction.PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Transaction.PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public PlanModel getPlan() {
        return plan;
    }

    public void setPlan(PlanModel plan) {
        this.plan = plan;
    }

    public CustomerModel getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerModel customer) {
        this.customer = customer;
    }

    public TransactionModel getLastTransaction() {
        return lastTransaction;
    }

    public void setLastTransaction(TransactionModel lastTransaction) {
        this.lastTransaction = lastTransaction;
    }
}
