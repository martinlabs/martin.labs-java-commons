package br.com.martinlabs.commons.pagarme;

import br.com.martinlabs.commons.pagarme.model.TransactionModel;

/**
 * @author ftgibran
 */
public interface ITransaction {
    // pagarmeTransactionId
    String getPagarmeTransactionId();
    void setPagarmeTransactionId(String transactionId);

    /*** Request ***/
    TransactionModel getTransaction();

    /*** Response ***/
    void setTransaction(TransactionModel transaction);
}
