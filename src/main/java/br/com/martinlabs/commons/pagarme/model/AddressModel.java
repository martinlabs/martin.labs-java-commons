package br.com.martinlabs.commons.pagarme.model;

/**
 * @author ftgibran
 */
public class AddressModel {
    private String street;
    private String streetNumber;
    private String complementary;
    private String neighborhood;
    private String zipcode;
    private String state;
    private String city;

    public AddressModel() {
    }

    public AddressModel(String street, String streetNumber, String neighborhood, String zipcode) {
        this.street = street;
        this.streetNumber = streetNumber;
        this.neighborhood = neighborhood;
        this.zipcode = zipcode;
    }

    public AddressModel(String street, String streetNumber, String neighborhood, String zipcode, String state, String city) {
        this.street = street;
        this.streetNumber = streetNumber;
        this.neighborhood = neighborhood;
        this.zipcode = zipcode;
        this.state = state;
        this.city = city;
    }
    
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getComplementary() {
        return complementary;
    }

    public void setComplementary(String complementary) {
        this.complementary = complementary;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    
    
}
