package br.com.martinlabs.commons.pagarme;

import br.com.martinlabs.commons.exceptions.RespException;
import com.google.common.base.Strings;
import com.google.gson.annotations.SerializedName;
import me.pagar.SubscriptionStatus;
import me.pagar.model.PagarMe;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;

/**
 * @author ftgibran
 * Params class used for PagarMe POSTBACK
 */
public class Postback extends HashMap<String, String> {
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    public Postback() {
        super();
    }

    public Postback(String payload, String signature, final String API_KEY) {
        super();
        allowPostbackWithParams(payload, signature, API_KEY);
    }

    /**
     * Checks permissions of a requested POSTBACK and generates a Params object from String payload
     *
     * @param payload   body of request
     * @param signature X-Hub-Signature from header
     */
    public void allowPostbackWithParams(final String payload, final String signature, final String API_KEY) {
        PagarMe.init(API_KEY);

        boolean isValid = validateRequestSignature(payload, signature, API_KEY);
        if (!isValid) throw new RespException("Acesso Restrito");

        try {
            String[] parts = URLDecoder.decode(payload, "ASCII").split("&");

            for (String part : parts) {
                String[] keyValue = part.split("=");

                if (keyValue.length == 2) {
                    String key = keyValue[0];
                    String value = keyValue[1];
                    this.put(key, value);
                } else if (keyValue.length == 1) {
                    String key = keyValue[0];
                    this.put(key, null);
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static boolean validateRequestSignature(final String payload, final String signature, final String API_KEY) {
        final String HMAC_MD5_ALGORITHM = "HmacMD5";
        final String HMAC_SHA1_ALGORITHM = "HmacSHA1";
        final String HMAC_SHA256_ALGORITHM = "HmacSHA256";
        final String SHA1_ALGORITHM = "sha1";
        final String SHA256_ALGORITHM = "sha256";
        final String ASCII = "ASCII";

        if (Strings.isNullOrEmpty(payload) || Strings.isNullOrEmpty(signature)) {
            return false;
        }

        final String[] parts = signature.split("=");

        try {
            // get an hmac_sha1 key from the raw key bytes
            final SecretKeySpec signingKey = new SecretKeySpec(API_KEY.getBytes(ASCII), parts[0]);

            String algorithm = HMAC_MD5_ALGORITHM;

            if (parts[0].equalsIgnoreCase(SHA1_ALGORITHM)) {
                algorithm = HMAC_SHA1_ALGORITHM;
            } else if (parts[0].equalsIgnoreCase(SHA256_ALGORITHM)) {
                algorithm = HMAC_SHA256_ALGORITHM;
            }

            // get an hmac_sha1 Mac instance and initialize with the signing key
            final Mac mac = Mac.getInstance(algorithm);
            mac.init(signingKey);

            // compute the hmac on input data bytes
            final byte[] rawHmac = mac.doFinal(payload.getBytes(ASCII));

            final Formatter formatter = new Formatter();

            // right transform into sha1 hash
            for (byte b : rawHmac) {
                formatter.format("%02x", 0xFF & b);
            }

            final String hash = formatter.toString();

            return (parts.length == 2) && (hash.equals(parts[1]));
        } catch (Exception e) {
            return false;
        }
    }

    public String getString(Object key) {
        return get(key);
    }

    public Double getDouble(Object key) {
        return Double.parseDouble(get(key));
    }

    public Date getDate(Object key) {
        try {
            return format.parse(get(key));
        } catch (ParseException e) {
            return null;
        }
    }

    public SubscriptionStatus getSubscriptionStatus(Object key) {
        try {
            return SubscriptionStatus.valueOf(get(key).toUpperCase());
        } catch (IllegalArgumentException ignored) {
            return null;
        }
    }

    public Event getEvent(Object key) {
        try {
            return Event.valueOf(get(key).toUpperCase());
        } catch (IllegalArgumentException ignored) {
            return null;
        }
    }

    public enum Event {
        @SerializedName("transaction_status_changed")
        TRANSACTION_STATUS_CHANGED,
        @SerializedName("subscription_status_changed")
        SUBSCRIPTION_STATUS_CHANGED,
        @SerializedName("recipient_status_changed")
        RECIPIENT_STATUS_CHANGED,
        @SerializedName("transaction_created")
        TRANSACTION_CREATED;

        private Event() {
        }
    }

}

