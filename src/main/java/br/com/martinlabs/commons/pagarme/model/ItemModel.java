/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.martinlabs.commons.pagarme.model;

/**
 *
 * @author developer
 */
public class ItemModel {
    
 private String id;
 private String title; // Nome do item vendido.
 private int unitPrice; //Ex R$20,06, o valor deve ser fornecido como ‘2006’
 private int quantity;
 private Boolean tangible; // Caracteriza o produto como bem físico ou não

    public ItemModel(String id, String title, int unitPrice, int quantity, Boolean tangible) {
        this.id = id;
        this.title = title;
        this.unitPrice = unitPrice;
        this.quantity = quantity;
        this.tangible = tangible;
    }
 
 

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Boolean getTangible() {
        return tangible;
    }

    public void setTangible(Boolean tangible) {
        this.tangible = tangible;
    }
 
 

}
