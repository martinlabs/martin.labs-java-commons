package br.com.martinlabs.commons.pagarme.model;

/**
 * @author ftgibran
 */
public class PhoneModel {
    private String ddi;
    private String ddd;
    private String number;

    public PhoneModel() {
    }

    public PhoneModel(String ddd, String number) {
        this.ddd = ddd;
        this.number = number;
    }

    public String getDdi() {
        return ddi;
    }

    public void setDdi(String ddi) {
        this.ddi = ddi;
    }

    public String getDdd() {
        return ddd;
    }

    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
