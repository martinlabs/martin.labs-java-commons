/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.martinlabs.commons.pagarme;

import br.com.martinlabs.commons.pagarme.model.CustomerModel;

/**
 *
 * @author developer
 */
public interface ICustomer {
    
    // pagarmeCustomerId
    String getPagarmeCostumerId();
    void setPagarmeCostumeId(String planId);

    /*** Request ***/
    CustomerModel getCustomer();

    /*** Response ***/
    void setCostumer(CustomerModel costumer);
    
}