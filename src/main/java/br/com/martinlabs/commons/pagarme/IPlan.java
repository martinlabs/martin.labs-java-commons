package br.com.martinlabs.commons.pagarme;

import br.com.martinlabs.commons.pagarme.model.PlanModel;

/**
 * @author ftgibran
 */
public interface IPlan {
    // pagarmePlanId
    String getPagarmePlanId();
    void setPagarmePlanId(String planId);

    /*** Request ***/
    PlanModel getPlan();

    /*** Response ***/
    void setPlan(PlanModel plan);
}
