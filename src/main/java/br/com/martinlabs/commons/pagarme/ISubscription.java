package br.com.martinlabs.commons.pagarme;

import br.com.martinlabs.commons.pagarme.model.CardModel;
import br.com.martinlabs.commons.pagarme.model.SubscriptionModel;

/**
 * @author ftgibran
 */
public interface ISubscription {
    // pagarmeSubscritpionId
    String getPagarmeSubscriptionId();
    void setPagarmeSubscriptionId(String subscriptionId);

    // pagarmeCardId
    String getPagarmeCardId();
    void setPagarmeCardId(String cardId);

    /*** Request ***/
    SubscriptionModel getSubscription();
    CardModel getCard();

    /*** Response ***/
    void setSubscription(SubscriptionModel subscription);
    void setCard(CardModel card);
}
