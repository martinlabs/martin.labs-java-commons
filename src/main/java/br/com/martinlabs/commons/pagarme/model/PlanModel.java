package br.com.martinlabs.commons.pagarme.model;

import me.pagar.model.Plan;

import java.util.Date;

/**
 * @author ftgibran
 */
public class PlanModel {
    private String planId;
    private String name;
    private Integer amountInCents; //in cents
    private Integer days;
    private Integer trialDays;
    private Date dateCreated;
    private String[] paymentMethods = {"credit_card"};
    private String color;
    private Integer charges;
    private Integer installments;
    private Integer invoiceReminder;
    private Integer paymentDeadlineChargesInterval;

    public PlanModel() {
    }

    public PlanModel(Plan plan) {
        if (plan != null) {
            setPlanId(plan.getId());
            setName(plan.getName());
            setAmountInCents(plan.getAmount());
            setDays(plan.getDays());
            setTrialDays(plan.getTrialDays());
            setCharges(plan.getCharges());
            setInstallments(plan.getInstallments());
        }
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmountInCents() {
        return amountInCents;
    }

    public void setAmountInCents(Integer amountInCents) {
        this.amountInCents = amountInCents;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public Integer getTrialDays() {
        return trialDays;
    }

    public void setTrialDays(Integer trialDays) {
        this.trialDays = trialDays;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String[] getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(String[] paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getCharges() {
        return charges;
    }

    public void setCharges(Integer charges) {
        this.charges = charges;
    }

    public Integer getInstallments() {
        return installments;
    }

    public void setInstallments(Integer installments) {
        this.installments = installments;
    }

    public Integer getInvoiceReminder() {
        return invoiceReminder;
    }

    public void setInvoiceReminder(Integer invoiceReminder) {
        this.invoiceReminder = invoiceReminder;
    }

    public Integer getPaymentDeadlineChargesInterval() {
        return paymentDeadlineChargesInterval;
    }

    public void setPaymentDeadlineChargesInterval(Integer paymentDeadlineChargesInterval) {
        this.paymentDeadlineChargesInterval = paymentDeadlineChargesInterval;
    }
}
