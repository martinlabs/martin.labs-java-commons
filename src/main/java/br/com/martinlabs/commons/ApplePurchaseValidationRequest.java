package br.com.martinlabs.commons;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author ricardoprado
 */
public class ApplePurchaseValidationRequest {
    
    //Base64 string
     @SerializedName("receipt-data")
    private String receipt_data;
     //Para assinaturas recorrentes;
    private String password;

    public String getReceipt_data() {
        return receipt_data;
    }

    public void setReceipt_data(String receipt_data) {
        this.receipt_data = receipt_data;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
