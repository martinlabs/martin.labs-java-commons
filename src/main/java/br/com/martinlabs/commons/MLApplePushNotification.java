package br.com.martinlabs.commons;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import javapns.notification.PushedNotifications;
import javapns.test.NotificationTest;
import org.json.JSONException;

/**
 *
 * @author Kobayashi
 */
public class MLApplePushNotification {

    private String keystoreIOSProduction;
    private String keystoreIOSSandbox;
    private String senhaKeyStoreProduction;
    private String senhaKeyStoreSandbox;
    private boolean isSandbox;

    private static final String KEYSTORE_PRODUCTION = "pushAppleProducao.p12";
    private static final String KEYSTORE_SANDBOX = "pushAppleSandbox.p12";
    private static final String DEFAULT_PASSWORD = "12345";

    public MLApplePushNotification(String keystoreIOSProduction,
            String keystoreIOSSandbox,
            String senhaKeyStoreProduction,
            String senhaKeyStoreSandbox,
            boolean isSandbox) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        this.keystoreIOSProduction = classloader.getResource(keystoreIOSProduction).getFile();
        this.keystoreIOSSandbox = classloader.getResource(keystoreIOSSandbox).getFile();
        this.senhaKeyStoreProduction = senhaKeyStoreProduction;
        this.senhaKeyStoreSandbox = senhaKeyStoreSandbox;
        this.isSandbox = isSandbox;
    }

    public MLApplePushNotification(boolean isSandbox) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        this.keystoreIOSProduction = classloader.getResource(KEYSTORE_PRODUCTION).getFile();
        this.keystoreIOSSandbox = classloader.getResource(KEYSTORE_SANDBOX).getFile();
        this.senhaKeyStoreProduction = DEFAULT_PASSWORD;
        this.senhaKeyStoreSandbox = DEFAULT_PASSWORD;
        this.isSandbox = isSandbox;
    }

    public MLApplePushNotification(String keystoreProducao, String keystoreSandbox, boolean ehSandboxArg) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        keystoreIOSProduction = classloader.getResource(keystoreProducao).getFile();
        keystoreIOSSandbox = classloader.getResource(keystoreSandbox).getFile();
        senhaKeyStoreProduction = DEFAULT_PASSWORD;
        senhaKeyStoreSandbox = DEFAULT_PASSWORD;
        isSandbox = ehSandboxArg;
    } 

    public interface ICallbackInvalidTokenPush {

        public void invalidTokens(List<PushedNotification> unsuccessfull, List<PushedNotification> expections, List<PushedNotification> responses);
    }
    
    
    public void sendWithObject(final String text, final List<String> idsToken, final Object objectPayload) {
        ThreadPool.queue(new Runnable() {
            public void run() {
                try {
                    if (isSandbox) {
                        sendSandboxWithObject(text, idsToken, objectPayload);
                    } else {
                        sendProductionWithObject(text, idsToken, new ICallbackInvalidTokenPush() {
                            public void invalidTokens(List<PushedNotification> unsuccessfull, List<PushedNotification> expections, List<PushedNotification> resps) {
                            }
                        }, objectPayload);
                    }
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
        });
    }
    
    /**
     * Send a push notification to iOS device. There is no Sandbox in Android
     *
     * @param text
     */
    public void send(final String text, final List<String> idsToken, final int badge) {
        ThreadPool.queue(new Runnable() {
            public void run() {
                try {
                    if (isSandbox) {
                        sendSandbox(text, idsToken);
                    } else {
                        sendProduction(text, idsToken, new ICallbackInvalidTokenPush() {
                            public void invalidTokens(List<PushedNotification> unsuccessfull, List<PushedNotification> expections, List<PushedNotification> resps) {
                            }
                        }, badge);
                    }
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
        });
    }
    
    public void send(final String text, final List<String> idsToken, final ICallbackInvalidTokenPush callback, int badge) {
        ThreadPool.queue(new Runnable() {
            public void run() {
                try {
                    if (isSandbox) {
                        sendSandbox(text, idsToken);
                    } else {
                        sendProduction(text, idsToken, callback, badge);
                    }
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
        });
    }
    
    public void send(final String text, final List<String> idsToken, final String soundName) {
        ThreadPool.queue(new Runnable() {
            public void run() {
                try {
                    if (isSandbox) {
                        sendSandbox(text, idsToken, soundName);
                    } else {
                        sendProduction(text, idsToken, soundName);
                    }
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
        });
    }
    
    public void sendWithObject(final String text, final String idToken, final Object objectPayload) {
        ThreadPool.queue(new Runnable() {
            public void run() {
                try {
                    if (isSandbox) {
                        sendSandboxWithObject(text, idToken, objectPayload);
                    } else {
                        sendProductionWithObject(text, idToken, objectPayload);
                    }
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
        });
    }
    
    public void send(final String text, final String idToken, final int badge) {
        ThreadPool.queue(new Runnable() {
            public void run() {
                try {
                    if (isSandbox) {
                        sendSandbox(text, idToken);
                    } else {
                        sendProduction(text, idToken, badge);
                    }
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
        });
    }
    
    public void send(final String text, final String idToken, final String soundName) {
        ThreadPool.queue(new Runnable() {
            public void run() {
                try {
                    if (isSandbox) {
                        sendSandbox(text, idToken, soundName);
                    } else {
                        sendProduction(text, idToken, soundName);
                    }
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
        });
    }
    
    public void send(final javapns.notification.Payload payload, final String idToken) {
        ThreadPool.queue(new Runnable() {
            public void run() {
                try {
                    if (isSandbox) {
                        sendSandbox(payload, idToken);
                    } else {
                        sendProduction(payload, idToken);
                    }
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
        });
    }
    
    public void send(final javapns.notification.Payload payload, final List<String> idTokens) {
        ThreadPool.queue(new Runnable() {
            public void run() {
                try {
                    if (isSandbox) {
                        sendSandbox(payload, idTokens);
                    } else {
                        sendProduction(payload, idTokens);
                    }
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
        });
    }
    
    public void sendSync(final javapns.notification.Payload payload, final String idToken) {
        if (isSandbox) {
            sendSandbox(payload, idToken);
        } else {
            sendProduction(payload, idToken);
        }
    }

    private void sendProduction(javapns.notification.Payload payload, String idToken) {
        try {
            Push.payload(payload, keystoreIOSProduction, senhaKeyStoreProduction, true, idToken);
        } catch (CommunicationException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } catch (KeystoreException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    private void sendProduction(javapns.notification.Payload payload, List<String> idTokens) {
        try {
            Push.payload(payload, keystoreIOSProduction, senhaKeyStoreProduction, true, idTokens);
        } catch (CommunicationException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } catch (KeystoreException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    private void sendSandbox(javapns.notification.Payload payload, String idToken) {
        try {
            PushedNotifications payload1 = Push.payload(payload, keystoreIOSSandbox, senhaKeyStoreSandbox, false, idToken);
            NotificationTest.printPushedNotifications(payload1);
        } catch (CommunicationException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } catch (KeystoreException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    private void sendSandbox(javapns.notification.Payload payload, List<String> idTokens) {
        try {
            PushedNotifications payload1 = Push.payload(payload, keystoreIOSSandbox, senhaKeyStoreSandbox, false, idTokens);
            NotificationTest.printPushedNotifications(payload1);
        } catch (CommunicationException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } catch (KeystoreException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    private void sendSandbox(String text, List<String> idsToken, String soundName) {
        PushNotificationPayload payload = PushNotificationPayload.alert(text);
        try {
            payload.addBadge(1);
            payload.addSound(soundName);
            PushedNotifications push = Push.payload(payload, keystoreIOSSandbox, senhaKeyStoreSandbox, false, idsToken);
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Push enviado para {0}", idsToken);
            NotificationTest.printPushedNotifications(push);
        } catch (JSONException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        } catch (CommunicationException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        } catch (KeystoreException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        }
    }

    private void sendSandbox(String text, List<String> idsToken) {
        PushNotificationPayload payload = PushNotificationPayload.alert(text);
        try {
            payload.addBadge(1);
            PushedNotifications push = Push.payload(payload, keystoreIOSSandbox, senhaKeyStoreSandbox, false, idsToken);
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Push enviado para {0}", idsToken);
            NotificationTest.printPushedNotifications(push);
        } catch (JSONException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        } catch (CommunicationException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        } catch (KeystoreException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        }
    }
    
    private void sendSandboxWithObject(String text, List<String> idsToken, Object objectPayload) {
        PushNotificationPayload payload = PushNotificationPayload.alert(text);
        try {
            if(objectPayload != null){
                payload.addCustomDictionary("object", objectPayload);
            }payload.addBadge(1);
            PushedNotifications push = Push.payload(payload, keystoreIOSSandbox, senhaKeyStoreSandbox, false, idsToken);
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Push enviado para {0}", idsToken);
            NotificationTest.printPushedNotifications(push);
        } catch (JSONException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        } catch (CommunicationException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        } catch (KeystoreException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        }
    }

    private void sendProduction(String text, List<String> idToken, String soundName) {
        PushNotificationPayload payload = PushNotificationPayload.alert(text);
        try {
            payload.addBadge(1);
            payload.addSound(soundName);
            PushedNotifications push = Push.payload(payload, keystoreIOSProduction, senhaKeyStoreProduction, true, idToken);
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Push enviado para {0}", idToken);
            NotificationTest.printPushedNotifications(push);
        } catch (JSONException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        } catch (CommunicationException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        } catch (KeystoreException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        }
    }
    
    private void sendProductionWithObject(String text, List<String> idToken, ICallbackInvalidTokenPush callback, Object objectPayload) {
        PushNotificationPayload payload = PushNotificationPayload.alert(text);
        try {
            if(objectPayload != null){  
                payload.addCustomDictionary("object", objectPayload);
            }
            payload.addBadge(1);
            List<PushedNotification> unsuccessfull = new ArrayList<PushedNotification>();
            List<PushedNotification> responsespkgts = new ArrayList<PushedNotification>();
            List<PushedNotification> exceptions = new ArrayList<PushedNotification>();
            PushedNotifications pushes = Push.payload(payload, keystoreIOSProduction, senhaKeyStoreProduction, true, idToken);
            for (PushedNotification pushindividual : pushes) {
                if (!pushindividual.isSuccessful()) {
                    unsuccessfull.add(pushindividual);
                }
                if (!pushindividual.isTransmissionCompleted()) {
                    unsuccessfull.add(pushindividual);                        
                }
                if (pushindividual.getResponse() != null) {
                    responsespkgts.add(pushindividual);
                }
                if (pushindividual.getException() != null) {
                    exceptions.add(pushindividual);
                }
            }
            NotificationTest.printPushedNotifications(pushes);
            callback.invalidTokens(unsuccessfull, exceptions, responsespkgts);
        } catch (JSONException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        } catch (CommunicationException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        } catch (KeystoreException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        }
    }
    
    
    private void sendProduction(String text, List<String> idToken, ICallbackInvalidTokenPush callback, int badge) {
        PushNotificationPayload payload = PushNotificationPayload.alert(text);
        try {
            payload.addBadge(badge);
            List<PushedNotification> unsuccessfull = new ArrayList<PushedNotification>();
            List<PushedNotification> responsespkgts = new ArrayList<PushedNotification>();
            List<PushedNotification> exceptions = new ArrayList<PushedNotification>();
            PushedNotifications pushes = Push.payload(payload, keystoreIOSProduction, senhaKeyStoreProduction, true, idToken);
            for (PushedNotification pushindividual : pushes) {
                if (!pushindividual.isSuccessful()) {
                    unsuccessfull.add(pushindividual);
                }
                if (!pushindividual.isTransmissionCompleted()) {
                    unsuccessfull.add(pushindividual);                        
                }
                if (pushindividual.getResponse() != null) {
                    responsespkgts.add(pushindividual);
                }
                if (pushindividual.getException() != null) {
                    exceptions.add(pushindividual);
                }
            }
            NotificationTest.printPushedNotifications(pushes);
            callback.invalidTokens(unsuccessfull, exceptions, responsespkgts);
        } catch (JSONException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        } catch (CommunicationException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        } catch (KeystoreException ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, "Erro ao enviar push:" + ex.getLocalizedMessage(), ex);
        }
    }

    private void sendSandbox(String text, String idToken, String soundName) {
        List<String> tokens = new ArrayList<String>();
        tokens.add(idToken);
        sendSandbox(text, tokens, soundName);
    }

    private void sendProduction(String text, String idToken, String soundName) {
        List<String> tokens = new ArrayList<String>();
        tokens.add(idToken);
        sendProduction(text, tokens, soundName);
    }

    private void sendSandboxWithObject(String text, String idToken, Object objectPayload) {
        List<String> tokens = new ArrayList<String>();
        tokens.add(idToken);
        sendSandboxWithObject(text, tokens, objectPayload);
    }
    
    private void sendSandbox(String text, String idToken) {
        List<String> tokens = new ArrayList<String>();
        tokens.add(idToken);
        sendSandbox(text, tokens);
    }
    
    private void sendProductionWithObject(String text, String idToken, Object objectPayload) {
        List<String> tokens = new ArrayList<String>();
        tokens.add(idToken);
        sendProductionWithObject(text, tokens, new ICallbackInvalidTokenPush() {
            public void invalidTokens(List<PushedNotification> unsuccessfull, List<PushedNotification> expections, List<PushedNotification> resps) {
            }
        }, objectPayload);
    }

    private void sendProduction(String text, String idToken, int badge) {
        List<String> tokens = new ArrayList<String>();
        tokens.add(idToken);
        sendProduction(text, tokens, new ICallbackInvalidTokenPush() {
            public void invalidTokens(List<PushedNotification> unsuccessfull, List<PushedNotification> expections, List<PushedNotification> resps) {
            }
        }, badge);
    }

    @Deprecated
    public void enviarPushTextoParaIOS(String text, List<String> idsToken) {
        send(text, idsToken, 1);
    }  

    @Deprecated
    public void enviarPushTextoParaIOS(String texto, List<String> idsToken, ICallbackInvalidTokenPush callback) {
        send(texto, idsToken, callback, 1);
    }

    @Deprecated
    public void enviarPushTextoParaIOS(final String text, final List<String> idsToken, final String soundName) {
        send(text, idsToken, soundName);
    }
    
    @Deprecated
    public void enviarPushTextoParaIOS(final String text, final String idToken) {
        send(text, idToken, 1);
    }

    @Deprecated
    public void enviarPushTextoParaIOS(final String text, final String idToken, final String soundName) {
        send(text, idToken, soundName);
    }

    @Deprecated
    public void enviarPushComplexoParaIOS(final javapns.notification.Payload payload, final String idToken) {
        send(payload, idToken);
    }
}
