package br.com.martinlabs.commons;

import br.com.martinlabs.commons.exceptions.RespException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A solution to encapsulate the servlet, using a ServletContent
 *
 * @author gil
 */
public abstract class ServletWrapper extends HttpServlet {

    protected void processRequest(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        
        ServletContent content = createServletContent();
        
        content.context = getServletContext();
        
        content.request = req;
        content.request.setCharacterEncoding(content.charset);
        
        content.response = res;

        content.response.setContentType(content.contentType);
        PrintWriter out = content.response.getWriter();

        try {
            Object result = process(content);
            if (result instanceof String) {
                out.print(result);
            } else {
                out.print(content.gson.toJson(result));
            }

        } catch (RespException re) {
            Logger.getLogger(ServletWrapper.class.getName()).log(Level.INFO, re.getMessage(), re);
            out.print(content.gson.toJson(re.getOperationResponse()));
        } catch (Throwable e) {
            Logger.getLogger(ServletWrapper.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            out.print(content.gson.toJson(new OpResponse(false, LanguageHolder.instance.unexpectedError())));
        } finally {
            out.close();
        }
    }
    
    protected abstract Object process(ServletContent content);

    protected ServletContent createServletContent() {
        return new ServletContent();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
