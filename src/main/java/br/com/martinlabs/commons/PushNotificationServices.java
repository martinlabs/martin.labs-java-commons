package br.com.martinlabs.commons;

import br.com.martinlabs.commons.MLApplePushNotification.ICallbackInvalidTokenPush;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import javax.net.ssl.HttpsURLConnection;
import org.apache.log4j.BasicConfigurator;

/**
 *
 * @author ricardomeira
 */
public class PushNotificationServices {

    private static MLApplePushNotification staticAppleNotification;
    private MLApplePushNotification instanceAppleNotification;
    private static String staticKeyAndroidConsole = null;
    private String instanceKeyAndroidConsole = null;
    private static final Gson gson = new Gson();
    public static ObjectMapper mapper = new ObjectMapper();
    private static Boolean useFcm = false;
    private static final String urlGcm = "https://android.googleapis.com/gcm/send";
    private static final String urlFcm = "https://fcm.googleapis.com/fcm/send";

    static {
        BasicConfigurator.configure();
    }

    public PushNotificationServices(
            String ksProduction,
            String ksSandbox,
            String passwordKsProduction,
            String passwordKsSandbox,
            String keyAndroidConsoleArg,
            boolean ehSandboxArg){
        
        instanceAppleNotification = new MLApplePushNotification(ksProduction, ksSandbox, passwordKsProduction, passwordKsSandbox, ehSandboxArg);
        instanceKeyAndroidConsole = keyAndroidConsoleArg;
    }
    
    public PushNotificationServices(String keyAndroidConsoleArg, boolean isSandbox) {
        instanceAppleNotification = new MLApplePushNotification(isSandbox);
        instanceKeyAndroidConsole = keyAndroidConsoleArg;
    }
    
    @Deprecated
    public static void Initialize(String ksProduction,
            String ksSandbox,
            String passwordKsProduction,
            String passwordKsSandbox,
            String keyAndroidConsoleArg,
            boolean ehSandboxArg) {
        staticAppleNotification = new MLApplePushNotification(ksProduction, ksSandbox, passwordKsProduction, passwordKsSandbox, ehSandboxArg);
        staticKeyAndroidConsole = keyAndroidConsoleArg;
    }

    @Deprecated
    public static void Initialize(String keyAndroidConsoleArg, boolean isSandbox) {
        staticAppleNotification = new MLApplePushNotification(isSandbox);
        staticKeyAndroidConsole = keyAndroidConsoleArg;
    }

    @Deprecated
    public static void InitializeAndroid(String keyAndroidConsoleArg, boolean isSandbox) {
        staticKeyAndroidConsole = keyAndroidConsoleArg;
    }

    public static void UseFcm() {
        useFcm = true;
    }
    
    public void instanceSendToAndroidInBackground(final String text, final String idToken, final boolean encode) {
        ThreadPool.queue(new Runnable() {
            public void run() {
                instanceSendToAndroid(text, idToken, encode);
            }
        });
    }
    
    public void instanceSendToAndroidInBackgroundSync(final String text, final String idToken, final boolean encode) {
        instanceSendToAndroid(text, idToken, encode);
    }

    public static void staticSendToAndroidInBackground(final String text, final String idToken, final boolean encode) {
        ThreadPool.queue(new Runnable() {
            public void run() {
                staticSendToAndroid(text, idToken, encode);
            }
        });
    }

    public void instanceSendToAndroidInBackground(final String text, final List<String> registrationIds, final boolean encode) {
        ThreadPool.queue(new Runnable() {
            public void run() {
                instanceSendToAndroid(text, registrationIds, encode);
            }
        });
    }
    
    @Deprecated
    public static void staticSendToAndroidInBackground(final String text, final List<String> registrationIds, final boolean encode) {
        ThreadPool.queue(new Runnable() {
            public void run() {
                staticSendToAndroid(text, registrationIds, encode);
            }
        });
    }

    @Deprecated
    public static void staticSendToAndroid(final Object objeto, final String idToken) {
        staticSendToAndroidInBackground(gson.toJson(objeto), idToken, false);
    }

    @Deprecated
    public static void staticSendToAndroid(final String text, final String idToken) {
        staticSendToAndroidInBackground(text, idToken, true);
    }

    @Deprecated
    public static void staticSendToAndroid(final Object objeto, final List<String> registrationIds) {
        staticSendToAndroidInBackground(gson.toJson(objeto), registrationIds, false);
    }

    @Deprecated
    public static void staticSendToAndroid(final String text, final List<String> registrationIds) {
        staticSendToAndroidInBackground(text, registrationIds, true);
    }

    @Deprecated
    public static void staticSendToIOS(final String text, final List<String> idsToken, int badge) {
        staticAppleNotification.send(text, idsToken, badge);
    }
    
    public void instanceSendToIOS(final String text, final List<String> idsToken, final ICallbackInvalidTokenPush callback, int badge) {
        instanceAppleNotification.send(text, idsToken, callback, badge);
    }

    @Deprecated
    public static void staticSendToIOS(final String text, final List<String> idsToken, final ICallbackInvalidTokenPush callback, int badge) {
        staticAppleNotification.send(text, idsToken, callback, badge);
    }

    @Deprecated
    public static void staticSendToIOS(final String text, final List<String> idsToken, final String soundName) {
        staticAppleNotification.send(text, idsToken, soundName);
    }

    @Deprecated
    public static void staticSendToIOS(final String text, final String idToken, int badge) {
        staticAppleNotification.send(text, idToken, badge);
    }
    
    public void instanceSendToIOS(final String text, final String idToken, int badge) {
        instanceAppleNotification.send(text, idToken, badge);
    }
    
    public void instanceSendToIOS(final javapns.notification.Payload payload, final String idToken) {
        instanceAppleNotification.send(payload, idToken);
    }
    
    public void instanceSendToIOS(final javapns.notification.Payload payload, final List<String> idTokens) {
        instanceAppleNotification.send(payload, idTokens);
    }
    
    public void instanceSendToIOSSync(final javapns.notification.Payload payload, final String idToken) {
        instanceAppleNotification.sendSync(payload, idToken);
    }

    @Deprecated
    public static void staticSendToIOSWithObject(final String text, final String idToken, final Object objectPayload) {
        staticAppleNotification.sendWithObject(text, idToken, objectPayload);
    }

    @Deprecated
    public static void staticSendToIOS(final String text, final String idToken, final String nomeSom) {
        staticAppleNotification.send(text, idToken, nomeSom);
    }

    @Deprecated
    public static void staticSendToIOS(final javapns.notification.Payload payload, final String idToken) {
        staticAppleNotification.send(payload, idToken);
    }
    
    public void instanceSendToAndroid(String text, List<String> registrationIds, boolean encode) {
        if (registrationIds.size() >= 1000) {
            List<List<String>> smallerLists = Lists.partition(registrationIds, 999);
            for (List l : smallerLists) {
                instanceSendToAndroid(text, l, encode);
            }

        } else {
            try {
                instanceSendPost(text, registrationIds, encode);
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }

    @Deprecated
    public static void staticSendToAndroid(String text, List<String> registrationIds, boolean encode) {
        if (registrationIds.size() >= 1000) {
            List<List<String>> smallerLists = Lists.partition(registrationIds, 999);
            for (List l : smallerLists) {
                staticSendToAndroid(text, l, encode);
            }

        } else {
            try {
                staticSendPost(text, registrationIds, encode);
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }

    
    public void instanceSendToAndroid(String text, String idToken, boolean encode) {
        ArrayList<String> registrationIds = new ArrayList<String>();
        registrationIds.add(idToken);
        try {
            instanceSendPost(text, registrationIds, encode);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }
    
    @Deprecated
    public static void staticSendToAndroid(String text, String idToken, boolean encode) {
        ArrayList<String> registrationIds = new ArrayList<String>();
        registrationIds.add(idToken);
        try {
            staticSendPost(text, registrationIds, encode);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(PushNotificationServices.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    
    private void instanceSendPost(String text, List<String> registrationIds, boolean encode) throws Exception {
        String url;
        if (useFcm) {
            url = urlFcm;
        } else {
            url = urlGcm;
        }
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Authorization", "key=" + instanceKeyAndroidConsole);

        HashMap<String, String> mensagem = new HashMap<String, String>();

        if (encode) {
            text = URLEncoder.encode(text, "UTF-8");
        }

        mensagem.put("message", text);
        List<String> tokens = registrationIds;
        HashMap<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("registration_ids", tokens);
        parametros.put("data", mensagem);

        String urlParameters = gson.toJson(parametros);
        Logger.getLogger(PushNotificationServices.class).debug("Enviando Push: " + urlParameters);
        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        Logger.getLogger(PushNotificationServices.class).debug("Push Response Code:" + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        Logger.getLogger(PushNotificationServices.class).debug(response);
    }

    
    @Deprecated
    private static void staticSendPost(String text, List<String> registrationIds, boolean encode) throws Exception {

        String url;
        if (useFcm) {
            url = urlFcm;
        } else {
            url = urlGcm;
        }
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Authorization", "key=" + staticKeyAndroidConsole);

        HashMap<String, String> mensagem = new HashMap<String, String>();

        if (encode) {
            text = URLEncoder.encode(text, "UTF-8");
        }

        mensagem.put("message", text);
        List<String> tokens = registrationIds;
        HashMap<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("registration_ids", tokens);
        parametros.put("data", mensagem);

        String urlParameters = gson.toJson(parametros);
        Logger.getLogger(PushNotificationServices.class).debug("Enviando Push: " + urlParameters);
        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        Logger.getLogger(PushNotificationServices.class).debug("Push Response Code:" + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        Logger.getLogger(PushNotificationServices.class).debug(response);

    }

    @Deprecated
    public static void staticEnviarPushTextoParaAndroid(final String text, final String idToken, final boolean encode) {
        staticSendToAndroidInBackground(text, idToken, encode);
    }
    
    public void instanceEnviarPushTextoParaAndroid(final String text, final String idToken, final boolean encode) {
        instanceSendToAndroidInBackground(text, idToken, encode);
    }
    
    public void instanceEnviarPushTextoParaAndroidSync(final String text, final String idToken, final boolean encode) {
        instanceSendToAndroidInBackgroundSync(text, idToken, encode);
    }
    
    public void instanceEnviarPushTextoParaAndroid(final String text, final List<String> registrationIds, final boolean encode) {
        instanceSendToAndroidInBackground(text, registrationIds, encode);
    }

    @Deprecated
    public static void staticEnviarPushTextoParaAndroid(final String text, final List<String> registrationIds, final boolean encode) {
        staticSendToAndroidInBackground(text, registrationIds, encode);
    }

    @Deprecated
    public static void staticEnviarPushTextoParaAndroid(final String text, final String idToken) {
        staticSendToAndroid(text, idToken);
    }

    @Deprecated
    public static void staticEnviarPushTextoParaAndroid(final String text, final List<String> registrationIds) {
        staticSendToAndroid(text, registrationIds);
    }

    @Deprecated
    public static void staticEnviarPushTextoParaIOS(final String text, final List<String> idsToken) {
        staticSendToIOS(text, idsToken, 1);
    }

    @Deprecated
    public static void staticEnviarPushTextoParaIOS(final String text, final List<String> idsToken, final ICallbackInvalidTokenPush callback) {
        staticSendToIOS(text, idsToken, callback, 1);
    }

    @Deprecated
    public static void staticEnviarPushTextoParaIOS(final String text, final List<String> idsToken, final String soundName) {
        staticSendToIOS(text, idsToken, soundName);
    }

    @Deprecated
    public static void staticEnviarPushTextoParaIOS(final String text, final String idToken) {
        staticSendToIOS(text, idToken, 1);
    }

    @Deprecated
    public static void staticEnviarPushTextoParaIOS(final String text, final String idToken, final String soundName) {
        staticSendToIOS(text, idToken, soundName);
    }

    @Deprecated
    public static void staticEnviarPushComplexoParaIOS(final javapns.notification.Payload payload, final String idToken) {
        staticSendToIOS(payload, idToken);
    }

    @Deprecated
    public static void staticEnviarPushParaAndroid(String text, List<String> registrationIds, boolean encode) {
        staticSendToAndroid(text, registrationIds, encode);
    }

    @Deprecated
    public static void staticEnviarPushParaAndroid(String text, String idToken, boolean encode) {
        staticSendToAndroid(text, idToken, encode);
    }
}
