package br.com.martinlabs.commons;

import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Logger;

/**
 *
 * @author gil
 */
public abstract class ThreadPool {

    private static Queue<Runnable> queue;
    private static Thread WORKER_THREAD;
    private static boolean isStarted = false;


    synchronized private static void checkAndInit() {
        if (isStarted) {
            return;
        }

        WORKER_THREAD = new Thread(new Runnable() {
            public void run() {
                process();
            }
        });

        WORKER_THREAD.setPriority(Thread.MAX_PRIORITY);
        WORKER_THREAD.start();
        
        if(queue == null) {
            queue = new LinkedList<>();
        }


        isStarted = true;
    }

    private static void process() {
        while (isStarted) {
            if (WORKER_THREAD == null || !WORKER_THREAD.isAlive() || WORKER_THREAD.isInterrupted()) {
                System.out.println("ThreadPool is dead.");
                isStarted = false;
                checkAndInit();
            }

            try {
                while (!queue.isEmpty()) {
                    Runnable runnable = queue.poll();
                    runnable.run();
                }
                synchronized (WORKER_THREAD) {
                    WORKER_THREAD.wait();
                }
            } catch (Exception ex) {
                Logger.getGlobal().severe(ex.getLocalizedMessage());
                ex.printStackTrace();
            }


        }
    }

    public static void queue(Runnable runnable) {
        checkAndInit();
        queue.add(runnable);
        synchronized (WORKER_THREAD) {
            WORKER_THREAD.notifyAll();
        }

    }

}
