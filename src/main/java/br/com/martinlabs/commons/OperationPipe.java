package br.com.martinlabs.commons;

import br.com.martinlabs.commons.exceptions.RespException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.ParameterStyle;
import org.apache.oltu.oauth2.rs.request.OAuthAccessResourceRequest;

/**
 *
 * @author gil
 */
public class OperationPipe {
    
    private DataSource ds;
    
    private static Context ctx;
    private static Context envContext;
    
    public OperationPipe(String dsName) {
    
        try {
            if (ctx == null) {
                ctx = new InitialContext();
            }

            if (envContext == null) {
                envContext = (Context) ctx.lookup("java:/comp/env");
            }

            ds = (DataSource) envContext.lookup(dsName);
        
        } catch (NamingException ex) {
            Logger.getLogger(OperationPipe.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }
    
    public <T> OpResp<T> handle(HttpServletRequest req, TransWithToken<T> c){
        return handle(con -> {
            
            String token = null;
            
            try {
                OAuthAccessResourceRequest oauthRequest = new OAuthAccessResourceRequest(req, ParameterStyle.HEADER);
                token = oauthRequest.getAccessToken();
            } catch (OAuthSystemException ex) {
            } catch (OAuthProblemException ex) {
            }
                
            return c.action(con, token);
        });
    }
    
    public <T> OpResp<T> handle(Trans<T> c){
        return handle(c, true);
    }
    
    public <T> OpResp<T> handle(Trans<T> c, boolean useTransaction){
        
        OpResp<T> result;
        Connection con;
            
        try {
            con = ds.getConnection();
            //nega o useTransaction no autocommit porque false é transaction, true é NÃO transaction
            con.setAutoCommit(!useTransaction);
        } catch (SQLException ex) {
            Logger.getLogger(OperationPipe.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            throw new RespException(LanguageHolder.instance.unexpectedError());
        }
        
        try {
            
            T resp  = c.action(con);
            
            if (!(resp instanceof OpResp)) {
                //se não é OpResp vamos coloca-lo dentro de um
                result = new OpResp(resp);
            } else {
                result = (OpResp<T>) resp;
            }
            
            commitAndFinish(con, useTransaction);
            
        } catch (RespException e) {
            
            rollbackAndFinish(con, useTransaction);
            Logger.getLogger(OperationPipe.class.getName()).log(Level.INFO, e.getMessage(), e);
            result = new OpResp (false, e.getMessage());
            result.setCode(e.getCode());
            
        } catch (Throwable e) {
            
            rollbackAndFinish(con, useTransaction);
            Logger.getLogger(OperationPipe.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            result = new OpResp (false, LanguageHolder.instance.unexpectedError());
            
        }
        
        return result;
    }
    
    public static void commit(Connection con) {
        if (con != null) {
            try {
                con.commit();
            } catch (SQLException ex) {
                Logger.getLogger(OperationPipe.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }
    
    public static void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                Logger.getLogger(OperationPipe.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }
    
    public static void finish(Connection con) {
        if (con != null) {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(OperationPipe.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }
    
    public static void commitAndFinish(Connection con, boolean useTransaction) {
        if (useTransaction)
            commit(con);
        
        finish(con);
    }
    
    public static void rollbackAndFinish(Connection con, boolean useTransaction) {
        if (useTransaction)
            rollback(con);
        
        finish(con);
    }
    
    public interface Trans<T> {
        public T action(Connection con);
    }
    
    public interface TransWithToken<T> {
        public T action(Connection con, String token);
    }

}
