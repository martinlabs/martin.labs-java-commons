package br.com.martinlabs.commons;

import br.com.martinlabs.commons.exceptions.RespException;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import net.coobird.thumbnailator.Thumbnails;
import java.io.InputStream;

/**
 *
 * @author gil
 */
public abstract class FileIO {
    
    public static void save(String folder, String name, byte[] bytes, LanguageHolder lang) throws FileIOException {
        FileOutputStream output = null;
        try {

            File newFolder = new File(folder);
            if (!newFolder.exists()) {
                newFolder.mkdirs();
            }

            File uploadLocation = new File(folder);
            File file = new File(uploadLocation, name);
            output = new FileOutputStream(file);
            output.write(bytes);
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new FileIOException(lang.unexpectedError());
        } finally {
            try {
                output.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void save(ServletContext ctx, String folder, String name, byte[] bytes, LanguageHolder lang) throws FileIOException {
        folder = ctx.getRealPath(folder);
        save(folder, name, bytes, lang);
    }
    
    public static void save(String folder, String name, InputStream input, LanguageHolder lang) throws FileIOException {
        FileOutputStream output = null;
        try {

            File newFolder = new File(folder);
            if (!newFolder.exists()) {
                newFolder.mkdirs();
            }

            File uploadLocation = new File(folder);
            File file = new File(uploadLocation, name);
            output = new FileOutputStream(file);
            
            byte[] buffer = new byte[8 * 1024];
            int bytesRead;
            while ((bytesRead = input.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new FileIOException(lang.unexpectedError());
        } finally {
            try {
                output.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static void save(ServletContext ctx, String folder, String name, InputStream input, LanguageHolder lang) throws FileIOException {
        folder = ctx.getRealPath(folder);
        save(folder, name, input, lang);
    }

    public static void rename(ServletContext ctx, String folder, String oldName, String newName) {
        folder = ctx.getRealPath(folder);
        File newFolder = new File(folder);

        if (!newFolder.exists()) {
            newFolder.mkdirs();
        }

        File uploadLocation = new File(folder);
        File oldFile = new File(uploadLocation, oldName);
        File newFile = new File(uploadLocation, newName);
        oldFile.renameTo(newFile);
    }

    public static void delete(ServletContext ctx, String folder, String nomeArquivo) {
        folder = ctx.getRealPath(folder);
        File newFolder = new File(folder);
        File arquivo = new File(newFolder, nomeArquivo);
        arquivo.delete();
    }
    
    @Deprecated
    public static byte[] resize(InputStream input, int dimensao) {
        return resize(input, dimensao, LanguageHolder.instance);
    }
    
    public static byte[] resize(InputStream input, int dimensao, LanguageHolder lang) {
        try {    
            input.reset();
            BufferedImage img = ImageIO.read(input);
            boolean ehMaisLargoQueAlto = img.getWidth() > img.getHeight();

            if (img.getWidth() > 3000 || img.getHeight() > 3000) {
                throw new RespException(lang.theImageIsTooBig());
                //TODO: deprecated
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            
            if (ehMaisLargoQueAlto) {
                dimensao = Math.min(img.getHeight(), dimensao);
                Thumbnails.of(img).height(dimensao).keepAspectRatio(true).outputFormat("jpg").toOutputStream(baos);
            } else {
                dimensao = Math.min(img.getWidth(), dimensao);
                Thumbnails.of(img).width(dimensao).keepAspectRatio(true).outputFormat("jpg").toOutputStream(baos);
            }
            
            return baos.toByteArray();
        } catch (IOException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            return null;
        }
    }
    
    public static void resizeAndSave(ServletContext ctx, String folder, String name, InputStream input, int dimensao, LanguageHolder lang) {

        folder = ctx.getRealPath(folder);
        
        try {
            
            File newFolder = new File(folder);
            if (!newFolder.exists()) {
                newFolder.mkdirs();
            }

            File uploadLocation = new File(folder);
            File file = new File(uploadLocation, name);
            
            BufferedImage img = ImageIO.read(input);
            boolean ehMaisLargoQueAlto = img.getWidth() > img.getHeight();
            
            if (img.getWidth() > 3000 || img.getHeight() > 3000) {
                throw new RespException(lang.theImageIsTooBig());
                //TODO: deprecated
            }
            
            if (ehMaisLargoQueAlto) {
                
                dimensao = Math.min(img.getHeight(), dimensao);
                Thumbnails.of(img).height(dimensao).keepAspectRatio(true).toFile(file);
            } else {
                
                dimensao = Math.min(img.getWidth(), dimensao);
                Thumbnails.of(img).width(dimensao).keepAspectRatio(true).toFile(file);
            }
        } catch (IOException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

    }

    public static void makeThumbnail(String folder, String nomeImagem, String nomeThumb, int dimensao) {
        
        File uploadLocation = new File(folder);
        File file = new File(uploadLocation, nomeImagem);
        File thumb = new File(uploadLocation, nomeThumb);

        try {
            BufferedImage img = ImageIO.read(file);
            boolean ehMaisLargoQueAlto = img.getWidth() > img.getHeight();
            
            if (ehMaisLargoQueAlto) {
                
                if (img.getHeight() < dimensao)
                {
                    return;
                }
                
                Thumbnails.of(file).height(dimensao).keepAspectRatio(true).toFile(thumb);
            } else {
                
                if (img.getWidth() < dimensao)
                {
                    return;
                }
                
                Thumbnails.of(file).width(dimensao).keepAspectRatio(true).toFile(thumb);
            }
        } catch (IOException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

    }

    public static void makeThumbnail(ServletContext ctx, String folder, String nomeImagem, String nomeThumb, int dimensao) {
        folder = ctx.getRealPath(folder);

        File uploadLocation = new File(folder);
        File file = new File(uploadLocation, nomeImagem);
        File thumb = new File(uploadLocation, nomeThumb);

        try {
            BufferedImage img = ImageIO.read(file);
            boolean ehMaisLargoQueAlto = img.getWidth() > img.getHeight();
            if(ehMaisLargoQueAlto){
                Thumbnails.of(file).height(dimensao).keepAspectRatio(true).toFile(thumb);
            }else{
                Thumbnails.of(file).width(dimensao).keepAspectRatio(true).toFile(thumb);
            }
        } catch (IOException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

    }

    public static class FileIOException extends RespException {

        public FileIOException(String message) {
            super(message);
        }
    }
}
