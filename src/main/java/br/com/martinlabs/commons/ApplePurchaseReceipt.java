package br.com.martinlabs.commons;

import java.util.Date;
import java.util.List;

/**
 *
 * @author ricardoprado
 */
public class ApplePurchaseReceipt {
    
    private String bundle_id;
    private String application_version;
    private List<ApplePurchasedObject> in_app;
    private String original_application_version;
    //This key is present only for apps purchased through the Volume Purchase Program.
    //If this key is not present, the receipt does not expire.
    //When validating a receipt, compare this date to the current date to determine
    //whether the receipt is expired. Do not try to use this date to calculate any other information,
    //such as the time remaining before expiration.
    private Date expiration_date;

    public String getBundle_id() {
        return bundle_id;
    }

    public void setBundle_id(String bundle_id) {
        this.bundle_id = bundle_id;
    }

    public String getApplication_version() {
        return application_version;
    }

    public void setApplication_version(String application_version) {
        this.application_version = application_version;
    }

    public List<ApplePurchasedObject> getIn_app() {
        return in_app;
    }

    public void setIn_app(List<ApplePurchasedObject> in_app) {
        this.in_app = in_app;
    }

    public String getOriginal_application_version() {
        return original_application_version;
    }

    public void setOriginal_application_version(String original_application_version) {
        this.original_application_version = original_application_version;
    }

    public Date getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(Date expiration_date) {
        this.expiration_date = expiration_date;
    }
    
    
}
