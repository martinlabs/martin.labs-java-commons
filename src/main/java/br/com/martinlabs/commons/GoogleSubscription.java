package br.com.martinlabs.commons;

/**
 *
 * @author ricardoprado
 */
@Deprecated
public class GoogleSubscription {
    
    private boolean autoRenewing;
    private long validUntilTimestampMsec;
    private String kind;
    private long initiationTimestampMsec;

    public boolean isAutoRenewing() {
        return autoRenewing;
    }

    public void setAutoRenewing(boolean autoRenewing) {
        this.autoRenewing = autoRenewing;
    }

    public long getValidUntilTimestampMsec() {
        return validUntilTimestampMsec;
    }

    public void setValidUntilTimestampMsec(long validUntilTimestampMsec) {
        this.validUntilTimestampMsec = validUntilTimestampMsec;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public long getInitiationTimestampMsec() {
        return initiationTimestampMsec;
    }

    public void setInitiationTimestampMsec(long initiationTimestampMsec) {
        this.initiationTimestampMsec = initiationTimestampMsec;
    }

    
    
    
    
}
