package br.com.martinlabs.commons;

import java.util.List;

/**
 *
 * @author gil
 */
public class PagedResp<T> {
    private List<T> list;
    private int count;

    public PagedResp() {
    }

    public PagedResp(List<T> list) {
        this.list = list;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
    
    public int getRecordsTotal() {
        return count;
    }
    
    public void setRecordsTotal(int recordsTotal) {
        this.count = recordsTotal;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
