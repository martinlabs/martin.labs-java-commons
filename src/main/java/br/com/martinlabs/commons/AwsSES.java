/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.martinlabs.commons;

import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ricardoprado
 */
public class AwsSES {
    
    
    private PropertiesCredentials credentials;
    private AmazonSimpleEmailServiceClient sesClient;

    public AwsSES() {
        try {
            InputStream properties = AwsFileManager.class.getResourceAsStream("/AwsCredentials.properties");
            credentials = new PropertiesCredentials(properties);
            sesClient = new AmazonSimpleEmailServiceClient(credentials);
            sesClient.setRegion(Region.getRegion(Regions.US_WEST_2));
        } catch (IOException ex) {
            Logger.getLogger(AwsSNS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public AwsSES(String credentialsFileName) {
        try {
            InputStream properties = AwsFileManager.class.getResourceAsStream(credentialsFileName);
            credentials = new PropertiesCredentials(properties);
            sesClient = new AmazonSimpleEmailServiceClient(credentials);
            sesClient.setRegion(Region.getRegion(Regions.US_EAST_1));
        } catch (IOException ex) {
            Logger.getLogger(AwsSNS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setRegion(Region region) {
        sesClient.setRegion(region);
    }

    public void sendEmail(SendEmailRequest request){
        sesClient.sendEmail(request);
    }
}
