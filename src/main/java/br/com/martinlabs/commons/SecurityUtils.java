package br.com.martinlabs.commons;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Formatter;
import java.security.Key;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author gil
 */
public abstract class SecurityUtils {
    
    public final static String generateRandomString(int length) {
        SecureRandom random = new SecureRandom();

        return new BigInteger(length*5, random).toString(32);
    }
    
    public final static int randInt(int min, int max) {
        SecureRandom random = new SecureRandom();
        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        return random.nextInt((max - min) + 1) + min;
    }

    public final static String generateHash(Object subject, long prime) {
        return sha1((subject.hashCode() * prime) + "");
    }
    
    public final static String md5(String input) {
         
        String md5 = null;
         
        if(null == input) return null;
         
        try {
             
        //Create MessageDigest object for MD5
        MessageDigest digest = MessageDigest.getInstance("MD5");
         
        //Update input string in message digest
        digest.update(input.getBytes(), 0, input.length());
 
        //Converts message digest value in base 16 (hex) 
        md5 = new BigInteger(1, digest.digest()).toString(16);
 
        } catch (NoSuchAlgorithmException e) {
        }
        return md5;
    }
    
    public final static String sha1(String subject) {
        String sha1 = "";
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(subject.getBytes("UTF-8"));
            sha1 = byteToHex(crypt.digest());
        } catch (NoSuchAlgorithmException e) {
            
        } catch (UnsupportedEncodingException e) {
            
        }
        return sha1;
    }
    
    public final static String sha256(String subject) {
        return DigestUtils.sha256Hex(subject); 
    }

    private final static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
    
    public static String encrypt(String raw, String key) {
        return encrypt(raw, key, false);
    }

    public static String encrypt(String raw, String key, boolean encode) {
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] iv = { 1, 2, 3, 4, 5, 6, 6, 5, 4, 3, 2, 1, 7, 7, 7, 7 };
            cipher.init(Cipher.ENCRYPT_MODE, generateKey(key), new IvParameterSpec(iv));
            byte[] b = raw.getBytes("UTF-8");
            byte[] ciphertext = cipher.doFinal(b, 0, b.length);
            byte[] encoded = Base64.getEncoder().encode(ciphertext);
            String encryptedValue = new String(encoded, "UTF-8");
            
            if (encode) {
                encryptedValue = URLEncoder.encode(encryptedValue,"UTF-8");
            }
            
            return encryptedValue;
        }
        catch(Exception ex){
            return null;
        }
    }
    
    public static String decrypt(String encrypted, String key){
        return decrypt(encrypted, key, false);
    }

    public static String decrypt(String encrypted, String key, boolean decode){
        try {
            if (decode) {
                encrypted = URLDecoder.decode(encrypted, "UTF-8");
            }
            
            byte[] decoded = Base64.getDecoder().decode(encrypted.getBytes());
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] iv = { 1, 2, 3, 4, 5, 6, 6, 5, 4, 3, 2, 1, 7, 7, 7, 7 };
            cipher.init(Cipher.DECRYPT_MODE, generateKey(key), new IvParameterSpec(iv));
            byte[] decryptedBytes = cipher.doFinal(decoded, 0, decoded.length);
            return new String(decryptedBytes, "UTF-8");
        }
        catch(Exception ex){
            return null;
        }
    }

    private static Key generateKey(String key) throws Exception {
        byte[] salt = new byte[]{-84, -119, 25, 56, -100, 100, -120, -45, 84, 67, 96, 10, 24, 111, 112, -119, 3};
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec spec = new PBEKeySpec(key.toCharArray(), salt, 1024, 128);
        SecretKey tmp = factory.generateSecret(spec);
        return new SecretKeySpec(tmp.getEncoded(), "AES");
    }

    public static String encode(String token, String enc) {
        try {
            return URLEncoder.encode(token, enc);
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public static String decode(String token, String enc) {
        try {
            return URLDecoder.decode(token, enc);
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
            return null;
        }
    }

}
