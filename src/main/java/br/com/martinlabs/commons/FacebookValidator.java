package br.com.martinlabs.commons;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author developer-a
 */
@Deprecated
public abstract class FacebookValidator {
    
    @Deprecated
    public static boolean validarFacebookToken(String facebookToken)
    {
        return validateToken(facebookToken);
    }
    
    public static boolean validateToken(String facebookToken)
    {
        try {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss 'Etc/GMT'").create();
            HttpClient httpclient;
            String url = "https://graph.facebook.com/me?fields=id&access_token=" + facebookToken;
            HttpPost httppost = new HttpPost(url);
            httpclient = new DefaultHttpClient();
            HttpResponse response = httpclient.execute(httppost);
            String json = EntityUtils.toString(response.getEntity());
            ValidateFacebookToken validacao = gson.fromJson(json, ValidateFacebookToken.class);
            return validacao.success;
            
        } catch (Exception ex) {
            return false;
        }
    }
    
    private class ValidateFacebookToken {
        public boolean success;
    }
}
