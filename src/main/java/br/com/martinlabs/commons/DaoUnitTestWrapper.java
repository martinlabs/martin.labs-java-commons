package br.com.martinlabs.commons;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import org.junit.After;
import org.junit.AfterClass;

/**
 *
 * @author ricardomeira
 */
public class DaoUnitTestWrapper {
    protected DataSource dataSource;
    private String dsName;
    private String databaseName;
    private String userName = "root";
    private String password = "root";
    private String host = "localhost";

    private static Connection currentConnection;
    

    public DaoUnitTestWrapper(String userName, String password, String host, String datasourceName, String databaseName){
        this.userName = userName;
        this.password = password;
        this.host = host;

        try {
            this.dsName = datasourceName;
            this.databaseName = databaseName;

            // sets up the InitialContextFactoryForTest as default factory.
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                    InitialContextFactoryForTests.class.getName());

            // binds the object
            dataSource = getDataSource();
            InitialContextFactoryForTests.bind(dsName, dataSource);
            Context ctx = new InitialContextFactoryForTests().getInitialContext(null);
            InitialContextFactoryForTests.bind("java:/comp/env", ctx);
            currentConnection = dataSource.getConnection();
            currentConnection.setAutoCommit(false);
        } catch (NamingException ex) {
            Logger.getLogger(DaoUnitTestWrapper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DaoUnitTestWrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Deprecated
    public DaoUnitTestWrapper(String _dsName, String databaseName) {
        try {
            this.dsName = dsName;
            this.databaseName = databaseName;
            
            // sets up the InitialContextFactoryForTest as default factory.
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                    InitialContextFactoryForTests.class.getName());
            
            // binds the object
            dataSource = getDataSource();
            InitialContextFactoryForTests.bind(dsName, dataSource);
            Context ctx = new InitialContextFactoryForTests().getInitialContext(null);
            InitialContextFactoryForTests.bind("java:/comp/env", ctx);
            currentConnection = dataSource.getConnection();
            currentConnection.setAutoCommit(false);
        } catch (NamingException ex) {
            Logger.getLogger(DaoUnitTestWrapper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DaoUnitTestWrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @AfterClass
    public static void afterAll() throws SQLException {
        if (currentConnection != null) {
            currentConnection.close();
        }
    }
    
    @After
    public void afterEach() throws Exception{
        if (currentConnection != null) {
            currentConnection.rollback();
        }
    }

    private DataSource getDataSource() {
        MysqlConnectionPoolDataSource localDb = new MysqlConnectionPoolDataSource();
        localDb.setUser(this.userName);
        localDb.setPassword(this.password);
        localDb.setServerName(this.host);
        localDb.setPort(3306);
        localDb.setDatabaseName(databaseName);
        return localDb;
    }
    
    protected Connection getConnection() {
        return currentConnection;
    }
}
