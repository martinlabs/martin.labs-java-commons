package br.com.martinlabs.commons;

import br.com.martinlabs.commons.exceptions.RespException;
import com.google.common.io.ByteStreams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 * Simpify Servlet operations
 * @author gil
 */
public class ServletContent {
        public HttpServletRequest request;
        public HttpServletResponse response;
        public Gson gson;
        public SimpleDateFormat dateFormat;
        public SimpleDateFormat timeFormat;
        public String contentType;
        public String charset = "UTF-8";
        public ServletContext context;
        
        public ServletContent() {
            gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
            dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            timeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            contentType = "text/json;charset="+charset;
        }
        
        public ServletContent(String gsonDateFormat, String strDateFormat, String strTimeFormat) {
            gson = new GsonBuilder().setDateFormat(gsonDateFormat).create();
            dateFormat = new SimpleDateFormat(strDateFormat);
            timeFormat = new SimpleDateFormat(strTimeFormat);
            contentType = "text/json;charset="+charset;
        }
    
        private String decode(String pre){
            if (pre == null) {
                return null;
            }

            String decoded = null;

            try {
                decoded = URLDecoder.decode(pre, charset);
            } catch (UnsupportedEncodingException ex) {}

            return decoded;
        }

        public <T> T extractJson(String decoded, Class<T> classOfT) {

            if (decoded == null) {
                return null;
            }

            T resp = null;

            try {
                resp = gson.fromJson(decoded, classOfT);
            } catch (JsonSyntaxException e){
                Logger.getLogger(ServletWrapper.class.getName()).log(Level.INFO, e.getMessage(), e);
            }

            return resp;
        }

        public String getBody() {
            BufferedReader reader = null;
            StringBuilder sb = new StringBuilder();

            try {

                reader = request.getReader();
                char[] buf = new char[512 * 1024]; // 4 KB char buffer
                int len;
                while ((len = reader.read(buf, 0, buf.length)) != -1) {
                    sb.append(buf, 0, len);
                }

            } catch (IOException ex) {
                Logger.getLogger(ServletWrapper.class.getName()).log(Level.INFO, ex.getMessage(), ex);
            } finally {
                try {
                    reader.close();
                } catch (IOException ex) {
                    Logger.getLogger(ServletWrapper.class.getName()).log(Level.INFO, ex.getMessage(), ex);
                }
            }

            return sb.toString();
        }
        
        public <T> T getBody(Class<T> classOfT, boolean decode) {
            if (decode) {
                return extractJson(decode(getBody()), classOfT);
            }
            
            return extractJson(getBody(), classOfT);
        }

        public <T> T getBody(Class<T> classOfT) {
            return getBody(classOfT, true);
        }

        public String getParamString(String name) {       
            return request.getParameter(name);
        }


        public String getParamDecodedString(String name) {
            return decode(getParamString(name));
        }

        public <T> T getParam(String name, Class<T> classOfT) {
            return extractJson(getParamDecodedString(name), classOfT);
        }

        public Long getParamLong(String name) {
            Long value = null;

            try {
                value = Long.parseLong(getParamString(name));
            } catch (NumberFormatException e) {
            }

            return value;
        }

        public Float getParamFloat(String name) {
            Float value = null;

            try {
                value = Float.parseFloat(getParamString(name));
            } catch (NumberFormatException e) {
            }

            return value;
        }

        public Integer getParamInteger(String name) {
            Integer value = null;

            try {
                value = Integer.parseInt(getParamString(name));
            } catch (NumberFormatException e) {
            }

            return value;
        }
        
        public Boolean getParamBoolean(String name) {
            return Boolean.parseBoolean(getParamString(name));
        }

        public boolean isParamTrue(String name) {
            return Boolean.parseBoolean(getParamString(name));
        }

        public Date getParamDate(String name) {
            Date value = null;

            try {
                value = dateFormat.parse(getParamString(name));
            } catch (ParseException ex) {
            }

            return value;
        }

        public Date getParamTime(String name) {
            Date value = null;

            try {
                value = timeFormat.parse(getParamString(name));
            } catch (Throwable ex) {
            }

            return value;
        }

        public byte[] getPartBytes(String name) {
            byte[] value = null;

            Part part = null;

            try {
                part = request.getPart(name);
            } catch (Throwable ignored) {

            }

            if (part != null) { 
                try {
                    value = ByteStreams.toByteArray(part.getInputStream());
                } catch (IOException ignored) {}
            }

            return value;
        }

        public List<FileMeta> getFilesInParts() {

            List<FileMeta> files = new LinkedList<FileMeta>();

            try {

                // 1. Get all parts
                Collection<Part> parts = request.getParts();

                // 3. Go over each part
                FileMeta temp = null;
                for(Part part:parts){

                    // 3.1 if part is multiparts "file"
                    if(part.getContentType() != null){

                        // 3.2 Create a new FileMeta object
                        temp = new FileMeta();
                        temp.setFileName(getFilename(part));
                        temp.setFileSize(part.getSize());
                        temp.setFileType(part.getContentType());
                        temp.setContent(part.getInputStream());

                        // 3.3 Add created FileMeta object to List<FileMeta> files
                        files.add(temp);

                    }
                }
            } catch (IOException ex) {
                throw new RespException(LanguageHolder.instance.unexpectedError());
                //TODO: deprecated
            } catch (ServletException ex) {
                throw new RespException(LanguageHolder.instance.unexpectedError());
                //TODO: deprecated
            }

            return files;
        }

        private String getFilename(Part part) {
            for (String cd : part.getHeader("content-disposition").split(";")) {
                if (cd.trim().startsWith("filename")) {
                    String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                    return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.
                }
            }
            return null;
        }

        public String getPartString(String name) {
            Part part = null;

            try {
                part = request.getPart(name);
            } catch (Throwable ignored) {
                return null;
            }

            Scanner scanner = null;
            try {
                scanner = new Scanner(part.getInputStream());
            } catch (Throwable ignored) {
                return null;
            }

            return scanner.nextLine();
        }

        public String getPartDecodedString(String name) {
            return decode(getPartString(name));
        }

        public <T> T getPart(String name, Class<T> classOfT) {
            return extractJson(getPartDecodedString(name), classOfT);
        }
        
        public String getAppUrl(){
            return request.getScheme() + "://" + request.getServerName() + (request.getServerPort() > 0 ? (":" + request.getServerPort()) : "") + request.getContextPath();
        }
        
        public String getAppBase(){
            return request.getScheme() + "://" + request.getServerName() + (request.getServerPort() > 0 ? (":" + request.getServerPort()) : "");
        }
    }