package br.com.martinlabs.commons;

import br.com.martinlabs.commons.exceptions.RespException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author gil
 */
public abstract class TransacProcess {
    
    private DataSource ds;
    
    protected static Context ctx;
    protected static Context envContext;
    
    private Connection testConnection;
    
    public TransacProcess(String dsName) {
    
        try {
            if (ctx == null) {
                ctx = new InitialContext();
            }

            if (envContext == null) {
                envContext = (Context) ctx.lookup("java:/comp/env");
            }

            ds = (DataSource) envContext.lookup(dsName);
        
        } catch (NamingException ex) {
            Logger.getLogger(TransacProcess.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }
    
    public Connection initTest() {
        try {
            testConnection = ds.getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(TransacProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return testConnection;
    }
    
    //DaoWrapperUnit Testing: send a connection that rollsback after test is finished
    public void forceConnection(Connection connection){
        testConnection = connection;
    }
    
    public void finishTest() {
        rollbackAndFinish(testConnection, true);
    }
    
    protected <T> T open(Trans<T> c){
        return open(c, true);
    }
    
    protected <T> T open(Trans<T> c, boolean useTransaction){
        
        Connection con;
        
        if (testConnection == null) {
            
            try {
                con = ds.getConnection();
                //nega o useTransaction no autocommit porque false é transaction, true é NÃO transaction
                con.setAutoCommit(!useTransaction);
            } catch (SQLException ex) {
                Logger.getLogger(TransacProcess.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                throw new RespException(LanguageHolder.instance.unexpectedError());
            }
            
        } else {
            con = testConnection;
        }
        
        try {
            T resp  = c.action(con);
            
            if (testConnection == null) {
                commitAndFinish(con, useTransaction);
            }
            
            return resp;
        } catch (RespException e) {
            
            if (testConnection == null) {
                rollbackAndFinish(con, useTransaction);
            }
            
            throw e;
        } catch (RuntimeException e) {
            
            if (testConnection == null) {
                rollbackAndFinish(con, useTransaction);
            }
            
            throw e;
        } 
    }
    
    protected void commit(Connection con) {
        if (con != null) {
            try {
                con.commit();
            } catch (SQLException ex) {
                Logger.getLogger(DaoWrapper.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }
    
    protected void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                Logger.getLogger(DaoWrapper.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }
    
    protected void finish(Connection con) {
        if (con != null) {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(DaoWrapper.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }
    
    private void commitAndFinish(Connection con, boolean useTransaction) {
        if (useTransaction)
            commit(con);
        
        finish(con);
    }
    
    private void rollbackAndFinish(Connection con, boolean useTransaction) {
        if (useTransaction)
            rollback(con);
        
        finish(con);
    }
    
    public interface Trans<T> {
        public T action(Connection con);
    }

}
