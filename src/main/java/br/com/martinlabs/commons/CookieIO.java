package br.com.martinlabs.commons;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author gil
 */
public abstract class CookieIO {

    public static String get(String name, HttpServletRequest request) {

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(name)) {

                    return cookie.getValue();

                }
            }
        }

        return null;

    }

    public static void set(String name, String val, HttpServletResponse response) {
        Cookie c = new Cookie(name, val);
        c.setSecure(false); // determines whether the cookie should only be sent using a secure protocol, such as HTTPS or SSL
        c.setMaxAge(60 * 60 * 24 * 30); // 30 dias em segundos
        c.setPath("/"); 
        response.addCookie(c);
    }

}
