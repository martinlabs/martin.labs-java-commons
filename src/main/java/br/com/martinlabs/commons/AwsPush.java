package br.com.martinlabs.commons;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.amazonaws.services.sns.model.DeleteTopicRequest;

/**
 *
 * @author gil
 */
public class AwsPush {
    
    private AmazonSNSClient snsClient;
    
    public AwsPush() {
        //create a new SNS client and set endpoint
        snsClient = new AmazonSNSClient(new ClasspathPropertiesFileCredentialsProvider());		                           
        snsClient.setRegion(Region.getRegion(Regions.SA_EAST_1));
    }
    
    public String createTopic(String name) {
        //create a new SNS topic
        CreateTopicRequest createTopicRequest = new CreateTopicRequest(name);
        CreateTopicResult createTopicResult = snsClient.createTopic(createTopicRequest);
        
        return createTopicResult.getTopicArn();
    }
    
    public void subscribeToTopic(String arn, String email) {
        //subscribe to an SNS topic
        SubscribeRequest subRequest = new SubscribeRequest(arn, "email", email);
        snsClient.subscribe(subRequest);
        //get request id for SubscribeRequest from SNS metadata
        System.out.println("SubscribeRequest - " + snsClient.getCachedResponseMetadata(subRequest));
    }
    
    public void publishToTopic(String arn, String value) {
        //publish to an SNS topic
        PublishRequest publishRequest = new PublishRequest(arn, value);
        PublishResult publishResult = snsClient.publish(publishRequest);
        
        //print MessageId of message published to SNS topic
        System.out.println("MessageId - " + publishResult.getMessageId());
    }
    
    public void deleteTopic(String arn) {
        //delete an SNS topic
        DeleteTopicRequest deleteTopicRequest = new DeleteTopicRequest(arn);
        snsClient.deleteTopic(deleteTopicRequest);
        //get request id for DeleteTopicRequest from SNS metadata
        System.out.println("DeleteTopicRequest - " + snsClient.getCachedResponseMetadata(deleteTopicRequest));
    }

}
