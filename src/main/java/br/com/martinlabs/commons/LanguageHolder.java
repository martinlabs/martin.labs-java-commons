package br.com.martinlabs.commons;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author gil
 */
public abstract class LanguageHolder {

    @Deprecated
    public static LanguageHolder instance = new EnglishLanguage();
    
    protected Map<String, String> dictionary = new HashMap<>();
    
    public String get(String o) {
        return dictionary.get(o);
    }
    
    public abstract String language();
    public abstract String cannotBeNull(String propertyName);
    public abstract String cannotBeNegative(String propertyName);
    public abstract String lengthCannotBeMoreThan(String propertyName, int size);
    public abstract String unexpectedError();
    public abstract String invalidLogin();
    public abstract String pleaseLogin();
    public abstract String invalidEntry();
    public abstract String isNotAValidEmail(String propertyName);
    public abstract String isNotAValidCPF(String propertyName);
    public abstract String isNotAValidCNPJ(String propertyName);
    public abstract String errorSendingTheEmail();
    public abstract String contactEmail();
    public abstract String theImageIsTooBig();
    public abstract String errorExecutingTheFunctionInThreadPool();
    public abstract String alreadyExist(String unico);
    
}
