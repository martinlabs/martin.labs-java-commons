package br.com.martinlabs.commons;

/**
 *
 * @author Kobayashi
 */
@Deprecated
public abstract class MapaUtils {
    
    public static double calcularDistancia(double latitudeA, double longitudeA, double latitudeB, double longitudeB) {
        return MapUtils.calcDistance(latitudeA, longitudeA, latitudeB, longitudeB);
    }

    public static double degreeToRadius(double degree) {
        return MapUtils.degreeToRadius(degree);
    }

    public static double milesToLatitudeDegrees(double miles) {
        return MapUtils.milesToLatitudeDegrees(miles);
    }

    public static double milesToLongitudeDegrees(double miles, double atLatitude) {
        return MapUtils.milesToLongitudeDegrees(miles, atLatitude);
    }
}