package br.com.martinlabs.commons;

import java.util.Date;

/**
 *
 * @author ricardoprado
 */
public class ApplePurchasedObject {
    
    private long quantity;
    private String product_id;
    private String transaction_id;
    //For a transaction that restores a previous transaction,
    //the transaction identifier of the original transaction.
    //Otherwise, identical to the transaction identifier.
    private String original_transaction_id;
    private Date purchase_date;
    //For a transaction that restores a previous transaction,
    //the date of the original transaction.
    private Date original_purchase_date;
    //The expiration date for the subscription,
    //expressed as the number of milliseconds since January 1, 1970, 00:00:00 GMT.
    private long expires_date;
    private Date cancellation_date;
    private String app_item_id;
    private String version_external_identifier;
    private String web_order_line_item_id;

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getOriginal_transaction_id() {
        return original_transaction_id;
    }

    public void setOriginal_transaction_id(String original_transaction_id) {
        this.original_transaction_id = original_transaction_id;
    }

    public Date getPurchase_date() {
        return purchase_date;
    }

    public void setPurchase_date(Date purchase_date) {
        this.purchase_date = purchase_date;
    }

    public Date getOriginal_purchase_date() {
        return original_purchase_date;
    }

    public void setOriginal_purchase_date(Date original_purchase_date) {
        this.original_purchase_date = original_purchase_date;
    }

    public long getExpires_date() {
        return expires_date;
    }

    public void setExpires_date(long expires_date) {
        this.expires_date = expires_date;
    }

    public Date getCancellation_date() {
        return cancellation_date;
    }

    public void setCancellation_date(Date cancellation_date) {
        this.cancellation_date = cancellation_date;
    }

    public String getApp_item_id() {
        return app_item_id;
    }

    public void setApp_item_id(String app_item_id) {
        this.app_item_id = app_item_id;
    }

    public String getVersion_external_identifier() {
        return version_external_identifier;
    }

    public void setVersion_external_identifier(String version_external_identifier) {
        this.version_external_identifier = version_external_identifier;
    }

    public String getWeb_order_line_item_id() {
        return web_order_line_item_id;
    }

    public void setWeb_order_line_item_id(String web_order_line_item_id) {
        this.web_order_line_item_id = web_order_line_item_id;
    }
            
    
}
