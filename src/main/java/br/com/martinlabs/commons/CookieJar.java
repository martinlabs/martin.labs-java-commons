/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.martinlabs.commons;

import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ricardoprado
 */
public class CookieJar implements CookieStore {

private Map<URI, List<HttpCookie>> jar;
private List<HttpCookie> freeCookies;

public CookieJar() {
    jar = new HashMap<URI, List<HttpCookie>>();
    freeCookies = new ArrayList<HttpCookie>();
}

@Override
public void add(URI uri, HttpCookie cookie) {
    if (uri != null) {
        if (!jar.containsKey(uri))
            jar.put(uri, new ArrayList<HttpCookie>());
        List<HttpCookie> cookies = jar.get(uri);
        cookies.add(cookie);
    } else {
        freeCookies.add(cookie);
    }
}

@Override
public List<HttpCookie> get(URI uri) {
    List<HttpCookie> liveCookies = new ArrayList<HttpCookie>();
    if (jar.containsKey(uri)) {
        for (HttpCookie cookie : jar.get(uri)) {
            if (!cookie.hasExpired())
                liveCookies.add(cookie);
        }
    }
    for (HttpCookie cookie : getCookies()) {
        if (cookie.getDomain().equals(uri.getHost()))
            if (!liveCookies.contains(cookie))
                liveCookies.add(cookie);
    }
    return Collections.unmodifiableList(liveCookies);
}

@Override
public List<HttpCookie> getCookies() {
    List<HttpCookie> liveCookies = new ArrayList<HttpCookie>();
    for (URI uri : jar.keySet())
        for (HttpCookie cookie : jar.get(uri)) {
            if (!cookie.hasExpired())
                liveCookies.add(cookie);
        }
    for (HttpCookie cookie : freeCookies) {
        if (!cookie.hasExpired())
            liveCookies.add(cookie);
    }
    return Collections.unmodifiableList(liveCookies);
}

@Override
public List<URI> getURIs() {
    return Collections.unmodifiableList(new ArrayList<URI>(jar.keySet()));
}

@Override
public boolean remove(URI uri, HttpCookie cookie) {
    if (jar.containsKey(uri)) {
        return jar.get(uri).remove(cookie);
    } else {
        return freeCookies.remove(cookie);
    }
}

@Override
public boolean removeAll() {
    boolean ret = (jar.size() > 0) || (freeCookies.size() > 0);
    jar.clear();
    freeCookies.clear();
    return ret;
}

    
}
