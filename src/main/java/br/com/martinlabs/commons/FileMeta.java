package br.com.martinlabs.commons;

import java.io.InputStream;

/**
 *
 * @author gil
 */

    
public class FileMeta {
    private String fileName;
    private long fileSizeBytes;
    private String fileSizeStr;
    private String fileType;
    private InputStream content;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    public long getFileSize() {
        return fileSizeBytes;
    }

    public String getFileSizeStr() {
        return fileSizeStr;
    }

    public void setFileSize(long fileSize) {
        this.fileSizeBytes = fileSize;
        this.fileSizeStr = fileSize/1024 +" Kb";
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public InputStream getContent() {
        return content;
    }

    public void setContent(InputStream content) {
        this.content = content;
    }
        
        
        
        
        
}