package br.com.martinlabs.commons;

/**
 *
 * @author gil
 */
public class EnglishLanguage extends LanguageHolder {

    @Override
    public String language() {
        return "en-us";
    }
    
    public String cannotBeNull(String propertyName) {
        return propertyName + " cannot be null";
    }

    @Override
    public String cannotBeNegative(String propertyName) {
        return propertyName + " cannot be a negative number";
    }

    public String lengthCannotBeMoreThan(String propertyName, int size) {
        return propertyName + " length cannot be more than "+size;
    }

    public String unexpectedError() {
        return "Unexpected Error. Please try again later";
    }

    public String invalidLogin() {
        return "Invalid Login";
    }

    public String pleaseLogin() {
        return "Please Login";
    }

    public String errorSendingTheEmail() {
        return "Error sending the e-mail";
    }

    @Override
    public String theImageIsTooBig() {
        return "The image is too big";
    }

    @Override
    public String errorExecutingTheFunctionInThreadPool() {
        return "Error executing the function in Thread Pool: ";
    }

    @Override
    public String contactEmail() {
        return "Contact E-mail";
    }

    @Override
    public String invalidEntry() {
        return "Invalid Entry";
    }

    @Override
    public String isNotAValidEmail(String propertyName) {
        return propertyName + " is not a valid e-mail";
    }

    @Override
    public String isNotAValidCPF(String propertyName) {
        return propertyName + " is not a valid CPF";
    }

    @Override
    public String isNotAValidCNPJ(String propertyName) {
        return propertyName + " is not a valid CNPJ";
    }

    @Override
    public String alreadyExist(String propertyName) {
        return "There is already an entry with the same " + propertyName;
    }
}
